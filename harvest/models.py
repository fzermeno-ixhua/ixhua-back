from django.db import models
from families.models import Family

class Harvest(models.Model):
    harvest_number = models.CharField(verbose_name="No. de cosecha", max_length=255, unique=True)
    family = models.ForeignKey(Family, verbose_name="Familia", on_delete=models.PROTECT)
    sub_family = models.CharField(verbose_name="Subfamilias", max_length=255)
    initial_date = models.DateField(verbose_name="Fecha inicial")
    end_date = models.DateField(verbose_name="Fecha final")
    starting_amount = models.IntegerField(verbose_name="Monto inicial", blank=True, null=True)
    active_amount = models.IntegerField(verbose_name="Monto activo (MXN)", default=0)
    investors = models.CharField(verbose_name="Inversionistas", max_length=100, blank=True, null=True)
    qualification = models.CharField(verbose_name="Calificación", max_length=5, blank=True, null=True)
    is_active = models.BooleanField(verbose_name="Activo", default=True)

    def __unicode__(self):
        return u"{0}".format(self.harvest_number)

    def __str__(self):
        return u"{0}".format(self.harvest_number)

    class Meta:
        ordering = ['harvest_number']
        verbose_name = 'Cosecha'
        verbose_name_plural = 'Cosechas'