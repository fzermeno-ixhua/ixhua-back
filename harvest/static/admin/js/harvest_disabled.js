$(document).ready(function() {

    $("#harvest_form").on('submit', function(event) {
        $("#harvest_form input[type='submit']").attr("disabled", true);
        Swal.fire({
            type: 'success',
            title: 'Enviando invitaciones',
            text: 'Por favor espere hasta que se envien las invitaciones.',
            showConfirmButton: false

        });
    });

});