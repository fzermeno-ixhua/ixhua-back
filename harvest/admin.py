# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Harvest
from django.utils.safestring import mark_safe
from families.models import Family
from clients.models import Client
from loan.models import Loan, PartialPayment
from django.core.mail import EmailMessage
from decouple import config

from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
import sys

class PacketsAdmin(admin.ModelAdmin):
    list_display = ('harvest_number', 'family', 'harvest_amount', 'qualification', 'is_active')
    list_max_show_all = sys.maxsize
    list_per_page = 10

    ordering = ('harvest_number',)
    filter_horizontal = ()
    #search_fields = ('name', 'price', 'city')
    search_fields = ('harvest_number', 'family__tradename', 'active_amount', 'qualification')
    readonly_fields = ('end_date', 'detail_actions')
    exclude = ['is_active', 'starting_amount']

    def harvest_amount(self, obj):
        return 'MXN {:20,.2f}'.format(obj.active_amount)
    harvest_amount.short_description = 'Monto activo (MXN)'
    harvest_amount.admin_order_field = 'active_amount'

    def detail_actions(self, obj):
        if obj.id:
            action = 'Desactivar' if obj.is_active else 'Activar'
            model = 'Harvest'
            message = 'esta cosecha'
            return mark_safe(
                '<a id="activate-model" style="float: none;" class="button default" data-id={0} data-action={1}  data-model={2} data-label={3} data-message="{4}" href="{0}">{1}</a>&nbsp;'
                '<a id="cancel-save" class="deletelink" style="background: none;" data-url="{5}" href="#">Cancelar</a>'.format(
                    obj.pk,
                    action,
                    model,
                    obj._meta.app_label,
                    message,
                    '/admin/{}/{}'.format(obj._meta.app_label, obj._meta.model_name))
            )
        return mark_safe('<a id="cancel-save" class="btn" data-url="{}" href="#">Cancel</a>'.format(
            '/admin/{0}/{1}'.format(obj._meta.app_label, obj._meta.model_name)))
    detail_actions.short_description = 'Acciones'

    def save_model(self, request, obj, form, change):
        obj.end_date = obj.initial_date.replace(obj.initial_date.year + 1)
        
        if not obj.id:
            obj.starting_amount = obj.active_amount
            obj.save()
            family = Family.objects.get(tradename=obj.family)
        
            clients = Client.objects.filter(family = family.pk)
            for client in clients:
                # Calculate available_loan
                total_loan = client.credit_amount
                active_loan = 0
                loan_payment = 0

                loans = Loan.objects.filter(client=client, state=2)
                
                for loan in loans:
                    active_loan += loan.amount_requested
                    payments = PartialPayment.objects.filter(loan=loan)
                    
                    for payment in payments:
                        loan_payment += payment.amount
                        if loan_payment > loan.amount_requested:
                            loan_payment = loan.amount_requested
                    
                    active_loan -= loan_payment
                    loan_payment = 0

                
                available_loan = total_loan - active_loan

                subject = 'Tienes un crédito pre - autorizado'
                html_message = render_to_string(
                    'index.html',
                    {
                        'names': client.names,
                        'last_names': client.father_last_name + ' ' + client.mother_last_name,
                        'credit_amount': '{:20,.2f}'.format(available_loan),
                        'url': config('FRONT_URL')
                    }
                )
                plain_message = strip_tags(html_message)
                from_email = config('EMAIL_HOST_USER')
                to = client.email

                mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
                
                # Set invited as True
                client.invited = True
                client.save()

        obj.save()


    class Media:
        css = {
            'all': ('admin/css/sweetalert2.css',)
        }
        js = ('admin/js/vendor/jquery.min.js', 'admin/js/vendor/sweetalert2.min.js', 'admin/js/admin.js', 'admin/js/harvest_disabled.js')

admin.site.register(Harvest, PacketsAdmin)