# -*- coding: utf-8 -*-
from django.apps import AppConfig

class HarvestConfig(AppConfig):
    name = 'harvest'
    verbose_name = 'Gestor de Cosechas'