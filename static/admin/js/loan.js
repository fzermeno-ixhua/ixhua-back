$(document).ready(function() {

    /*********************************************
        Get django cookie
  *********************************************/

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    // debugger
    $("#approved-model, #rejected-model").on('click', function(event) {
        console.log("here");
        var dataId = $(this).data("id");
        var action = $(this).data("action").toLowerCase();
        var state = $(this).data("state");
        var harvest = $(this).data("harvest");
        var model = $(this).data("model");
        var label = $(this).data("label");
        var message = $(this).data("message");
        var values = {
            'action': action,
            'id': dataId,
            'model': model,
            'label': label,
            'harvest': harvest,
        };
        event.preventDefault();
        if (state !== 'disabled') {
            console.log("here2");
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/loan/change-loan-status";
            console.log(baseUrl, values);
            ajaxRequest(values, baseUrl);
        }


        // swal({
        //         title: `Â¿Seguro que deseas ${action} ${message}`,
        //         text: "",
        //         type: "warning",
        //         showCancelButton: true,
        //         confirmButtonText: `Si, ${action}`,
        //         cancelButtonText: "Cancelar",
        //         closeOnConfirm: true,
        //     },
        //     function(isConfirm) {
        //         if (isConfirm) {
        //             var getUrl = window.location;
        //             var baseUrl = getUrl.protocol + "//" + getUrl.host + "/users/activate-model";
        //             console.log(baseUrl, values);
        //             ajaxRequest(values, baseUrl);
        //         }
        //     });
    });

    function ajaxRequest(data, url) {
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function(data, status) {
                location.reload();
            },
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        }); // end ajax call
    }

});