from django.shortcuts import render
from .models import Loan, ForDispersed, PendingPayment, Seeds, PartialPayment,BackupLoan
from clients.models import ClientAdjustment,ClientDocuments
from harvest.models import Harvest
from core.models import CustomUser
from django.http import HttpResponse
import json
from django.core.mail import EmailMessage,EmailMultiAlternatives
from decouple import config
import datetime,pytz
from decimal import Decimal, ROUND_HALF_UP
from api.views import calculate_interest

from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission
from django.contrib import messages

from datetime import date
from clients.models import Client
from datetime import datetime, timedelta
import json
from django.core.mail import send_mail

#SMS
import nexmo
import phonenumbers

import requests


# Create your views here.

def ChangeLoanStatus(request):
    if request.POST:
      
        user = request.user        
        permission= user.has_perm("change_loan")
        
        if permission is False:
            messages.error(request, 'No tienes el permiso para realizar esta acción. Ponte en contacto con un Administra')
            return HttpResponse(json.dumps({'error': True}), content_type="application/json")


        id = request.POST.get('id')
        loan = Loan.objects.select_related('client').get(pk=id)
        
        if request.POST.get('action') == 'approved':
            state = 2
            harvest = Harvest.objects.get(harvest_number=str(request.POST.get('harvest')))
            harvest.active_amount = harvest.active_amount - loan.amount_requested
            harvest.save()
            loan.harvest = harvest
            for_dispersed = ForDispersed.objects.create(loan=loan, state=2)
			# interest = 0.0060
            interest = calculate_interest(loan.client)
            interest_to_pay = (loan.amount_requested * interest * loan.term) + (loan.amount_requested * interest*loan.term) * (0.16)
            full_payment = loan.amount_requested + interest_to_pay
            # send_approved(loan.client.email, loan.payment_date, full_payment)
            #treasurers = CustomUser.objects.filter(role=4)
            #for treasurer in treasurers:
            send_treasurers(loan.client, loan)



        if request.POST.get('action') == 'rejected':
            state = 3
            reason_for_rejection = request.POST.get('text')
            loan.reason_for_rejection = reason_for_rejection
            send_rejected(loan, reason_for_rejection)
        
        loan.state = state
        loan.save()

        try:
            #loan_backup_save(loan)
            b = BackupLoan.objects.create(
                request_no = loan.request_no,
                amount_requested = loan.amount_requested,
                aperture = loan.aperture,
                amount_total = loan.amount_total,
                state = loan.get_state_display(),
                option = loan.option,
                payment_date = loan.payment_date,
                term = loan.term,
                created_at = loan.created_at,
                reason_for_rejection = loan.reason_for_rejection,
                contract_no = loan.contract_no,
                contract = loan.contract ,
                names = loan.client.names,
                father_last_name = loan.client.father_last_name,
                mother_last_name = loan.client.mother_last_name,
                gender = loan.client.gender,
                birthdate = loan.client.birthdate,
                nationality = loan.client.nationality,
                civil_status = loan.client.civil_status,
                address = loan.client.address,
                outdoor_no = loan.client.outdoor_no,
                interior_no = loan.client.interior_no,
                postal_code =loan.client.postal_code,
                suburb = loan.client.suburb,
                city = loan.client.city,
                state_country = loan.client.state,
                country = loan.client.country,
                house_type = loan.client.house_type,
                years_residence = loan.client.years_residence,
                cellphone = loan.client.cellphone,
                phone = loan.client.phone,
                email = loan.client.email,
                birth_place = loan.client.birth_place,
                curp = loan.client.curp,
                rfc = loan.client.rfc,
                study_level = loan.client.study_level,
                family = loan.client.family.tradename,
                family_str = loan.client.family_str ,
                actual_position = loan.client.actual_position,
                labor_old = loan.client.labor_old,
                labor_old_month = loan.client.labor_old_month,
                salary = loan.client.salary,
                family_income = loan.client.family_income,
                economic_dependent = loan.client.economic_dependent,
                bank_name = loan.client.bank_name,
                bank_clabe = loan.client.bank_clabe,
                credit_bureau = loan.client.credit_bureau,
                credit_lines = loan.client.credit_lines,
                tell_us = loan.client.tell_us,
                reference = loan.client.reference,
                reference_phone = loan.client.reference_phone,
                family_reference = loan.client.family_reference,
                family_reference_phone = loan.client.family_reference_phone,
                notes = loan.client.notes,
                )


            #try:
            #    docs = ClientDocuments.objects.get(client=loan.client.id)
                #if docs.ine:
            #    b.ine = docs.ine
                #if docs.bank_account:
            #    b.bank_account = docs.bank_account
                #if docs.proof_address:
            #    b.proof_address = docs.proof_address
                #if docs.additional_file:
            #    b.additional_file = docs.additional_file
                #if docs.paysheet_one:
            #   b.paysheet_one = docs.paysheet_one
                #if docs.paysheet_two:
            #    b.paysheet_two = docs.paysheet_two
                #if docs.paysheet_three:
            #    b.paysheet_three = docs.paysheet_three
               # if docs.selfie:
            #    b.selfie = docs.selfie
            #except:
            #    pass

            b.save()
            messages.info(request, 'Se ha hecho un respaldo de la información. Puedes consultarla en el modulo de Backup')
        except:
            pass

    return HttpResponse(json.dumps({'success': True}), content_type="application/json")



def loan_backup_email(loan):
    
    try:
        docs = ClientDocuments.objects.get(client=loan.client.id)
    except:
        pass

    subject = 'Backup Solicitud -  ' + str(loan.request_no)
    html_message = render_to_string(
        'loan_backup_notification.html',
        {
            'request_no': str(loan.request_no),
            'amount_requested': str(loan.amount_requested),
            'aperture': str(loan.aperture),
            'amount_total': str(loan.amount_requested),
            'state': str(loan.get_state_display()),
            'option': str(loan.option),
            'term': str(loan.term),
            'created_at': str(loan.created_at.strftime("%d/%m/%Y %H:%M:%S")),
            'reason_for_rejection': str(loan.reason_for_rejection),
            'contract_no': str(loan.contract_no),
            'payment_date': str(loan.payment_date.strftime("%d/%m/%Y")),
            'names':str(loan.client.names) ,
            'father_last_name':str(loan.client.father_last_name) ,
            'mother_last_name':str(loan.client.mother_last_name),
            'gender':str(loan.client.gender),
            'birthdate':str(loan.client.birthdate.strftime("%d/%m/%Y")),
            'nationality':str(loan.client.nationality),
            'civil_status':str(loan.client.civil_status),
            'address':str(loan.client.address),
            'outdoor_no':str(loan.client.outdoor_no),
            'interior_no':str(loan.client.interior_no),
            'postal_code':str(loan.client.postal_code),
            'suburb':str(loan.client.suburb),
            'city':str(loan.client.city),
            'city_state':str(loan.client.state),
            'country':str(loan.client.country),
            'house_type':str(loan.client.house_type),
            'years_residence':str(loan.client.years_residence),
            'phone':str(loan.client.phone),
            'cellphone':str(loan.client.cellphone),
            'email':str(loan.client.email),
            'birth_place':str(loan.client.birth_place),
            'curp':str(loan.client.curp),
            'rfc':str(loan.client.rfc),
            'study_level':str(loan.client.study_level),         
            'family':str(loan.client.family.tradename),
            'family_str':str(loan.client.family_str),
            'actual_position':str(loan.client.actual_position),
            'labor_old':str(loan.client.labor_old),
            'labor_old_month':str(loan.client.labor_old_month),
            'salary':str(loan.client.salary),
            'family_income':str(loan.client.family_income),
            'economic_dependent':str(loan.client.economic_dependent),
            'bank_name':str(loan.client.bank_name),
            'bank_clabe':str(loan.client.bank_clabe),          
            'credit_bureau':str(loan.client.credit_bureau),
            'credit_lines':str(loan.client.credit_lines),
            'tell_us':str(loan.client.tell_us),
            'reference':str(loan.client.reference),
            'reference_phone':str(loan.client.reference_phone),
            'family_reference':str(loan.client.family_reference),
            'family_reference_phone':str(loan.client.family_reference_phone),
            'notes':str(loan.client.notes),
        }
    )
    plain_message = strip_tags(html_message)
    from_email = config('EMAIL_HOST_USER')
    to = config('LOAN_EMAIL')

    email = EmailMultiAlternatives(subject, plain_message, from_email, [to])
    email.attach_alternative(html_message, "text/html")
    
    try:
        email.attach(docs.ine.name,docs.ine.read(),mimetype="application/pdf")
    except:
        pass

    try:
        email.attach(docs.bank_account.name,docs.bank_account.read(),mimetype="application/pdf")
    except:
        pass

    try:
        email.attach(docs.proof_address.name,docs.proof_address.read(),mimetype="application/pdf")
    except:
        pass

    try:
        email.attach(docs.additional_file.name,docs.additional_file.read(),mimetype="application/pdf")
    except:
        pass

    try:
        email.attach(docs.paysheet_one.name,docs.paysheet_one.read(),mimetype="application/pdf")
    except:
        pass

    try:
        email.attach(docs.paysheet_two.name,docs.paysheet_two.read(),mimetype="application/pdf")
    except:
        pass

    try:
        email.attach(docs.paysheet_three.name,docs.paysheet_three.read(),mimetype="application/pdf")
    except:
        pass

    try:
        email.attach(docs.selfie.name,docs.selfie.read(),mimetype="application/pdf")
    except:
        pass
    
    try:
        email.attach(loan.contract.name,loan.contract.read(),mimetype="application/pdf")
    except:
        pass

    #mail.attach(loan.contract.name,loan.contract.read(),mimetype="application/pdf")
    #email.send()






    
def send_approved(client_email, payment_date, total, loan):
    # Send recovery email
    subject = '¡Tu crédito fue aprobado!'
    html_message = render_to_string(
        'base.html',
        {
            'title': '¡Felicidades!',
            'content_template': 'content_approved.html',
            'contact_template': 'contact.html',
            'url': config('FRONT_URL') + "auth/login/",
            'payment_date': payment_date.strftime('%d/%m/%Y'),
            'amount': total
        }
    )
    plain_message = strip_tags(html_message)
    from_email = config('EMAIL_HOST_USER')
    to = client_email

    email = EmailMessage(subject, html_message, from_email, [to])

    email.content_subtype = "html"
    if loan.contract is not None:
        email.attach(loan.request_no + '.pdf', loan.contract.read())
    email.send()
#         email = EmailMessage(
#                     'Préstamo aprobado',
#         """
# ¡Felicidades! Todos los datos de tu solicitud fueron correctos. <br>
# <br>
# El dinero debe de estar en menos de 3 horas en tu cuenta de banco. Tu fecha de pago es el día {0}, y te pedimos que ese día tengas el dinero disponible ya que cargaremos la cantidad de ${1} a tu cuenta en la que te depositamos. <br>
# <br>
# Te recordamos que puedes consultar tu perfil para revisar tu fecha de pago, montos y créditos autorizados que puedas disponer en <a href="{2}" style="color:#1C7BFF; text-decoration: underline;">{2}</a>  <br>
# <br>
# ¡Entre más puntual seas con tu pago, tendrás acceso a mejores tasas y a mayores montos!
#         """.format(payment_date.strftime('%d/%m/%Y'), total, config('FRONT_URL') + "auth/login/")
#                     ,
#                     to=[client_email])
#         email.content_subtype = "html"
#         email.attach(loan.request_no + '.pdf', loan.contract.read())
#         email.send()


def send_rejected(loan, reason_for_rejection):
        # Send rejected email
        subject = 'Tu solicitud presenta un error'
        html_message = render_to_string(
            'base.html',
            {
                'title': 'Tu solicitud presenta un error',
                'content_template': 'content_rejected.html',
                'contact_template': 'contact.html',
                'names': loan.client.names,
                'father_last_name': loan.client.father_last_name,
                'mother_last_name': loan.client.mother_last_name,
                'url': config('FRONT_URL') + "auth/login/",
                'request_no': loan.request_no,
                'reason_for_rejection': reason_for_rejection
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = loan.client.email

        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)

#         email = EmailMessage(
#                     'Préstamo rechazado',
#         """
# Hola {0} {1} {2} <br>
# <br>
# La información que proporcionaste en tu solicitud, no coincide con nuestra base de datos. <br>
# <br>
# Te pedimos acceder de nuevo a tu solicitud en nuestro sitio de ínternet <a href="{3}" style="color:#1C7BFF; text-decoration: underline;">{3}</a>. <br>
# <br>
# Cualquier duda o comentario, contáctanos vía email o comunícate a nuestras oficinas. Con gusto te atenderemos. <br>
# <br>
# El contrato de préstamo {4} en este acto queda rescindido y sin efectos legales, por lo que el monto no será abonado a tu cuenta, te pedimos verificar tus datos y volver a generar tu préstamo. <br>
# <br>
# Cualquier duda o comentario, contáctanos vía email a <a href="mailto:contacto@ixhua.mx" style="color:#1C7BFF; text-decoration: underline;">contacto@ixhua.mx</a> o comunícate a nuestro whatsapp 331-913-6707 <br>
# <br>
# Consulta nuestro aviso de privacidad aquí www.xxxxxxx.xxxxx.mx
#         """.format(loan.client.names, loan.client.father_last_name, loan.client.mother_last_name, config('FRONT_URL') + "auth/login/", loan.request_no)
#                     ,
#                     to=[loan.client.email])
#         email.content_subtype = "html"
#         email.send()


def send_treasurers(client, loan):
        # Send treasurers email
        subject = '{} Solicitud de dispersión'.format(loan.request_no)
        html_message = render_to_string(
            'base.html',
            {
                'title': '{} Solicitud de dispersión'.format(loan.request_no),
                'content_template': 'content_treasurers.html',
                'names': client.names,
                'father_last_name': client.father_last_name,
                'mother_last_name': client.mother_last_name,
                'amount': loan.amount_requested,
                'payment_date': loan.payment_date.strftime('%d/%m/%Y'),
                'clabe': client.bank_clabe,
                'bank': client.bank_name,
                'request_no': loan.request_no
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = config('TREASURY_EMAIL') #treasurer.email
        cc = [config('TREASURY_CC1_EMAIL'),config('TREASURY_CC2_EMAIL')]

        #mail.send_mail(subject, plain_message, from_email,[to],cc, html_message=html_message)

        email = EmailMessage(subject, html_message, from_email, [to],cc=cc)
        email.content_subtype = "html"
        email.send()
#         email = EmailMessage(
#                     'Aceptación de solicitud',
#         """
# Datos verificados y aprobados para dispersión de: {}

# {} {} {}

# Monto ${}

# Fecha de pago: {}

# Cuenta Clabe: {} 

# Banco: {}
#         """.format(loan.request_no, loan.client.names, loan.client.father_last_name, loan.client.mother_last_name, loan.amount_requested, loan.payment_date.strftime('%d/%m/%Y'), loan.client.bank_clabe, loan.client.bank_name)
#                     ,
#                     to=[treasurer.email])
#         email.send()


def send_dispersion_notification(name,lastname,rfc,request_no,amount_requested):
    subject = 'SE HA REALIZADO UNA DISPERSIÓN -  ' + str(request_no)
    html_message = render_to_string(
        'dispersion_admin_notification.html',
        {
            'name': str(name),
            'lastname': str(lastname),
            'rfc': str(rfc),
            'request_no': str(request_no),
            'amount_requested': str(amount_requested),
        }
    )
    plain_message = strip_tags(html_message)
    from_email = config('EMAIL_HOST_USER')
    to = config('CONTACT_EMAIL')
    
    mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)

#enviar encuesta al dispersar
def send_sms_dispersion_survey(name,cellphone):
    sms = nexmo.Client(key=settings.NEXMO_KEY, secret=settings.NEXMO_SECRET)
    sms.send_message({
    'from': 'Ixhua',
    'to': '52'+ cellphone,
    'text': "Hola "+ str(name) + 
    ", gracias por tu solicitud en ixhua.mx en menos de 3 horas tendras el dinero en tu cuenta. ¿Nos podrias ayudar con una encuesta? Solo es una pregunta. " + config('SURVEY_DISPERSION')   
    })


def ChangeForDispersedStatus(request):
    if request.POST:

        user = request.user        
        permission= user.has_perm("change_loan")
        
        if permission is False:
            messages.error(request, 'No tienes el permiso para realizar esta acción. Ponte en contacto con un Administrador.')
            return HttpResponse(json.dumps({'error': True}), content_type="application/json")

        id = request.POST.get('id')
        for_disperse = ForDispersed.objects.get(pk=id)
        for_disperse.state = 1
        
        #fecha de dispersion con el cambio de zona horaria
        try:
            utcnow = pytz.timezone('utc').localize(datetime.utcnow())
            here = utcnow.astimezone(pytz.timezone('America/Mexico_City')).replace(tzinfo=None)
            for_disperse.dispersion_date = here.date()     
        except:
            for_disperse.dispersion_date = date.today()
        
        for_disperse.save()

        seeds_count = 0

        try:
            seeds_count =  for_disperse.loan.client.seeds.yellow
        except:
            pass

        try:      
            for_disperse.loan.client.subfamily="RECURRENTE-"+str(seeds_count)
            for_disperse.loan.client.save()
        except:
            pass

        pending = PendingPayment.objects.create(loan=for_disperse.loan, amount_total=for_disperse.loan.amount_total)

        send_approved(for_disperse.loan.client.email, for_disperse.loan.payment_date, for_disperse.loan.amount_total, for_disperse.loan)

        try:
            send_dispersion_notification(for_disperse.loan.client.names,for_disperse.loan.client.father_last_name,for_disperse.loan.client.rfc,for_disperse.loan.request_no,for_disperse.loan.amount_requested)
        except:
            pass
                
        try:
            send_sms_dispersion_survey(for_disperse.loan.client.names, for_disperse.loan.client.cellphone)
        except:
            pass
                
        collectors = CustomUser.objects.filter(role=3)
            
        for collector in collectors:
            send_collectors(collector, for_disperse.loan.client, for_disperse.loan)
  
    return HttpResponse(json.dumps({'success': True}), content_type="application/json")


def send_collectors(collector, client, loan):
        # Send collectors email
        subject = "{} Confirmación de dispersión a cliente".format(loan.request_no)
        html_message = render_to_string(
            'base.html',
            {
                'title': "{} Confirmación de dispersión a cliente".format(loan.request_no),
                'content_template': 'content_collectors.html',
                'request_no': loan.request_no,
                'names': client.names,
                'father_last_name': client.father_last_name,
                'mother_last_name': client.mother_last_name,
                'amount': loan.amount_requested,
                'payment_date': loan.payment_date.strftime('%d/%m/%Y'),
                'clabe': client.bank_clabe,
                'bank': client.bank_name,
                'email': client.email
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = collector.email

        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
#         email = EmailMessage(
#                     loan.request_no + " Solicitud de dispersión ",
#         """
# Datos verificados y aprobados para dispersión de: 

# Número de solicitud: {} 
# Nombre(s) apellido(s) del cliente: {} {} {}
# Monto Aprobado: ${}
# Fecha de pago: {}
# Cuenta Clabe: {}
# Banco: {} 
# Correo de confirmación: {}
#         """.format(loan.request_no, loan.client.names, loan.client.father_last_name, loan.client.mother_last_name, loan.amount_requested, loan.payment_date.strftime('%d/%m/%Y'), loan.client.bank_clabe, loan.client.bank_name, loan.client.email)
#                     ,
#                     to=[collector.email])
#         email.send()


def ChangePendingPaymentStatus(request):
    """ ChangePendingPaymentStatus method
        It is triggered when a payment is done

        Returns:
           
    """
    if request.POST:

        user = request.user        
        permission= user.has_perm("change_loan")    

        if permission is False:
            messages.error(request, 'No tienes el permiso para realizar esta acción. Ponte en contacto con un Administrador.')
            return HttpResponse(json.dumps({'error': True}), content_type="application/json")

        id = request.POST.get('id')
        ts = request.POST.get('date')
        amount = Decimal(request.POST.get('amount'))
        
        paid_out = PendingPayment.objects.get(pk=id)
        dt_object = date.fromtimestamp(int(ts)/1000)
        paid_out.payday = dt_object
        total_date = calculate_total_date(paid_out.loan.client, dt_object, paid_out)
        
        paid_out.amount_total = total_date
        paid_out.amount_total -= amount
        
        if dt_object <= paid_out.loan.payment_date:
            paid_out.state = 1
        else:
            paid_out.state = 2 

        
        if paid_out.amount_total <= 0:

            paid_out.paid_out = True
            paid_out.amount_total = 0
            if dt_object <= paid_out.loan.payment_date:
                    paid_out.state = 1
            else:
                    paid_out.state = 2
            
            try:
                paid_out.loan.harvest.active_amount += paid_out.loan.amount_requested
                paid_out.loan.harvest.save()
            except:
                print("No fue posible regresar el dinero a la cosecha.")
                pass

            # send_payment_confirmation(paid_out.loan.client, paid_out.loan, amount)
        else:
            paid_out.state = 2
        
        paid_out.save()
        
        # Creacion de registro de pago, ya sea completo o parcial
        last_partial = PartialPayment.objects.filter(loan=paid_out.loan).order_by('-partial_no').first()
        
        # Which payment Number
        if last_partial is not None:
            partial_no = last_partial.partial_no + 1
        else:
            partial_no = 1
        
        partial = PartialPayment.objects.create(loan=paid_out.loan, partial_no=partial_no, payday=dt_object, amount=amount)       
        send_payment_confirmation(paid_out.loan.client, paid_out.loan, amount)

        try:
            send_payment_notification(paid_out.loan.client.names,paid_out.loan.client.father_last_name,paid_out.loan.client.rfc,paid_out.loan.request_no,amount)
        except:
            pass

        try:
            send_sms_payment_survey(paid_out.loan.client.names,paid_out.loan.client.cellphone)
        except:
            pass

        if paid_out.paid_out:

            # Aqui codigo para crear registro de semilla
            loans = Loan.objects.filter(client=paid_out.loan.client)
            seed = Seeds.objects.filter(loan__in=list(loans)).order_by('-pk').first()
            adjustment = ClientAdjustment.objects.filter(client=paid_out.loan.client).order_by('-pk').first()


            if adjustment is None and seed is None:
                    gray = 0
                    green = 5
                    yellow = 0
            elif adjustment is None:
                    gray = seed.gray
                    green = seed.green
                    yellow = seed.yellow
            elif seed is None:
                    gray = adjustment.gray
                    green = adjustment.green
                    yellow = adjustment.yellow
            else:
                    if adjustment.created_at > seed.created_at:
                            gray = adjustment.gray
                            green = adjustment.green
                            yellow = adjustment.yellow
                    else:
                            gray = seed.gray
                            green = seed.green
                            yellow = seed.yellow
            
            # if seeds is None:
            #         gray = 0
            #         green = 5
            #         yellow = 0
            # else:
            #         gray = seeds.gray
            #         green = seeds.green
            #         yellow = seeds.yellow
            
            if paid_out.state == 1:
                    gray, green, yellow = calculate_seed(gray, green, yellow, True)
            else:
                    gray, green, yellow = calculate_seed(gray, green, yellow, False)
            
            seed = Seeds.objects.create(loan=paid_out.loan, gray=gray, green=green, yellow=yellow)

            if seed.gray == 5:
                    paid_out.loan.client.user.is_active = False
                    paid_out.loan.client.user.save()

            ###
            ## Credit Limit increase
            ###
            if paid_out.state == 1:

                client = Client.objects.get(pk=paid_out.loan.client_id)
                new_credit_limit = Decimal(client.credit_amount + client.credit_limit_increase).quantize(Decimal('.01'), rounding=ROUND_HALF_UP)
                
                if new_credit_limit >= client.credit_limit_max:
                    # If gets to the max, max is the limit
                    new_credit_limit = client.credit_limit_max

                client.credit_amount = new_credit_limit
                client.save()
                ## End Credit Limit increase
    

    return HttpResponse(json.dumps({'success': True}), content_type="application/json")


def calculate_total_date(client, date, paid_out):
        total_today = 0

        amount = paid_out.loan.amount_requested
        days_late = (paid_out.loan.payment_date - date).days
        #print("Dias tardes: ")
        #print(days_late)
        
        if days_late < -10:
                days_late = -10
        
        if paid_out.paid_out or days_late > 0:
                days_late = 0
        
        days_late = abs(days_late)

        interest = calculate_moratorium(client, days_late)
        #print("Interes:")
        #print(interest)
        remaining = 0
        
        # if days_late > 0:
        
        loan_payments = 0
        payments = PartialPayment.objects.filter(loan=paid_out.loan)

        for payment in payments:
                loan_payments += payment.amount

        pending_amount = paid_out.loan.amount_total - loan_payments
        
        interest_moratorium = calculate_moratorium(client, days_late)
        moratorium = (float(pending_amount) * interest_moratorium * days_late)
        moratorium_iva = moratorium*0.16
        total_moratorium = moratorium + moratorium_iva
        final_total = float(paid_out.loan.amount_total) + total_moratorium

        TWOPLACES = Decimal(10) ** -2
        remaining = Decimal(final_total - float(loan_payments)).quantize(TWOPLACES)
        
        # obj.amount_total = remaining
        # obj.save()


        return remaining

def calculate_moratorium(client, days):
    if days <= 6:
        loans = Loan.objects.filter(client=client)
        seed = Seeds.objects.filter(loan__in=list(loans)).order_by('-pk').first()
        adjustment = ClientAdjustment.objects.filter(client=client).order_by('-pk').first()


        if adjustment is None and seed is None:
            gray = 0
            green = 5
            yellow = 0
        elif adjustment is None:
            gray = seed.gray
            green = seed.green
            yellow = seed.yellow
        elif seed is None:
            gray = adjustment.gray
            green = adjustment.green
            yellow = adjustment.yellow
        else:
            if adjustment.created_at > seed.created_at:
                gray = adjustment.gray
                green = adjustment.green
                yellow = adjustment.yellow
            else:
                gray = seed.gray
                green = seed.green
                yellow = seed.yellow
        
        if yellow > 0:
            interest = 18 - yellow
        elif gray > 0:
            interest = 18 + gray
        else:
            interest = 18

        # if seed is not None:
        #     if seed.yellow > 0:
        #         interest = 18 - seed.yellow
        #     elif seed.gray > 0:
        #         interest = 18 + seed.gray
        #     else:
        #         interest = 18
        # else:
        #     interest = 18

        interest = interest / 100
        interest = (interest*12) / 360
    else:
        interest = 1.5 / 100

    return interest


def calculate_seed(gray, green, yellow, add):
        seeds_list_values = [5,4,3,2,1,5,4,3,2,1,5,4,3,2,1]
        seeds_list_colors = ['g','g','g','g','g','v','v','v','v','v','a','a','a','a','a']

        first_index = -1

        if gray > 0:
                first_index = seeds_list_values[:5].index(gray)
        elif green > 0:
                first_index = seeds_list_values[5:10].index(green) + 5
        elif yellow > 0:
                first_index = seeds_list_values[10:].index(yellow) + 10
        
        if add:
                first_index += 1
                if first_index > 10:
                        first_index = 10
        else:
                first_index -= 1
                if first_index < 0:
                        first_index = 0
        
        gray = seeds_list_colors[first_index:first_index+5].count('g')
        green = seeds_list_colors[first_index:first_index+5].count('v')
        yellow = seeds_list_colors[first_index:first_index+5].count('a')


        return gray, green, yellow

def send_payment_confirmation(client, loan, payment):
        # Send payment confirmation email
        subject = 'Pago exitoso'
        html_message = render_to_string(
            'base.html',
            {
                'title': 'Pago exitoso',
                'content_template': 'content_payment.html',
                'contact_template': 'contact.html',
                'names': client.names,
                'father_last_name': client.father_last_name,
                'mother_last_name': client.mother_last_name,
                'request_no': loan.request_no,
                'amount': payment,
                'payment_date': loan.payment_date.strftime('%d/%m/%Y'),
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = client.email

        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)

def send_payment_notification(name,lastname,rfc,request_no,amount):
    subject = 'SE HA REALIZADO UN PAGO -  ' + str(request_no)
    html_message = render_to_string(
        'payment_admin_notification.html',
        {
            'name': str(name),
            'lastname': str(lastname),
            'rfc': str(rfc),
            'request_no': str(request_no),
            'amount': str(amount),
        }
    )
    plain_message = strip_tags(html_message)
    from_email = config('EMAIL_HOST_USER')
    to = config('CONTACT_EMAIL')
    
    mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)

def send_sms_payment_survey(name,cellphone):
    sms = nexmo.Client(key=settings.NEXMO_KEY, secret=settings.NEXMO_SECRET)
    sms.send_message({
    'from': 'Ixhua',
    'to': '52'+ cellphone,
    'text': 'Hola '+ str(name) + 
    ' gracias por pagar tu préstamo en ixhua.mx Queremos mejorar ¿Nos podrias ayudar con una encuesta? Dale clic en el enlace. Te toma solo 30 segundos. ' + config('SURVEY_PAYMENT')
    })
        
def send_loan_notification(request):
    if request.POST:
        
        today = date.today()
        days_before_3 = today + timedelta(days=3)
        days_before_1 = today + timedelta(days=1)

        print(today)
        print(days_before_3)
        print(days_before_1)

        x = date(2020, 7, 30)

        pendingPayment = PendingPayment.objects.filter(paid_out=False)
        query = pendingPayment.prefetch_related('loan').all()

        for p in query:
            if (p.loan.payment_date == days_before_3) or (p.loan.payment_date == days_before_1) or (p.loan.payment_date == today):
                try:
                    send_mail(
                    'Recordatorio IXHUA.MX ' + str(p.loan.request_no) ,
                    'Hola '+(p.loan.client.names)+', te recordamos que el dia '+str(p.loan.payment_date)+' se hara el cobro automatico a tu cuenta por '+str(p.amount_total)+' pesos. No olvides tener disponible el dinero en tu cuenta. IXHUA.MX',
                    config('EMAIL_HOST_USER'),
                    [p.loan.client.email],
                    )
                except:
                    print("except email")
                    pass

    messages.success(request, 'Los recordatorios han sido enviados')
    return HttpResponse(json.dumps({'success': True}), content_type="application/json")


def send_survey_reference(request):
    if request.POST:
        try:
            id = request.POST.get('id')
            loan = Loan.objects.select_related('client').get(pk=id)
        except:
            messages.error(request, 'El numero de la solicitud no existe.')
            return HttpResponse(json.dumps({'success': False}), content_type="application/json")

        #Enviar SMS al solicitante
        sms = nexmo.Client(key=settings.NEXMO_KEY, secret=settings.NEXMO_SECRET)
        try:
            sms.send_message({
            'from': 'Ixhua',
            'to': '52'+ loan.client.cellphone,
            'text': 'Hola '+ str(loan.client.names) + ' ' + str(loan.client.father_last_name) + ', acabamos de enviar un mensaje de texto a tus 2 referencias. Avìsales para que lo contesten lo antes posible.'
            })
        except:
            pass

        try:
            sms.send_message({
            'from': 'Ixhua',
            'to': '52'+ loan.client.reference_phone,
            'text': 'Hola soy Marìa de ixhua.mx, '+ str(loan.client.names) + ' ' + str(loan.client.father_last_name) + ' està haciendo un tramite con nosotros y te puso como su referencia. ¿Nos ayudas a contestar un breve formulario? da clic en este enlace: ' + settings.SURVEY_REFERENCE
            })
        except:
            pass

        try:
            sms.send_message({
            'from': 'Ixhua',
            'to': '52'+ loan.client.family_reference_phone,
            'text': 'Hola soy Marìa de ixhua.mx, '+ str(loan.client.names) + ' ' + str(loan.client.father_last_name) + ' està haciendo un tramite con nosotros y te puso como su referencia. ¿Nos ayudas a contestar un breve formulario? da clic en este enlace: ' + settings.SURVEY_REFERENCE
            })
        except:
            pass


    messages.success(request, 'Se han enviado los SMS')
    return HttpResponse(json.dumps({'success': True}), content_type="application/json")


def ChangeForDispersedStatusKiban(loan_id):

    id = loan_id
    for_disperse = ForDispersed.objects.get(pk=id)
    for_disperse.state = 1
        
        #fecha de dispersion con el cambio de zona horaria
    try:
        utcnow = pytz.timezone('utc').localize(datetime.utcnow())
        here = utcnow.astimezone(pytz.timezone('America/Mexico_City')).replace(tzinfo=None)
        for_disperse.dispersion_date = here.date()     
    except:
        for_disperse.dispersion_date = date.today()
        
    for_disperse.save()

    seeds_count = 0

    try:
        seeds_count =  for_disperse.loan.client.seeds.yellow
    except:
        pass

    try:      
        for_disperse.loan.client.subfamily="RECURRENTE-"+str(seeds_count)
        for_disperse.loan.client.save()
    except:
        pass

    pending = PendingPayment.objects.create(loan=for_disperse.loan, amount_total=for_disperse.loan.amount_total)

    send_approved(for_disperse.loan.client.email, for_disperse.loan.payment_date, for_disperse.loan.amount_total, for_disperse.loan)

    try:
        send_dispersion_notification(for_disperse.loan.client.names,for_disperse.loan.client.father_last_name,for_disperse.loan.client.rfc,for_disperse.loan.request_no,for_disperse.loan.amount_requested)
    except:
        pass
                
    try:
        send_sms_dispersion_survey(for_disperse.loan.client.names, for_disperse.loan.client.cellphone)
    except:
        pass
                
    collectors = CustomUser.objects.filter(role=3)
            
    for collector in collectors:
        send_collectors(collector, for_disperse.loan.client, for_disperse.loan)
  
    return HttpResponse(json.dumps({'success': True}), content_type="application/json")
    
#=======================================================
#           CONEXION KIBAN
#=======================================================
def kiban_disperse(request):

    if request.POST:
        print("entra funcion disperse")
        try:
            id = request.POST.get('id')
            loan = Loan.objects.select_related('client').get(pk=id)
        except:
            messages.error(request, 'El numero de la solicitud no existe.')
            return HttpResponse(json.dumps({'success': True}), content_type="application/json")

        if loan.idUnykoo:
            messages.warning(request, 'Esta solicitud ya fue asignada a un Workflow de Kiban, intenta actualizar el estatus')
            return HttpResponse(json.dumps({'success': True}), content_type="application/json")

        #VARIABLES PARA LA CONECCION CON KIBAN
        url_kiban = settings.KIBAN_URL
        user_kiban = settings.KIBAN_USER
        company_code_kiban = settings.KIBAN_COMPANY_CODE
        api_key_kiban = settings.KIBAN_API_KEY
            
        url = str(url_kiban)+'/api/v1/workfloo'
        headers = {'Content-Type': 'application/json','company_code':str(company_code_kiban),'api_key': str(api_key_kiban),}
        myobj = {"login":str(user_kiban),"workflowName":"RECURRENTE-5"}
        print(loan.client.subfamily)

        #SOLO APLICA PARA LOS CLIENTES DONDE SU SUBFAMILIA SEA RECURRENTE, EN EL FUTURO SOLO TIENE QUE APLICAR PARA LOS RECURRENTES-5
        if (loan.client.subfamily == 'RECURRENTE-0') or (loan.client.subfamily == 'RECURRENTE-1') or (loan.client.subfamily == 'RECURRENTE-2') or (loan.client.subfamily == 'RECURRENTE-3') or (loan.client.subfamily == 'RECURRENTE-4') or (loan.client.subfamily == 'RECURRENTE-5'):      
            try:
                #POST PARA INICIAR EL WORKFLOW
                workfloo_request = requests.post(url,headers=headers,data=json.dumps(myobj))
                workfloo_response = json.loads(workfloo_request.text)

                if workfloo_response['success'] == True:
                    id_workfloo = workfloo_response['data']['idUnykoo'] #ID DE WORKFLOW
                    loan.idUnykoo = int(id_workfloo) #SE GUARDA EL ID DE KIBAN EN EL REGISTRO
                    url = str(url_kiban)+'/api/v1/form' #URL PARA FORMULARIO
                        
                    #SE LLENA EL FORMULARIO DE KIBAN PARA LA DISPERSION

                    #CONVERTIR ZONA HORARIA A MEXICO
                    try:
                        utcnow = pytz.timezone('utc').localize(datetime.utcnow())
                        here = utcnow.astimezone(pytz.timezone('America/Mexico_City')).replace(tzinfo=None)
                        for_disperse_date = here.date()     
                    except:
                        for_disperse_date = date.today()

                    myobj = {
                        "login":str(user_kiban),
                        "idUnykoo": id_workfloo,
                        "formName": 'Formulario 9 - Dispersión STP',
                        "data": {
                            "stp_dispersion_clave_rastreo": str(loan.request_no),
                            "stp_dispersion_concepto_pago": str(loan.request_no)+' '+"TU PRESTAMO IXHUA",
                            "stp_dispersion_cuenta_beneficiario": str(loan.client.bank_clabe),
                            "stp_dispersion_fecha_operacion": str(for_disperse_date),
                            #"stp_dispersion_folio_origen": '000001',
                            "stp_dispersion_institucion_contraparte": loan.client.bank_code_kiban,
                            "stp_dispersion_monto": loan.amount_requested,
                            "stp_dispersion_nombre_beneficiario": str(loan.client.names) + ' ' + str(loan.client.father_last_name) + ' ' + str(loan.client.mother_last_name),
                            "stp_dispersion_referencia_numerica": str(loan.request_no)[-7:],
                            "stp_dispersion_curp_beneficiario": loan.client.curp,
                            "stp_dispersion_tipo_cuenta_beneficiario": 40,
                            }
                    }

                    form_request = requests.post(url,headers=headers,data=json.dumps(myobj))
                    form_response = json.loads(form_request.text)
                    messages.success(request, 'Se han enviado los datos de dispersión a Kiban, idUnykoo:'+id_workfloo)
        
                else:
                    messages.error(request, 'Error al iniciar el workflow de Kiban')
            except:
                messages.error(request, 'Error en el proceso del workflow de Kiban')
                pass
        loan.save() 
        return HttpResponse(json.dumps({'success': True}), content_type="application/json")


def get_kiban_wokflow(request):
    if request.POST:
        try:
            id = request.POST.get('id')
            loan = Loan.objects.select_related('client').get(pk=id)
        except:
            messages.error(request, 'El numero de la solicitud no existe.')
            return HttpResponse(json.dumps({'success': False}), content_type="application/json")

        #VARIABLES PARA LA CONECCION CON KIBAN
        url_kiban = settings.KIBAN_URL
        user_kiban = settings.KIBAN_USER
        company_code_kiban = settings.KIBAN_COMPANY_CODE
        api_key_kiban = settings.KIBAN_API_KEY
            
        if loan.idUnykoo:
            url_status = str(url_kiban)+'/api/v1/workfloo/status/'+str(loan.idUnykoo)
            url_workflow_info = str(url_kiban)+'/api/v1/workfloo/'+str(loan.idUnykoo)
            headers = {'Content-Type': 'application/json','company_code':str(company_code_kiban),'api_key': str(api_key_kiban),}
            
            #CONSULTAR EL ESTATOS GENERAL DEL WORKFLOW
            workfloo_request = requests.get(url_status,headers=headers)
            workfloo_response = json.loads(workfloo_request.text)

            
            #SI EL LOAN YA TIENE UN ESTATOS DE SUCCESS SOLO MUESTRA EL MENSAJE
            if (loan.workflow_status == 'SUCCESS') or (loan.workflow_status == 'OK') or (loan.workflow_status == 'Transferencia STP exitosa') :
                messages.success(request, 'El workfloo se finalizó de manera exitosa')
                return HttpResponse(json.dumps({'success': True}), content_type="application/json")
            else:             
                #SI AL MOMENTO DE CONSULTAR EL WORKFLOW REGRESA UN ERROR MUESTRA EL MENSAJE
                try:
                    if workfloo_response['errorType']:
                        messages.error(request, 'El estatus de Kiban es: ' + str(workfloo_response['errorType']))
                        return HttpResponse(json.dumps({'success': True}), content_type="application/json")
                except:
                    pass
                
                try:
                    if workfloo_response['data']['status_unykoo']:
                        #SI EL WORKFLOW YA TERMINÓ SE TIENE QUE CONSULTAR EL ARBOL DE DECISION
                        if workfloo_response['data']['status_unykoo'] == 'SUCCESS':
                            
                            #SI EL WORKFLOW HA FINALIZADO CON UN SUCCESS, TIENE QUE CONSULTAR LA INFORMACION COMPLETA
                            workflow_info_request = requests.get(url_workflow_info,headers=headers)
                            workflow_info_response = json.loads(workflow_info_request.text)
                            #LA RESPUESTA DEL WORKFLOW ESTA DIVIDIDA EN UNA TUPLA DE 3 POSICIONES
                            #1.- Workflow
                            #2.- Formulario
                            #3.- Arbol
                            try:
                                #EL ARBOL TIENE QUE DEVOLVER UN MENSAJE
                                if workflow_info_response['data'][3]['arbolMessage']['message']:
                                    #SE ASIGNA EL MENSAJE EL EL CAMPO DE LOAN
                                    loan.workflow_status = workflow_info_response['data'][3]['arbolMessage']['message']
                                    #SI EL ARBOL RESPONDE OK, ENTONCES SE GUARDA LA DISPERSION
                                    if (workflow_info_response['data'][3]['arbolMessage']['message'] == 'OK') or (workflow_info_response['data'][3]['arbolMessage']['message'] == 'Transferencia STP exitosa') :
                                        try:
                                            loan_disperse = ForDispersed.objects.get(loan=loan.id)
                                            ChangeForDispersedStatusKiban(loan_disperse.id)
                                        except:
                                            messages.error(request, 'La solicitud no se pudo marcar como dispersada')
                                            return HttpResponse(json.dumps({'success': False}), content_type="application/json")
                            except:
                                messages.error(request, 'Ocurrio un problema durante la consulta del arbol de decision')
                                pass
                except:
                    pass
                
                loan.save()

        else:
            messages.warning(request, 'Esta solicitud no tiene un idUnykoo')
            return HttpResponse(json.dumps({'success': False}), content_type="application/json")


    messages.success(request, 'El estatus de Kiban es: ' + str(loan.workflow_status))
    return HttpResponse(json.dumps({'success': True}), content_type="application/json")



#         email = EmailMessage(
#                     'Pago exitoso',
#         """
# Hola {0}. <br>
# <br>
# ¡Muchas gracias por el pago de tu crédito {1} por la cantidad de ${2} con fecha de vencimiento el día {3}!<br>
# <br>
# Te recordamos que por cada pago puntual, accedes a mejores condiciones de pago. <br>
# <br>
# Cualquier duda o comentario, contáctanos vía email a <a href="mailto:contacto@ixhua.mx" style="color:#1C7BFF; text-decoration: underline;">contacto@ixhua.mx</a> o comunícate a nuestro whatsapp 331-913-6707 <br>
# <br>
# Consulta nuestro aviso de privacidad aquí <a href="{4}" style="color:#1C7BFF; text-decoration: underline;">{4}</a> 
#         """.format(
#                 "{} {} {}".format(client.names, client.father_last_name, client.mother_last_name),
#                 loan.request_no,
#                 # loan.amount_total,
#                 payment,
#                 loan.payment_date.strftime('%d/%m/%Y'),
#                 config('FRONT_URL') + 'privacy'),
#                     to=[client.email])
#         email.content_subtype = "html"
#         email.send()
