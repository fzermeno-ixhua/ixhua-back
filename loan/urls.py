from django.urls import path
from .views import ChangeLoanStatus, ChangeForDispersedStatus, ChangePendingPaymentStatus,send_loan_notification,send_survey_reference,get_kiban_wokflow,kiban_disperse

loan_patterns = ([
    path('change-loan-status', ChangeLoanStatus, name="change-loan-status"),
    path('change-disperse-status', ChangeForDispersedStatus, name="change-disperse-status"),
    path('change-payment-status', ChangePendingPaymentStatus, name="change-paymene-status"),
    path('send-payment-notification', send_loan_notification, name="send-payment-notification"),
    path('send-survey-reference', send_survey_reference, name="send-survey-reference"),
    path('kiban-disperse', kiban_disperse, name="kiban-disperse"),
    path('get-kiban-workflow', get_kiban_wokflow, name="get-kiban-workflow"),
], 'loans')