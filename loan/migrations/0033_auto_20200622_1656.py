# Generated by Django 3.0.4 on 2020-06-22 16:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loan', '0032_auto_20200508_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loan',
            name='contract_no',
            field=models.CharField(blank=True, max_length=14, null=True, verbose_name='Numero de contrato'),
        ),
    ]
