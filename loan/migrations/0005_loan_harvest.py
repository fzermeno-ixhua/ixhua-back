# Generated by Django 2.0.2 on 2019-08-07 17:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('harvest', '0002_auto_20190722_1810'),
        ('loan', '0004_auto_20190806_2115'),
    ]

    operations = [
        migrations.AddField(
            model_name='loan',
            name='harvest',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='harvest.Harvest'),
        ),
    ]
