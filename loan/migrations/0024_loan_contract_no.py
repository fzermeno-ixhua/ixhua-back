# Generated by Django 2.0.2 on 2019-10-10 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loan', '0023_fordispersed_dispersion_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='loan',
            name='contract_no',
            field=models.IntegerField(null=True, verbose_name='Numero de contrato'),
        ),
    ]
