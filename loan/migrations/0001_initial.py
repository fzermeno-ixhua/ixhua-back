# Generated by Django 2.0.2 on 2019-08-01 16:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('clients', '0008_auto_20190731_1424'),
    ]

    operations = [
        migrations.CreateModel(
            name='Loan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('request_no', models.CharField(max_length=25, verbose_name='No. de solicitud')),
                ('amount_requested', models.IntegerField(default=0, verbose_name='Monto activo')),
                ('state', models.IntegerField(choices=[(1, 'Pendiente de revisión'), (2, 'Aprobado'), (3, 'Rechazado')], default=1)),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='clients.Client')),
            ],
            options={
                'verbose_name': 'Préstamo',
                'verbose_name_plural': 'Préstamos',
                'ordering': ['request_no'],
            },
        ),
    ]
