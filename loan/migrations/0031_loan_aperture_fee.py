# Generated by Django 3.0.3 on 2020-02-25 14:10

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loan', '0030_auto_20200224_2300'),
    ]

    operations = [
        migrations.AddField(
            model_name='loan',
            name='aperture',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=7, validators=[django.core.validators.MinValueValidator(0)], verbose_name='Comisión apertura'),
        ),
    ]
