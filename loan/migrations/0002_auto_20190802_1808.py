# Generated by Django 2.0.2 on 2019-08-02 18:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loan', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loan',
            name='amount_requested',
            field=models.IntegerField(default=0, verbose_name='Monto Solicitado'),
        ),
    ]
