from django.contrib.postgres.fields import JSONField
from django.db import models
from dj.choices import Choices, Choice
from harvest.models import Harvest
from families.models import Family

from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator, MaxLengthValidator, RegexValidator

import boto

from storages.backends.s3boto3 import S3Boto3Storage
import uuid
from decouple import config
import datetime

class DownloadableS3Boto3Storage(S3Boto3Storage):

    def _save_content(self, obj, content, parameters):
        """
        The method is called by the storage for every file being uploaded to S3.
        Below we take care of setting proper ContentDisposition header for
        the file.
        """
        filename = obj.key.split('/')[-1]
        parameters.update({'ContentDisposition': f'attachment; filename="{filename}"'})
        return super()._save_content(obj, content, parameters)

class State(Choices):
    pending = Choice("Pendiente de revisión")
    approved = Choice("Aprobado")
    rejected = Choice("Rechazado")


class DispersedState(Choices):
    dispersed = Choice("Dispersada")
    not_dispersed = Choice("No dispersado")


class PendingPaymentState(Choices):
    on_time = Choice("A tiempo")
    expired = Choice("Vencidos")


# Create your models here.
class Loan(models.Model):
    PENDING = 1
    APPROVED = 2
    REJECTED = 3

    STATE_CHOICES = (
        (PENDING, 'Pendiente de revisión'),
        (APPROVED, 'Aprobado'),
        (REJECTED, 'Rechazado'),
    )

    request_no = models.CharField(verbose_name="No. de solicitud", max_length=25)
    amount_requested = models.IntegerField(verbose_name="Monto Solicitado (MXN)", default=0)
    aperture = models.DecimalField(
        verbose_name="Comisión apertura",
        decimal_places=2, max_digits=7, default=0,
        validators=[MinValueValidator(0)
    ])
    amount_total = models.DecimalField(verbose_name="Monto Total (MXN)", decimal_places=2, max_digits=7, default=0)
    state = models.IntegerField(verbose_name="Estado", choices=STATE_CHOICES, default=PENDING)
    option = models.PositiveIntegerField(verbose_name="Opcion", validators=[MinValueValidator(1)], default=1)
    payment_date = models.DateField(verbose_name="Fecha de Pago", null=True)
    term = models.PositiveIntegerField(verbose_name="Plazo", default=0)
    client = models.ForeignKey('clients.Client', on_delete=models.CASCADE, null=True, blank=True, related_name='client_loans')
    harvest = models.ForeignKey(Harvest, on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    reason_for_rejection = models.TextField(verbose_name="Motivo de rechazo", max_length=500, null=True)

    # Fields for ontract
    contract_no = models.CharField(verbose_name="Numero de contrato", max_length=14, null=True, blank=True)
    # contract_no = models.IntegerField(verbose_name="Numero de contrato", null=True, blank=True)
    idUnykoo = models.IntegerField(verbose_name="idUnykoo", null=True, blank=True)
    workflow_status = models.TextField(verbose_name="Workflow Estatus", max_length=500, null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    requirements = JSONField(verbose_name="Requisitos", null=True, default=None)



    sthree = S3Boto3Storage()

    def file_name(self, filename):
        ext = filename.split('.')[-1]
        name = "%s/%s.%s" % (config('INSTANCE'), uuid.uuid4(), ext)

        return name

    @property
    def amount_paid(self):
        "Returns paid amount."
        return sum(p.amount for p in self.payments.all())

    @property
    def amount_held(self):
        "Returns credit held amount."
        paid = self.amount_paid
        comisions = self.amount_total - self.amount_requested
        capital_paid = max(paid - comisions, 0)
        return max(self.amount_requested - capital_paid, 0)

    contract = models.FileField(verbose_name="Contrato", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())

    class Meta:
        ordering = ['request_no']
        verbose_name = 'Reporte de solicitud'
        verbose_name_plural = '1 - Reporte de solicitudes'

    def __unicode__(self):
        return u"{0}".format(self.request_no)

    def __str__(self):
        return u"{0}".format(self.request_no)


class ForDispersed(models.Model):
    state = models.IntegerField(verbose_name="Estatus", choices=DispersedState())
    loan = models.ForeignKey(Loan, verbose_name="No. de solicitud", on_delete=models.CASCADE, null=True, blank=True)
    dispersion_date = models.DateField(verbose_name="Fecha de Dispersión", null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Reporte de solicitud autorizada pendiente por dispersar'
        verbose_name_plural = '2 - Reporte de solicitudes autorizadas pendientes por dispersar'

    def __unicode__(self):
        return u"{0}".format(self.loan)

    def __str__(self):
        return u"{0}".format(self.loan)


class PendingPayment(models.Model):
    loan = models.ForeignKey(Loan, verbose_name="No. de solicitud", on_delete=models.CASCADE, null=True, blank=True)
    state = models.IntegerField(verbose_name="Estatus", choices=PendingPaymentState(), default=PendingPaymentState.on_time.id)
    paid_out = models.BooleanField(verbose_name="Liquidado", default=False)
    payday = models.DateField(verbose_name="Fecha de Pago", null=True)
    amount_total = models.DecimalField(verbose_name="Por pagar", decimal_places=2, max_digits=7, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Reporte de solicitud autorizada pendiente de pago'
        verbose_name_plural = '3 - Reporte de solicitudes autorizadas pendientes de pago'

    def __unicode__(self):
        return u"{0}".format(self.loan)

    def __str__(self):
        return u"{0}".format(self.loan)



class Seeds(models.Model):
    loan = models.ForeignKey(Loan, verbose_name="No. de solicitud", on_delete=models.CASCADE, null=True, blank=True)
    gray = models.IntegerField(verbose_name="Semillas Grises")
    green = models.IntegerField(verbose_name="Semillas Verdes")
    yellow = models.IntegerField(verbose_name="Semillas Amarillas")
    created_at = models.DateTimeField(default=datetime.datetime.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Semilla'
        verbose_name_plural = 'Semillas'

    def __unicode__(self):
        return u"{0}".format(self.loan)

    def __str__(self):
        return u"{0}".format(self.loan)


class PartialPayment(models.Model):
    loan = models.ForeignKey(Loan, verbose_name="No. de solicitud", on_delete=models.CASCADE, null=False)
    partial_no = models.IntegerField(verbose_name="No. de abono ", null=False)
    payday = models.DateField(verbose_name="Fecha de Pago", null=False)
    amount = models.DecimalField(verbose_name="Monto (MXN)", decimal_places=2, max_digits=7, default=0)
    #state = models.IntegerField(verbose_name="Estatus", choices=PendingPaymentState(), default=PendingPaymentState.on_time.id)
    #paid_out = models.BooleanField(verbose_name="Liquidado", default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Reporte de pago parcial'
        verbose_name_plural = '4 - Reporte de pagos parciales'
        default_related_name = 'payments'

    def __unicode__(self):
        return u"{0}".format(self.loan)

    def __str__(self):
        return u"{0}".format(self.loan)


class BackupLoan(models.Model):
    request_no = models.CharField(verbose_name="No. de solicitud", max_length=25,null=True,blank=True)
    amount_requested = models.IntegerField(verbose_name="Monto Solicitado (MXN)", default=0,null=True,blank=True)
    aperture = models.DecimalField(verbose_name="Comisión apertura",decimal_places=2, max_digits=7, default=0,validators=[MinValueValidator(0)])
    amount_total = models.DecimalField(verbose_name="Monto Total (MXN)", decimal_places=2, max_digits=7, default=0)
    state = models.CharField(verbose_name="Estado", max_length=25,null=True,blank=True)
    option = models.PositiveIntegerField(verbose_name="Opcion", validators=[MinValueValidator(1)], default=1)
    payment_date = models.DateField(verbose_name="Fecha de Pago",null=True,blank=True)
    term = models.PositiveIntegerField(verbose_name="Plazo", default=0,null=True,blank=True)
    created_at = models.DateTimeField(verbose_name="Fecha y hora", null=True,blank=True)
    reason_for_rejection = models.TextField(verbose_name="Motivo de rechazo", max_length=500,null=True,blank=True)
    contract_no = models.CharField(verbose_name="Numero de contrato", max_length=14, null=True, blank=True)

    def file_name(self, filename):
        ext = filename.split('.')[-1]
        name = "%s/%s.%s" % (config('INSTANCE'), uuid.uuid4(), ext)

    contract = models.FileField(verbose_name="Contrato", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    names = models.CharField(verbose_name="Nombres", max_length=50, null=True,blank=True)
    father_last_name = models.CharField(verbose_name="Apellido paterno", max_length=50, null=True,blank=True)
    mother_last_name = models.CharField(verbose_name="Apellido materno", max_length=50, null=True,blank=True)
    gender = models.CharField(verbose_name="Género", max_length=10, null=True, blank=True)
    birthdate = models.DateField(verbose_name="Fecha Nacimiento", null=True, blank=True)
    nationality = models.CharField(verbose_name="Nacionalidad", max_length=20, null=True, blank=True)
    civil_status = models.CharField(verbose_name="Estado civil", max_length=30, null=True, blank=True)
    address = models.CharField(verbose_name="Domicilio", max_length=255,null=True, blank=True)
    outdoor_no = models.CharField(verbose_name="No. ext.", max_length=50, null=True, blank=True)
    interior_no = models.CharField(verbose_name="No. int.", max_length=50, null=True, blank=True)
    postal_code = models.CharField(verbose_name="Codigo Postal", max_length=5, validators=[MinLengthValidator(5), RegexValidator('^[0-9]*$')], null=True, blank=True)
    suburb = models.CharField(verbose_name="Colonia", max_length=255, null=True, blank=True)
    city = models.CharField(verbose_name="Municipio", max_length=255, null=True, blank=True)
    state_country = models.CharField(verbose_name="Estado", max_length=255, null=True, blank=True)
    country = models.CharField(verbose_name="Pais", max_length=255, null=True, blank=True)
    house_type = models.CharField(verbose_name="Tipo de vivienda", max_length=20, null=True, blank=True)
    years_residence = models.DecimalField(verbose_name="Años de residencia", validators=[MaxValueValidator(99)],decimal_places=2,max_digits=7,default=0)
    cellphone = models.CharField(verbose_name="Celular", null=True,blank=True ,max_length=10,validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    phone = models.CharField(verbose_name="Telefono fijo", null=True,blank=True ,max_length=10, validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    email = models.EmailField(null=True, blank=True)
    birth_place = models.CharField(verbose_name="Lugar de nacimiento", max_length=50, null=True, blank=True)
    curp = models.CharField(verbose_name="CURP", max_length=18, blank=True, null=True, validators=[MinLengthValidator(18)])
    rfc = models.CharField(verbose_name="RFC", max_length=13, blank=True, null=True, validators=[MinLengthValidator(12)])
    study_level = models.CharField(verbose_name="Grado de estudios", max_length=50, null=True, blank=True)
    family = models.CharField(verbose_name="Familia", max_length=255, null=True, blank=True)
    family_str = models.CharField(verbose_name="Familia ingresada por el cliente", max_length=255, null=True, blank=True)
    actual_position = models.CharField(verbose_name="Puesto Actual", max_length=50, null=True, blank=True)
    labor_old = models.PositiveIntegerField(verbose_name="Antigüedad",  validators=[MaxValueValidator(99)], default=0)
    labor_old_month = models.PositiveIntegerField(verbose_name="Antigüedad (Meses)",  validators=[MaxValueValidator(11)], default=0)
    salary = models.DecimalField(verbose_name="Salario Mensual (MXN)", decimal_places=2,null=True, blank=True, max_digits=7)
    family_income = models.DecimalField(verbose_name="Ingreso familiar acumulado (MXN)", decimal_places=2, max_digits=10, default=0)
    economic_dependent = models.IntegerField(verbose_name="Dependiente económicos", default=0)
    bank_name = models.CharField(verbose_name="Nombre del Banco", max_length=50, null=True, blank=True)
    bank_clabe = models.CharField(verbose_name="CLABE", max_length=20, null=True, blank=True)
    credit_bureau = models.CharField(verbose_name="¿Has presentado atrasos en Buró de Crédito?", max_length=20, null=True, blank=True)
    credit_lines = models.CharField(verbose_name="¿Cuentas con alguna línea de crédito?", max_length=255, null=True, blank=True)
    tell_us = models.TextField(verbose_name="¡Platícanos! ¿Para qué usarías tu préstamo?", null=True, blank=True)
    reference = models.CharField(verbose_name="Referencia no familiar", max_length=200, null=True, blank=True)
    reference_phone = models.CharField(verbose_name="Teléfono referencia no familiar", null=True, unique=False,blank=True ,max_length=10,validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    family_reference = models.CharField(verbose_name="Referencia familiar", max_length=200, null=True, blank=True)
    family_reference_phone = models.CharField(verbose_name="Teléfono referencia familiar", null=True, unique=False,blank=True ,max_length=10,validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    notes = models.TextField(verbose_name="Datos adicionales", max_length=500, null=True, blank=True)
    ine = models.FileField(verbose_name="Copia ID vigente", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    bank_account = models.FileField(verbose_name="Copia o foto de la cuenta de banco", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    proof_address = models.FileField(verbose_name="Copia comprobante de domicilio", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    additional_file = models.FileField(verbose_name="Documento Adicional", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    paysheet_one =models.FileField(verbose_name="Último recibo de nómina", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    paysheet_two =models.FileField(verbose_name="Penúltimo recibo de nómina", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    paysheet_three =models.FileField(verbose_name="Antepenúltimo recibo de nómina", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    selfie =models.FileField(verbose_name="Selfie", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())

    class Meta:
        verbose_name = 'Respaldo de solicitud'
        verbose_name_plural = '5 - Respaldo de solicitudes'

    def __unicode__(self):
        return u"{0}".format(self.request_no)

    def __str__(self):
        return u"{0}".format(self.request_no)

class Requirements(models.Model):
    """ 
    loan_requeriments table. 
    Catalog used to define each requeriment needed to ask for a salary advance

    [ralated_tables]
        loan_requirements_family
        loan_requirements_client

    Added-Dev on: [2020-12-16]
    Added-Prod on: 

    """
    ACTIVE = 'AC'
    INACTIVE = 'IN'
    REQSTATUS_CHOICES = [ (ACTIVE, 'ACTIVE'), (INACTIVE, 'INACTIVE') ]

    name = models.CharField(verbose_name="Nombre", max_length=50, unique=True)
    description = models.CharField(verbose_name="Descripción", max_length=150, null=True, blank=True)
    html_tag = models.CharField(verbose_name="Etiqueta HTML", max_length=50, unique=True)
    status = models.CharField(verbose_name="Status", max_length=2, choices=REQSTATUS_CHOICES, default=ACTIVE)
    created_at = models.DateTimeField(default=datetime.datetime.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Requerimiento'
        verbose_name_plural = 'Requerimientos'

    def __str__(self):
        return self.name

    def is_upperclass(self):
        return self.status in (self.ACTIVE,self.INACTIVE)  

    
class RequirementsFamily(models.Model):
    """ 
    loan_requeriments_family table. 
    One to One table to bridge many to many relation betwen requirements and family tables

    [ralated_tables]
        loan_requirements
        families_family

    Added-Dev on: [2020-12-16]
    Added-Prod on: 

    """
    ACTIVE = 'AC'
    INACTIVE = 'IN'
    REQSTATUS_CHOICES = [ (ACTIVE, 'ACTIVE'), (INACTIVE, 'INACTIVE') ]

    requirement = models.ForeignKey(Requirements, verbose_name="Requerimiento", on_delete=models.CASCADE, null=False)
    family = models.ForeignKey(Family, verbose_name="Familia", on_delete=models.CASCADE, null=False, related_name='family_requirements')
    status = models.CharField(verbose_name="Status", max_length=2, choices=REQSTATUS_CHOICES, default=ACTIVE)
    created_at = models.DateTimeField(default=datetime.datetime.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Requerimiento por Familia'
        verbose_name_plural = 'Requerimientos por Familia' 

    def is_upperclass(self):
        return self.status in (self.ACTIVE,self.INACTIVE)        


class RequirementsClient(models.Model):
    """ 
    loan_requeriments_client table. 
    One to One table to bridge many to many relation betwen requirements and client tables

    [ralated_tables]
        loan_requirements
        clients_client

    Added-Dev on: [2020-12-16]
    Added-Prod on: 

    """
    ACTIVE = 'AC'
    INACTIVE = 'IN'
    REQSTATUS_CHOICES = [ (ACTIVE, 'ACTIVE'), (INACTIVE, 'INACTIVE') ]

    requirement = models.ForeignKey(Requirements, verbose_name="Requerimiento", on_delete=models.CASCADE, null=False)
    client = client = models.ForeignKey('clients.Client', verbose_name="Cliente", on_delete=models.CASCADE, related_name='client_requirements')
    status = models.CharField(verbose_name="Status", max_length=2, choices=REQSTATUS_CHOICES, default=ACTIVE)
    created_at = models.DateTimeField(default=datetime.datetime.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Requerimiento por Cliente'
        verbose_name_plural = 'Requerimientos por Cliente'



    def is_upperclass(self):
        return self.status in (self.ACTIVE,self.INACTIVE)