from django.contrib import admin
from .models import Loan, ForDispersed, PendingPayment, Seeds, PartialPayment, BackupLoan, Requirements, RequirementsFamily, RequirementsClient
from clients.models import Client, ClientDocuments, ClientAdjustment
from harvest.models import Harvest
from core.models import CustomUser
from django.utils.safestring import mark_safe
from project import settings
from loan.views import send_collectors

import boto
import datetime

from django.contrib.admin.views.main import ChangeList
from django.contrib.contenttypes.models import ContentType

from django.core.mail import EmailMessage
from decouple import config

from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from decimal import Decimal
from django.contrib.admin import SimpleListFilter
from django.urls import reverse
from django.utils.html import format_html

import sys


LoanState = {
    'Pendiente de revisión': 1,
    'Aprobado': 2,
    'Rechazado': 3,
}

PendingPaymentState = {
    'A tiempo': 1,
    'Vencidos': 2,
}

PendingPaymentPaidOut = {
    'Sí': True,
    'No': False,
}

DispersedState = {
    'Dispersada': 1,
    'No dispersado': 2,
}

@admin.register(Seeds)
class SeedsAdmin(admin.ModelAdmin):
    list_max_show_all = sys.maxsize
    list_per_page = 20

@admin.register(PartialPayment)
class PartialPaymentAdmin(admin.ModelAdmin):
    list_max_show_all = sys.maxsize
    list_per_page = 50
    list_display_links = None
    list_display = (
        'loan',
        'partial_no',
        'partial_payday',
        # 'amount'
        'partial_amount'
    )

    search_fields = (
        'loan__request_no',
        'partial_no',
        'payday',
        'amount'
    )


    def partial_payday(self, obj):
        return obj.payday.strftime('%d/%m/%Y')
    partial_payday.short_description = 'Fecha de pago'
    partial_payday.admin_order_field = 'payday'

    def partial_amount(self, obj):
        return 'MXN {:20,.2f}'.format(obj.amount)
    partial_amount.short_description = 'Monto (MXN)'
    partial_amount.admin_order_field = 'amount'


#Section Filter
class SectionFilterPaidOut(SimpleListFilter):
    title = 'Liquidado'

    parameter_name = 'showName'
    default_status = 0

    def lookups(self, request, model_admin):
        return (
            (2, 'Todo'),
            (1, 'Liquidado'),
            (0, 'No Liquidado'),
        )

    def default_value(self):
        return 0

    def choices(self, cl):  # Overwrite this method to prevent the default "All"
        from django.utils.encoding import force_text
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == force_text(lookup),
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        #import pdb; pdb.set_trace()
        if self.value() is None:
            return queryset.filter(paid_out=0)

        if self.value() == '0' or self.value() == None:
            return queryset.filter(paid_out=0)
        elif self.value() == '1':
            return queryset.filter(paid_out=1)
        elif self.value() == '2':
            return queryset

@admin.register(PendingPayment)
class PendingPaymentAdmin(admin.ModelAdmin):
    """ loan_pendingpayment table aAdmin

        Returns:
            [View]: [Generates a list of records from table]
    """

    actions = ['export_loan_pendingpayment']
    list_display_links = None # Descomentar para commit
    list_max_show_all = sys.maxsize
    list_per_page = 50
    list_display = (
        'partial_link', # Descomentar para commit
        # 'loan_request_no', # Comentar para commit
        'loan_client',
        'loan_client_fullnames',
        'loan_client_family',
        'loan_client_bank_clabe',
        #'loan_amount_requested',
        'loan_amount_total',
        'loan_remaining',
        'loan_dispersion_date',
        'loan_payment_date',
        'loan_term',
        # 'state', # Comentar para commit
        'loan_state',
        'loan_paid_out',
        'state_actions',
    )
    search_fields = (
        'loan__request_no',
        'loan__client__pk',
        'loan__client__names',
        'loan__client__father_last_name',
        'loan__client__mother_last_name',
        'loan__amount_requested',
        'loan__term',
    )
    list_filter =(SectionFilterPaidOut,)
    change_list_template = "admin/loan_daily_notification.html"
    

    def state_actions(self, obj):
        if not obj.paid_out:
                return mark_safe(
                    # '<a id="payment-model" style="float: none;" class="button default payment" data-id={0} data-action="paid" >Pagar</a>&nbsp;'.format(
                    '<a style="float: none;" class="button default payment" data-id={0} data-action="paid" >Pagar</a>&nbsp;'.format(
                        obj.id,
                    )
                )
        else:
                return mark_safe(
                    '<a style="float: none;" class="button default" disabled>Pagar</a>&nbsp;'
                )

    state_actions.short_description = ''

    def partial_link(self, obj):
        if obj.loan:
            url = '/admin/loan/loan/{}'.format(obj.loan_id)
            return format_html("<a href='{}'>{}</a>", url, obj.loan)
    partial_link.short_description = 'No. de solicitud'
    partial_link.admin_order_field = 'loan__request_no'

    def loan_request_no(self, obj):
        if obj.loan:
            return obj.loan.request_no
    loan_request_no.short_description = 'No. de solicitud'
    loan_request_no.admin_order_field = 'loan__request_no'

    def loan_client(self, obj):
        if obj.loan:
            if obj.loan.client:
                url = '/admin/clients/client/{}'.format(obj.loan.client.pk)
                return format_html("<a href='{}'>{}</a>", url, obj.loan.client.pk)
    loan_client.short_description = 'No. de cliente'
    loan_client.admin_order_field = 'loan__client__pk'

    def loan_client_family(self, obj):
        if obj.loan:
            return obj.loan.client.family
    loan_client_family.short_description = 'Familia'
    loan_client_family.admin_order_field = 'loan__client__family'

    def loan_client_bank_clabe(self, obj):
        if obj.loan:
            if obj.loan.client:
                if obj.loan.client.bank_clabe:
                    return obj.loan.client.bank_clabe
                else:
                    return '-'
    loan_client_bank_clabe.short_description = 'Clabe'
    loan_client_bank_clabe.admin_order_field = 'loan__client__bank_clabe'

    def loan_client_fullnames(self, obj):
        if obj.loan:
            return obj.loan.client.names + ' ' + obj.loan.client.father_last_name + ' ' + obj.loan.client.mother_last_name
    loan_client_fullnames.short_description = 'Nombre completo'
    loan_client_fullnames.admin_order_field = 'loan__client__names'

    def loan_amount_requested(self, obj):
        if obj.loan:
            return 'MXN {:20,.2f}'.format(obj.loan.amount_requested)
    loan_amount_requested.short_description = 'Monto total (MXN)'
    loan_amount_requested.admin_order_field = 'loan__amount_requested'

    def loan_amount_total(self, obj):
        if obj.loan:
            return 'MXN {:20,.2f}'.format(obj.loan.amount_total)
    loan_amount_total.short_description = 'Monto total (MXN)'
    loan_amount_total.admin_order_field = 'loan__amount_total'

    def loan_term(self, obj):
        if obj.loan:
            today = datetime.date.today()
            term = obj.loan.payment_date - today
            return str(term.days if term.days >= 0 else 0) + " Dias"
            return str(obj.loan.term) + " Dias"
    loan_term.short_description = 'Plazo'
    loan_term.admin_order_field = 'loan__payment_date'

    def loan_paid_out(self, obj):
        if obj.loan:
            if obj.paid_out:
                return 'Sí'
            else:
                return 'No'
    loan_paid_out.short_description = 'Liquidado'
    loan_paid_out.admin_order_field = 'paid_out'

    def loan_state(self, obj):
        if obj.loan:
            today = datetime.date.today()
            if not obj.paid_out :
                if today >= obj.loan.payment_date + datetime.timedelta(days=1):
                    obj.state = 2
                else:
                    obj.state = 1
                obj.save()
            return list(PendingPaymentState.keys())[list(PendingPaymentState.values()).index(obj.state)]
    loan_state.short_description = 'Estado'
    loan_state.admin_order_field = 'state'


    def loan_payment_date(self, obj):
        if obj.loan:
            return obj.loan.payment_date
    loan_payment_date.short_description = 'Fecha de pago'
    loan_payment_date.admin_order_field = 'loan__payment_date'

    def loan_dispersion_date(self, obj):
        if obj.loan:
            fordispersed = ForDispersed.objects.filter(loan=obj.loan).first()
            return fordispersed.dispersion_date
    loan_dispersion_date.short_description = 'Fecha de Dispersión'
    loan_dispersion_date.admin_order_field = 'loan__fordispersed__dispersion_date'


    def loan_remaining(self, obj):
        if obj.loan:
            amount = obj.loan.amount_requested
            days_late = (obj.loan.payment_date - datetime.date.today()).days

            if days_late < -10:
                days_late = -10

            if obj.paid_out or days_late > 0:
                days_late = 0

            days_late = abs(days_late)
            remaining = 0

            if days_late > 0:
                loan_payments = 0
                payments = PartialPayment.objects.filter(loan=obj.loan)

                for payment in payments:
                    loan_payments += payment.amount

                pending_amount = obj.loan.amount_total - loan_payments

                interest_moratorium = calculate_moratorium(obj.loan.client, days_late)
                moratorium = (float(pending_amount) * interest_moratorium * days_late)
                moratorium_iva = moratorium*0.16
                total_moratorium = moratorium + moratorium_iva
                final_total = float(obj.loan.amount_total) + total_moratorium

                TWOPLACES = Decimal(10) ** -2
                remaining = Decimal(final_total - float(loan_payments)).quantize(TWOPLACES)

                obj.amount_total = remaining
                obj.save()

            return 'MXN {:20,.2f}'.format(obj.amount_total)
    loan_remaining.short_description = 'Monto restante (MXN)'
    loan_remaining.admin_order_field = 'amount_total'

    def export_loan_pendingpayment(self, request, queryset):
        """ Build PendingPayment table export Report

        Returns:
            [file]: [csv file with useful data]
        """
        import csv
        from django.http import HttpResponse
    
        meta = self.model._meta
        #Title for columns
        field_names = ['No. de solicitud','Nombre completo','Celular','Correo','Familiar','Celular','Referencia','Celular','CLABE','Monto Adelanto (MXN)','Monto Total (MXN)','Monto Restante(MXN)','Fecha Dispersión','Fecha Vencimiento', 'Plazo','Estado', 'Liquidado']

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response, quoting=csv.QUOTE_ALL)

        writer.writerow(field_names)
        for obj in queryset:
            #Each row iterarion

            loan_request_no = 0
            client_names = ''
            client_cellphone = ''
            client_email = ''
            client_family = ''
            client_family_phone = ''
            client_reference = ''
            client_reference_phone = ''
            client_bank_clabe = ''
            loan_amount_requested = 0
            loan_amount_total = 0
            loan_amount_debt = 0
            loan_dispersion_date = ""
            loan_payment_date = ""
            loan_term = 0
            loan_payment_status = ''
            loan_paid_out = ''

            if obj.loan:
                #Variables value assignation

                loan_request_no = obj.loan.request_no
                client_names = obj.loan.client.names + ' ' + obj.loan.client.father_last_name + ' ' + obj.loan.client.mother_last_name
                client_cellphone = obj.loan.client.cellphone
                client_email = obj.loan.client.email
                client_family = obj.loan.client.family_reference
                client_family_phone = obj.loan.client.family_reference_phone
                client_reference = obj.loan.client.reference
                client_reference_phone = obj.loan.client.reference_phone
                client_bank_clabe = str(obj.loan.client.bank_clabe)
                client_family = obj.loan.client.family
                client_subfamily = obj.loan.client.subfamily
                loan_amount_requested = '{:.2f}'.format(obj.loan.amount_requested)
                loan_amount_total = '{:.2f}'.format(obj.loan.amount_total)
                loan_amount_debt = '{:.2f}'.format(float(self.loan_remaining(obj)[4:].replace(',','')))
                loan_dispersion_date = self.loan_dispersion_date(obj)
                loan_payment_date = obj.loan.payment_date
                loan_term = str(obj.loan.term) + " Dias"
                loan_payment_status = self.loan_state(obj)
                loan_paid_out = self.loan_paid_out(obj)

            # Write each row
            row = writer.writerow([loan_request_no, client_names, client_cellphone, client_email, client_family, client_family_phone, client_reference, client_reference_phone, client_bank_clabe,loan_amount_requested, loan_amount_total, loan_amount_debt, loan_dispersion_date, loan_payment_date, loan_term, loan_payment_status, loan_paid_out])

        return response

    export_loan_pendingpayment.short_description = "Exportar Registros Seleccionados"


    class Media:
        css = {
            'all': ('admin/css/sweetalert2.css',)
        }
        js = ('admin/js/vendor/jquery.min.js', 'admin/js/vendor/sweetalert2.min.js', 'admin/js/loan_pay.js', )

    #To filter by Choice
    #def get_search_results(self, request, queryset, search_term):
    #    queryset, use_distinct = super().get_search_results(request, queryset, search_term)
    #
    #    keys = [key for key, value in PendingPaymentState.items() if search_term.lower() in key.lower()]
    #    for key in keys:
    #        queryset |= self.model.objects.filter(state=PendingPaymentState[key])
    #
    #    keys = [key for key, value in PendingPaymentPaidOut.items() if search_term.lower() in key.lower()]
    #    for key in keys:
    #        queryset |= self.model.objects.filter(paid_out=PendingPaymentPaidOut[key])
    #
    #    return queryset, use_distinct


def calculate_moratorium(client, days):
    if days <= 6:
        loans = Loan.objects.filter(client=client)
        seed = Seeds.objects.filter(loan__in=list(loans)).order_by('-pk').first()
        adjustment = ClientAdjustment.objects.filter(client=client).order_by('-pk').first()


        if adjustment is None and seed is None:
            gray = 0
            green = 5
            yellow = 0
        elif adjustment is None:
            gray = seed.gray
            green = seed.green
            yellow = seed.yellow
        elif seed is None:
            gray = adjustment.gray
            green = adjustment.green
            yellow = adjustment.yellow
        else:
            if adjustment.created_at > seed.created_at:
                gray = adjustment.gray
                green = adjustment.green
                yellow = adjustment.yellow
            else:
                gray = seed.gray
                green = seed.green
                yellow = seed.yellow

        if yellow > 0:
            interest = 18 - yellow
        elif gray > 0:
            interest = 18 + gray
        else:
            interest = 18

        # if seed is not None:
        #     if seed.yellow > 0:
        #         interest = 18 - seed.yellow
        #     elif seed.gray > 0:
        #         interest = 18 + seed.gray
        #     else:
        #         interest = 18
        # else:
        #     interest = 18

        interest = interest / 100
        interest = (interest*12) / 360
    else:
        interest = 1.5 / 100

    return interest



def disperse_requests(modeladmin, request, queryset):
    selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    ct = ContentType.objects.get_for_model(queryset.model)
    for_dispersed = ForDispersed.objects.filter(pk__in=selected)
    collectors = CustomUser.objects.filter(role=3)

    for item in for_dispersed:
        if item.state != 1:
            item.state = 1
            item.dispersion_date = datetime.date.today()
            item.save()
            pending = PendingPayment.objects.create(loan=item.loan, amount_total=item.loan.amount_total)

            send_approved(item.loan.client.email, item.loan.payment_date, item.loan.amount_total, item.loan)

            for collector in collectors:
                    send_collectors(collector, item.loan.client, item.loan)
disperse_requests.short_description = "Dispersar solicitud autorizada pendiente por dispersar seleccionado/s"


def send_approved(client_email, payment_date, total, loan):
    subject = '¡Tu crédito fue aprobado!'
    html_message = render_to_string(
        'base.html',
        {
            'title': '¡Felicidades!',
            'content_template': 'content_approved.html',
            'contact_template': 'contact.html',
            'url': config('FRONT_URL') + "auth/login/",
            'payment_date': payment_date.strftime('%d/%m/%Y'),
            'amount': total
        }
    )
    plain_message = strip_tags(html_message)
    from_email = config('EMAIL_HOST_USER')
    to = client_email

    email = EmailMessage(subject, html_message, from_email, [to])

    email.content_subtype = "html"
    if loan.contract is not None:
        email.attach(loan.request_no + '.pdf', loan.contract.read())
    email.send()

        # mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
#         email = EmailMessage(
#                     'Préstamo aprobado',
#         """
# ¡Felicidades! Todos los datos de tu solicitud fueron correctos. <br>
# <br>
# El dinero debe de estar en menos de 3 horas en tu cuenta de banco. Tu fecha de pago es el día {0}, y te pedimos que ese día tengas el dinero disponible ya que cargaremos la cantidad de ${1} a tu cuenta en la que te depositamos. <br>
# <br>
# Te recordamos que puedes consultar tu perfil para revisar tu fecha de pago, montos y créditos autorizados que puedas disponer en <a href="{2}" style="color:#1C7BFF; text-decoration: underline;">{2}</a>  <br>
# <br>
# ¡Entre más puntual seas con tu pago, tendrás acceso a mejores tasas y a mayores montos!
#         """.format(payment_date.strftime('%d/%m/%Y'), total, config('FRONT_URL') + "auth/login/")
#                     ,
#                     to=[client_email])
#         email.content_subtype = "html"
#         email.attach(loan.request_no + '.pdf', loan.contract.read())
#         email.send()

#Section Filter
class SectionFilter(SimpleListFilter):
    title = 'Activo'

    parameter_name = 'showName'
    default_status = 2

    def lookups(self, request, model_admin):
        return (
            (0, 'Todo'),
            (1, 'Dispersados'),
            (2, 'No Dispersados'),
        )

    def default_value(self):
        return 2

    def choices(self, cl):  # Overwrite this method to prevent the default "All"
        from django.utils.encoding import force_text
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == force_text(lookup),
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        #import pdb; pdb.set_trace()
        if self.value() is None:
            return queryset.filter(state=2)

        if self.value() == '1' or self.value() == None:
            return queryset.filter(state=1)
        elif self.value() == '2':
            return queryset.filter(state=2)
        elif self.value() == '0':
            return queryset


@admin.register(ForDispersed)
class ForDispersedAdmin(admin.ModelAdmin):
    """ loan_fordispersed table aAdmin

        Returns:
            [View]: [Generates a list of records from table]
    """

    actions = [disperse_requests,'export_loan_dispersion']
    # To disable detail
    list_display_links = None
    readonly_fields = (
        # 'state',
        'loan',
        )
    search_fields = (
        'loan__request_no',
        'loan__client__pk',
        'loan__client__names',
        'loan__client__father_last_name',
        'loan__client__mother_last_name',
        'loan__client__family__tradename',
        'loan__client__subfamily',
        'loan__harvest__harvest_number',
        'loan__amount_requested',
        'loan__term',
        'state'
    )
    list_filter = (SectionFilter, )
    list_max_show_all = sys.maxsize
    list_per_page = 50
    list_display = (
        'loan_request_no',
        #'loan_client',
        'loan_client_rfc',
        'loan_client_fullnames',
        'loan_client_family',
        'loan_client_subfamily',
        #'loan_harvest',
        'loan_client_bank_clabe',
        'loan_payment_date',
        'loan_amount_requested',
        'loan_amount_total',
        'loan_term',
        'state',
        'state_actions',
        )


    def loan_request_no(self, obj):
        if obj.loan:
            url = reverse('admin:loan_loan_change', args=(obj.loan_id,))
            return format_html("<a href='{}'>{}</a>", url, obj.loan.request_no)
            #return obj.loan.request_no
    loan_request_no.short_description = 'No. de solicitud'
    loan_request_no.admin_order_field = 'loan__request_no'

    def loan_client(self, obj):
        if obj.loan:
            if obj.loan.client:
                return obj.loan.client.pk
    loan_client.short_description = 'No. de cliente'
    loan_client.admin_order_field = 'loan__client__pk'

    def loan_client_fullnames(self, obj):
        if obj.loan:
            return obj.loan.client.names + ' ' + obj.loan.client.father_last_name + ' ' + obj.loan.client.mother_last_name
    loan_client_fullnames.short_description = 'Nombre completo'
    loan_client_fullnames.admin_order_field = 'loan__client__names'

    def loan_client_rfc(self, obj):
        if obj.loan:
            url = '/admin/clients/client/{}'.format(obj.loan.client.pk)
            return format_html("<a href='{}'>{}</a>", url, obj.loan.client.rfc)
            #return obj.loan.client.rfc
    loan_client_rfc.short_description = 'RFC'
    loan_client_rfc.admin_order_field = 'loan__client__rfc'

    def loan_client_bank_clabe(self, obj):
        if obj.loan:
            return obj.loan.client.bank_clabe
    loan_client_bank_clabe.short_description = 'CLABE'
    loan_client_bank_clabe.admin_order_field = 'loan__client__bank_clabe'

    def loan_client_family(self, obj):
        if obj.loan:
            url = reverse('admin:families_family_change', args=(obj.loan.client.family_id,))
            return format_html("<a href='{}'>{}</a>", url, obj.loan.client.family)
            #return obj.loan.client.family
    loan_client_family.short_description = 'Familia'
    loan_client_family.admin_order_field = 'loan__client__family'

    def loan_client_subfamily(self, obj):
        if obj.loan:
            if obj.loan.client:
                return obj.loan.client.subfamily
    loan_client_subfamily.short_description = 'Subfamilia'
    loan_client_subfamily.admin_order_field = 'loan__client__subfamily'

    def loan_harvest(self, obj):
        if obj.loan:
            return obj.loan.harvest
    loan_harvest.short_description = 'Cosecha'
    loan_harvest.admin_order_field = 'loan__harvest'

    def loan_amount_requested(self, obj):
        if obj.loan:
            return 'MXN {:20,.2f}'.format(obj.loan.amount_requested)
    loan_amount_requested.short_description = 'Monto total'
    loan_amount_requested.admin_order_field = 'loan__amount_requested'

    def loan_amount_total(self, obj):
        if obj.loan:
            return 'MXN {:20,.2f}'.format(obj.loan.amount_total)
    loan_amount_total.short_description = 'Monto con comisión'
    loan_amount_total.admin_order_field = 'loan__amount_total'

    def loan_term(self, obj):
        if obj.loan:
            return str(obj.loan.term) + " Dias"
    loan_term.short_description = 'Plazo'
    loan_term.admin_order_field = 'loan__term'

    def loan_payment_date(self, obj):
        if obj.loan:
            return obj.loan.payment_date
    loan_payment_date.short_description = 'Fecha de Pago'
    loan_payment_date.admin_order_field = 'loan__payment_date'

    def state_actions(self, obj):
        if obj.state == 2:
                return mark_safe(
                    # '<a id="disperse-model" style="float: none;" class="button default" data-id={0} data-action="approved" >Dispersar</a>&nbsp;'.format(
                    '<button style="float: none;" class="button default disperse" data-id={0} data-action="approved" >Dispersar</button>&nbsp;'.format(
                        obj.id,
                    )
                )
        else:
                return mark_safe(
                    '<button style="float: none;" class="button default" disabled>Dispersar</button>&nbsp;'
                )

    state_actions.short_description = ''
    state_actions.admin_order_field = 'state'

    class Media:
        js = ('admin/js/vendor/jquery.min.js', 'admin/js/loan_disperse.js', ) 


    def export_loan_dispersion(self, request, queryset):
        """ Build for diserpsion export Report

        Returns:
            [file]: [csv file with same in screen fields]
        """
        import csv
        from django.http import HttpResponse
    
        meta = self.model._meta
        #Title for columns
        field_names = ['No. de solicitud','RFC','Nombre completo','CLABE','Familia','Subfamilia','Monto total','Monto con comisión','Plazo','Fecha de Pago']

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response, quoting=csv.QUOTE_ALL)

        writer.writerow(field_names)
        for obj in queryset:
            #Each row iterarion

            loan_request_no = 0
            client_names = ''
            client_rfc = ''
            client_bank_clabe = 0
            client_family = ''
            client_subfamily = ''
            loan_amount_requested = 0
            loan_amount_total = 0
            loan_term = 0
            loan_payment_date = 0

            if obj.loan:
                #Variables value assignation

                loan_request_no = obj.loan.request_no
                client_names = obj.loan.client.names + ' ' + obj.loan.client.father_last_name + ' ' + obj.loan.client.mother_last_name
                client_rfc = obj.loan.client.rfc
                client_bank_clabe = obj.loan.client.bank_clabe
                client_family = obj.loan.client.family
                client_subfamily = obj.loan.client.subfamily
                loan_amount_requested = '{:.2f}'.format(obj.loan.amount_requested)
                loan_amount_total = '{:.2f}'.format(obj.loan.amount_total)
                loan_term = str(obj.loan.term) + " Dias"
                loan_payment_date = obj.loan.payment_date

            # Write each row
            row = writer.writerow([loan_request_no, client_rfc, client_names, client_bank_clabe, client_family, client_subfamily, loan_amount_requested, loan_amount_total, loan_term, loan_payment_date])

        return response

    export_loan_dispersion.short_description = "Exportar Registros Seleccionados"



    #To filter by Choice
    #def get_search_results(self, request, queryset, search_term):
    #    queryset, use_distinct = super().get_search_results(request, queryset, search_term)
    #
    #    keys = [key for key, value in DispersedState.items() if search_term.lower() in key.lower()]
    #    for key in keys:
    #        queryset |= self.model.objects.filter(state=DispersedState[key])

    #    return queryset, use_distinct


#Section Filter
class SectionFilterLoan(SimpleListFilter):
    title = 'Estado'

    parameter_name = 'showName'
    default_status = 1

    def lookups(self, request, model_admin):
        return (
            (0, 'Todo'),
            (1, 'Pendiente de revisión'),
            (2, 'Aprobado'),
            (3, 'Rechazado'),
        )

    def default_value(self):
        return 1

    def choices(self, cl):  # Overwrite this method to prevent the default "All"
        from django.utils.encoding import force_text
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == force_text(lookup),
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        #import pdb; pdb.set_trace()
        if self.value() is None:
            return queryset.filter(state=1)

        if self.value() == '1' or self.value() == None:
            return queryset.filter(state=1)
        elif self.value() == '2':
            return queryset.filter(state=2)
        elif self.value() == '3':
            return queryset.filter(state=3)
        elif self.value() == '0':
            return queryset

# Register your models here.
@admin.register(Loan)
class LoanAdmin(admin.ModelAdmin):
    """ loan_loan table aAdmin

        Returns:
            [View]: [Generates a list of records from table]
    """


    list_max_show_all = sys.maxsize
    list_per_page = 50

    #list_display = ('request_no', 'client_pk', 'client_fullnames', 'client_rfc', 'client_family', 'client_subfamily', 'credit_amount', 'loan_option', 'state',)
    list_display = ('request_no', 'client_pk', 'client_fullnames', 'client_rfc', 'client_family', 'client_subfamily', 'credit_amount', 'loan_term', 'state',)
    readonly_fields = [
        'state_actions',
        'kiban_actions',
        # 'reason_for_rejection',
        'idUnykoo',
        'workflow_status',
        'request_no',
        'loan_create_date',
        'amount_requested',
        'aperture',
        'amount_total',
        # 'loan_harvest',
        # 'loan_harvest_amount',
        'payment_date',
        'term',
        'client_personal_info',
        'client_names',
        'client_father_last_name',
        'client_mother_last_name',
        'client_gender',
        'client_birthdate',
        'client_nationality',
        'client_civil_status',
        'client_address',
        'client_outdoor_no',
        'client_interior_no',
        'client_postal_code',
        'client_suburb',
        'client_city',
        'client_state',
        'client_country',
        'client_house_type',
        'client_years_residence',
        'client_phone',
        'client_cellphone',
        'client_email',
        'client_birth_place',
        'client_curp',
        'client_rfc',
        'client_study_level',
        'client_labor_info',
        'client_family',
        'client_family_str',
        'client_actual_position',
        'client_labor_old',
        'client_salary',
        'client_family_income',
        'client_economic_dependent',
        'client_bank_name',
        'client_bank_clabe',
        'client_credit_info',
        'client_credit_bureau',
        'client_credit_lines',
        'client_tell_us',
        'send_sms_reference',
        'client_reference',
        'client_reference_phone',
        'client_family_reference',
        'client_family_reference_phone',
        'requirements',
        'client_documents_info',
        'client_notes',
        'client_ine',
        'client_bank_account',
        'client_proof_address',
        'client_additional_file',
        'client_paysheet_one',
        'client_paysheet_two',
        'client_paysheet_three',
        'client_selfie',
        'client_contract',
        'reason_for_rejection',
    ]
    exclude = ['client', 'option', 'harvest', 'state', 'contract_no', 'contract','idUnykoo','workflow_status']
    # exclude = ['client', 'option', 'harvest', 'state', 'contract']
    # exclude = ['client', 'option']
    #search_fields = ('request_no', 'client__pk', 'client__rfc', 'client__family', 'client__subfamily', 'credit_amount', 'loan_option', 'state',)
    search_fields = ('request_no', 'client__pk', 'client__rfc', 'client__family__tradename', 'client__family__subfamily', 'client__credit_amount', 'term', 'option', 'state',)
    #ordering = ('request_no', 'client__pk', 'client__rfc', 'client__family__tradename', 'client__family__subfamily', 'client__credit_amount', 'option', 'state',)
    list_filter = (SectionFilterLoan, )
    ordering = ('-id','request_no', 'client__pk', 'client__rfc', 'client__family__tradename', 'client__family__subfamily', 'client__credit_amount', 'option', 'state',)
    actions = ['export_loan_loan']


    def state_actions(self, obj):
        if obj.id:
            harvest = Harvest.objects.filter(family__id=obj.client.family.id, active_amount__gte=obj.amount_requested).order_by('-active_amount')[:5]

            harvest_str = ""
            for x in harvest:
                harvest_str += x.harvest_number + ' / '+ '${:20,.2f}'.format(x.active_amount) + ";"

            harvest_str = harvest_str[:-1]

            if obj.state == 1:
                if harvest.count() == 0:
                    state = "disabled"
                else:
                    state = "enabled"

                return mark_safe(
                    '<a id="approved-model" style="float: none;" class="button default" data-id={0} data-action="approved" data-state="{1}" data-harvest="{2}"  {1}>Aceptar</a>&nbsp;'
                    '<a id="rejected-model" class="button default" style="float: none;"  data-id={0} data-action="rejected" data-state="{1}" data-harvest="{2}" {1}>Rechazar</a>'.format(
                        obj.id,
                        state,
                        harvest_str
                    )
                )
            else:
                if obj.state == 2:
                    return 'Aprobado'
                else:
                    return 'Rechazado'
        else:
            return 'Error'

    state_actions.short_description = 'Estatus'

    def kiban_actions(self, obj):
        
        if obj.id:
            if obj.state == 2:
                return mark_safe(
                    '<a id="kiban-get" class="button default" style="float: none;"  data-id={0} data-action="kibanget">Revisar Estatus</a>&nbsp;'
                    '<a id="kiban-disperse" class="button default" style="float: none;"  data-id={0} data-action="kibandisperse">Dispersar</a>'.format(obj.id)
                )
            else:
                #return("La solicitud tiene que estar aprobada para tener acceso a Kiban")
                return mark_safe(
                    '<p style="color:gray;">La solicitud tiene que ser aprobada para tener acceso a las funciones de Kiban</p>'
                )
        else:
            return 'Error'

    kiban_actions.short_description = 'Acciones Kiban'

    def send_sms_reference(self, obj):
        if obj.id:
            return mark_safe(
                '<a id="send-survey" style="float: none;" class="button default" data-id={0}>Enviar SMS</a>'.format(
                    obj.id,
                )
            )
        else:
            return 'Error'

    send_sms_reference.short_description = 'Contactar Referencias'


    def loan_harvest(self, obj):
        if obj.client:
            harvest = Harvest.objects.filter(family=obj.client.family, active_amount__gte=obj.amount_requested).order_by('-active_amount').first()
            if harvest is not None:
                return harvest
            else:
                return "Ninguna cosecha disponible"
    loan_harvest.short_description = 'Cosecha'

    def loan_harvest_amount(self, obj):
        if obj.client:
            harvest = Harvest.objects.filter(family=obj.client.family, active_amount__gte=obj.amount_requested).order_by('-active_amount').first()
            if harvest is not None:
                return harvest.active_amount
            else:
                return "Ninguna cosecha disponible"
    loan_harvest_amount.short_description = 'Monto Disponible en cosecha'

    def client_ine(self, obj):
        if obj.client:
            documents = ClientDocuments.objects.filter(client=obj.client)
            documents_date = '-'
            effective_date_color = 'black'
            if documents.count() > 0:
                documents = documents[0]
                #mostrar la fecha
                if documents.ine_effective_date:            
                    documents_date = documents.ine_effective_date
                    #validacion para saber si la fecha es valida y cambiar el color       
                    if datetime.date.today() <= documents_date:
                        effective_date_color="green"
                    elif datetime.date.today() > documents_date:
                        effective_date_color = "red"
                    else:
                        effective_date_color="black"
            else:
                documents = None

            return mark_safe('<a href="{}">Descargar</a> <p style="color:{};">Fecha de Vigencia: {}  </p>'.format(documents.ine.url,effective_date_color,documents_date))
    client_ine.short_description = 'Copia ID vigente'

    def client_bank_account(self, obj):
        if obj.client:
            documents = ClientDocuments.objects.filter(client=obj.client)
            documents_date = '-'
            effective_date_color = 'black'
            if documents.count() > 0:
                documents = documents[0]

                if documents.bank_account_effective_date:
                    documents_date = documents.bank_account_effective_date

                    #validacion para saber si la fecha es valida y cambiar el color       
                    if datetime.date.today() <= documents_date:
                        effective_date_color="green"
                    elif datetime.date.today() > documents_date:
                        effective_date_color = "red"
                    else:
                        effective_date_color="black"
            else:
                documents = None

            return mark_safe('<a href="{}">Descargar</a> <p style="color:{};">Fecha de Vigencia: {}  </p>'.format(documents.bank_account.url,effective_date_color,documents_date))
    client_bank_account.short_description = 'Copia o foto de la cuenta de banco'

    def client_proof_address(self, obj):
        if obj.client:
            documents = ClientDocuments.objects.filter(client=obj.client)
            documents_date = '-'
            effective_date_color = 'black'
            if documents.count() > 0:
                documents = documents[0]

                if documents.proof_address_effective_date:
                    documents_date = documents.proof_address_effective_date

                    #validacion para saber si la fecha es valida y cambiar el color       
                    if datetime.date.today() <= documents_date:
                        effective_date_color="green"
                    elif datetime.date.today() > documents_date:
                        effective_date_color = "red"
                    else:
                        effective_date_color="black"
            else:
                documents = None

            return mark_safe('<a href="{}">Descargar</a> <p style="color:{};">Fecha de Vigencia: {}  </p>'.format(documents.proof_address.url,effective_date_color,documents_date))
    client_proof_address.short_description = 'Copia comprobante de domicilio'

    def client_additional_file(self, obj):
        if obj.client:
            documents = ClientDocuments.objects.filter(client=obj.client)
            documents_date = '-'
            effective_date_color = 'black'
            if documents.count() > 0:
                documents = documents[0]

                if documents.additional_file_effective_date:
                    documents_date = documents.additional_file_effective_date
                    #validacion para saber si la fecha es valida y cambiar el color       
                    if datetime.date.today() <= documents_date:
                        effective_date_color="green"
                    elif datetime.date.today() > documents_date:
                        effective_date_color = "red"
                    else:
                        effective_date_color="black"
            else:
                documents = None

            return mark_safe('<a href="{}">Descargar</a> <p style="color:{};">Fecha de Vigencia: {}  </p>'.format(documents.additional_file.url,effective_date_color,documents_date))
    client_additional_file.short_description = 'Documento Adicional'

    def client_paysheet_one(self, obj):
        if obj.client:
            documents = ClientDocuments.objects.filter(client=obj.client)
            documents_date = '-'
            effective_date_color = 'black'
            if documents.count() > 0:
                documents = documents[0]

                if documents.paysheet_one_effective_date:
                    documents_date = documents.paysheet_one_effective_date
                    #validacion para saber si la fecha es valida y cambiar el color       
                    if datetime.date.today() <= documents_date:
                        effective_date_color="green"
                    elif datetime.date.today() > documents_date:
                        effective_date_color = "red"
                    else:
                        effective_date_color="black"
            else:
                documents = None

            return mark_safe('<a href="{}">Descargar</a> <p style="color:{};">Fecha de Vigencia: {}  </p>'.format(documents.paysheet_one.url,effective_date_color,documents_date))
    client_paysheet_one.short_description = 'Último recibo de nómina'

    def client_paysheet_two(self, obj):
        if obj.client:
            documents = ClientDocuments.objects.filter(client=obj.client)
            documents_date = '-'
            effective_date_color = 'black'
            if documents.count() > 0:
                documents = documents[0]

                if documents.paysheet_two_effective_date:
                    documents_date = documents.paysheet_two_effective_date
                    #validacion para saber si la fecha es valida y cambiar el color       
                    if datetime.date.today() <= documents_date:
                        effective_date_color="green"
                    elif datetime.date.today() > documents_date:
                        effective_date_color = "red"
                    else:
                        effective_date_color="black"
            else:
                documents = None

            return mark_safe('<a href="{}">Descargar</a> <p style="color:{};">Fecha de Vigencia: {}  </p>'.format(documents.paysheet_two.url,effective_date_color,documents_date))
    client_paysheet_two.short_description = 'Penúltimo recibo de nómina'

    def client_paysheet_three(self, obj):
        if obj.client:
            documents = ClientDocuments.objects.filter(client=obj.client)
            documents_date = '-'
            effective_date_color = 'black'
            if documents.count() > 0:
                documents = documents[0]

                if documents.paysheet_three_effective_date:
                    documents_date = documents.paysheet_three_effective_date
                    #validacion para saber si la fecha es valida y cambiar el color       
                    if datetime.date.today() <= documents_date:
                        effective_date_color="green"
                    elif datetime.date.today() > documents_date:
                        effective_date_color = "red"
                    else:
                        effective_date_color="black"
            else:
                documents = None

            return mark_safe('<a href="{}">Descargar</a> <p style="color:{};">Fecha de Vigencia: {}  </p>'.format(documents.paysheet_three.url,effective_date_color,documents_date))
    client_paysheet_three.short_description = 'Antepenúltimo recibo de nómina'

    def client_selfie(self, obj):
        if obj.client:
            documents = ClientDocuments.objects.filter(client=obj.client)
            documents_date = '-'
            effective_date_color = 'black'
            if documents.count() > 0:
                documents = documents[0]

                if documents.selfie_effective_date:
                    documents_date = documents.selfie_effective_date
                    #validacion para saber si la fecha es valida y cambiar el color       
                    if datetime.date.today() <= documents_date:
                        effective_date_color="green"
                    elif datetime.date.today() > documents_date:
                        effective_date_color = "red"
                    else:
                        effective_date_color="black"
            else:
                documents = None

            return mark_safe('<a href="{}">Descargar</a> <p style="color:{};">Fecha de Vigencia: {}  </p>'.format(documents.selfie.url,effective_date_color,documents_date))
    client_selfie.short_description = 'Selfie'

    def client_contract(self, obj):
        if obj.contract is not None:

            return mark_safe('<a href="{}">Descargar</a>'.format(obj.contract.url))
    client_contract.short_description = 'Contrato'

    def loan_option(self, obj):
        return '15 días' if obj.option == 1 else '30 días'
    loan_option.short_description = 'Plazo'

    def loan_term(self, obj):
        return str(obj.term) + ' días'
    loan_term.short_description = 'Plazo'
    loan_term.admin_order_field = 'term'

    def client_fullnames(self, obj):
        if obj.client:
            return obj.client.names + ' ' + obj.client.father_last_name + ' ' + obj.client.mother_last_name
    client_fullnames.short_description = 'Nombre completo'
    client_fullnames.admin_order_field = 'client__names'

    def client_pk(self, obj):
        if obj.client:
            url = '/admin/clients/client/{}'.format(obj.client.pk)
            return format_html("<a href='{}'>{}</a>", url, obj.client.pk)
            #return obj.client__pk
    client_pk.short_description = 'No. de cliente'
    client_pk.admin_order_field = 'client__pk'

    def client_personal_info(self, obj):
        return ''
    client_personal_info.short_description = 'Información Personal'

    def client_names(self, obj):
        if obj.client:
            return obj.client.names
    client_names.short_description = 'Nombre'

    def client_father_last_name(self, obj):
        if obj.client:
            return obj.client.father_last_name
    client_father_last_name.short_description = 'Apellido paterno'

    def client_mother_last_name(self, obj):
        if obj.client:
            return obj.client.mother_last_name
    client_mother_last_name.short_description = 'Apellido materno'

    def client_gender(self, obj):
        if obj.client:
            return obj.client.gender if obj.client.gender is not None else '-'
    client_gender.short_description = 'Género'

    def client_birthdate(self, obj):
        if obj.client:
            return obj.client.birthdate
    client_birthdate.short_description = 'Fecha de nacimiento'

    def client_nationality(self, obj):
        if obj.client:
            return obj.client.nationality if obj.client.nationality is not None else '-'
    client_nationality.short_description = 'Nacionalidad'

    def client_civil_status(self, obj):
        if obj.client:
            return obj.client.civil_status if obj.client.civil_status is not None else '-'
    client_civil_status.short_description = 'Estado civil'

    def client_address(self, obj):
        if obj.client:
            return obj.client.address
    client_address.short_description = 'Calle'

    def client_outdoor_no(self, obj):
        if obj.client:
            return obj.client.outdoor_no if obj.client.outdoor_no is not None else '-'
    client_outdoor_no.short_description = 'No. ext.'

    def client_interior_no(self, obj):
        if obj.client:
            return obj.client.interior_no if obj.client.interior_no is not None else '-'
    client_interior_no.short_description = 'No. int.'

    def client_postal_code(self, obj):
        if obj.client:
            return obj.client.postal_code if obj.client.postal_code is not None else '-'
    client_postal_code.short_description = 'C.P.'

    def client_suburb(self, obj):
        if obj.client:
            return obj.client.suburb if obj.client.suburb is not None else '-'
    client_suburb.short_description = 'Colonia'

    def client_city(self, obj):
        if obj.client:
            return obj.client.city if obj.client.city is not None else '-'
    client_city.short_description = 'Municipio'

    def client_state(self, obj):
        if obj.client:
            return obj.client.state if obj.client.state is not None else '-'
    client_state.short_description = 'Estado'

    def client_country(self, obj):
        if obj.client:
            return obj.client.country if obj.client.country is not None else '-'
    client_country.short_description = 'País'

    def client_house_type(self, obj):
        if obj.client:
            return obj.client.house_type if obj.client.house_type is not None else '-'
    client_house_type.short_description = 'Tipo de vivienda'

    def client_birth_place(self, obj):
        if obj.client:
            return obj.client.birth_place if obj.client.birth_place is not None else '-'
    client_birth_place.short_description = 'Lugar de nacimiento'

    def client_curp(self, obj):
        if obj.client:
            return obj.client.curp if obj.client.curp is not None else '-'
    client_curp.short_description = 'CURP'

    def client_rfc(self, obj):
        if obj.client:
            return obj.client.rfc
    client_rfc.short_description = 'RFC'
    client_rfc.admin_order_field = 'client__rfc'

    def client_study_level(self, obj):
        if obj.client:
            return obj.client.study_level if obj.client.study_level is not None else '-'
    client_study_level.short_description = 'Grado de estudios'

    def client_phone(self, obj):
        if obj.client:
            return obj.client.phone
    client_phone.short_description = 'Teléfono casa'

    def client_cellphone(self, obj):
        if obj.client:
            return obj.client.cellphone
    client_cellphone.short_description = 'Teléfono celular'

    def client_email(self, obj):
        if obj.client:
            return obj.client.email
    client_email.short_description = 'Correo electrónico'

    def client_years_residence(self, obj):
        if obj.client:
            return obj.client.years_residence if obj.client.years_residence is not None else '-'
    client_years_residence.short_description = 'Años de residencia'

    def client_labor_info(self, obj):
        return ''
    client_labor_info.short_description = 'Datos laborales'

    def client_family(self, obj):
        if obj.client:
            if obj.client.family:
                url = reverse('admin:families_family_change', args=(obj.client.family_id,))
                return format_html("<a href='{}'>{}</a>", url, obj.client.family)
    client_family.short_description = 'Familia'
    client_family.admin_order_field = 'client__family'

    def client_family_str(self, obj):
        if obj.client:
            if obj.client.family:
                return obj.client.family_str
    client_family_str.short_description = 'Familia ingresada por el cliente'
    # client_family.admin_order_field = 'client__family'

    def client_actual_position(self, obj):
        if obj.client:
            return obj.client.actual_position
    client_actual_position.short_description = 'Puesto'

    def client_labor_old(self, obj):
        if obj.client:
            return obj.client.labor_old if obj.client.labor_old is not None else '-'
    client_labor_old.short_description = 'Antigüedad'

    def client_salary(self, obj):
        if obj.client:
            return obj.client.salary
    client_salary.short_description = 'Sueldo mensual (MXN)'

    def client_family_income(self, obj):
        if obj.client:
            return obj.client.family_income if obj.client.family_income is not None else '-'
    client_family_income.short_description = 'Ingreso familiar acumulado (MXN)'

    def client_economic_dependent(self, obj):
        if obj.client:
            return obj.client.economic_dependent if obj.client.economic_dependent is not None else '-'
    client_economic_dependent.short_description = 'Dependiente económicos'

    def client_bank_name(self, obj):
        if obj.client:
            return obj.client.bank_name
    client_bank_name.short_description = 'Banco'

    def client_bank_clabe(self, obj):
        if obj.client:
            return obj.client.bank_clabe
    client_bank_clabe.short_description = 'Cuenta CLABE'

    def client_credit_info(self, obj):
        return ''
    client_credit_info.short_description = 'Datos crediticios'

    def client_credit_bureau(self, obj):
        if obj.client:
            return obj.client.credit_bureau if obj.client.credit_bureau is not None else '-'
    client_credit_bureau.short_description = '¿Has presentado atrasos en Buró de Crédito?'

    def client_tell_us(self, obj):
        if obj.client:
            return obj.client.tell_us if obj.client.tell_us is not None else '-'
    client_tell_us.short_description = '¿¡Platícanos! ¿Para qué usarías tu préstamo? '

    def client_credit_lines(self, obj):
        if obj.client:
            return obj.client.credit_lines.replace(",", ", ") if obj.client.credit_lines is not None else '-'
    client_credit_lines.short_description = '¿Cuentas con alguna línea de crédito? '

    def client_documents_info(self, obj):
        return ''
    client_documents_info.short_description = 'Documentos'

    def client_subfamily(self, obj):
        if obj.client:
            if obj.client.family:
                return obj.client.subfamily
    client_subfamily.short_description = 'Subfamilia'
    client_subfamily.admin_order_field = 'client__subfamily'

    def credit_amount(self, obj):
        if obj.client:
            return 'MXN {:20,.2f}'.format(obj.client.credit_amount)
    credit_amount.short_description = 'Monto total (MXN)'
    credit_amount.admin_order_field = 'client__credit_amount'

    def client_reference(self, obj):
        if obj.client:
            return obj.client.reference
    client_reference.short_description = 'Referencia no familiar'

    def client_reference_phone(self, obj):
        if obj.client:
            return obj.client.reference_phone
    client_reference_phone.short_description = 'Teléfono referencia no familiar'

    def client_family_reference(self, obj):
        if obj.client:
            return obj.client.family_reference
    client_family_reference.short_description = 'Referencia familiar'

    def client_family_reference_phone(self, obj):
        if obj.client:
            return obj.client.family_reference_phone
    client_family_reference_phone.short_description = 'Referencia familiar'

    def client_notes(self, obj):
        if obj.client:
            return obj.client.notes if obj.client.notes is not None else '-'
    client_notes.short_description = 'Datos adicionales'

    #AJUSTAR HORA EN EL CAMBIO DE HORARIO
    def loan_create_date(self, obj):
        if obj.created_at:
            try:
                date = obj.created_at - datetime.timedelta(hours=6)
                return date.strftime("%d/%m/%Y %H:%M:%S")
            except:
                return "-"
    loan_create_date.short_description = 'Fecha y Hora'

    def requirements_in_json(self, obj):
        if obj.requirements:
            return obj.requirements
    requirements_in_json.short_description = 'Requisitos'        

    def export_loan_loan(self, request, queryset):
        """ Build loan export Report

        Returns:
            [file]: [csv file with same in screen fields]
        """
        import csv
        from django.http import HttpResponse
    
        meta = self.model._meta
        #Title for columns
        field_names = ['No. de solicitud','Nombre completo','RFC','Familia','Subfamilia','Monto total','Plazo','Estado']

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response, quoting=csv.QUOTE_ALL)

        writer.writerow(field_names)
        for obj in queryset:
            #Each row iterarion

            loan_request_no = 0
            client_names = ''
            client_rfc = ''
            client_family = ''
            client_subfamily = ''
            loan_amount_requested = 0
            loan_term = 0
            loan_status = ''

            if obj:
                #Variables value assignation

                loan_request_no = obj.request_no
                client_names = obj.client.names + ' ' + obj.client.father_last_name + ' ' + obj.client.mother_last_name
                client_rfc = obj.client.rfc
                client_family = obj.client.family
                client_subfamily = obj.client.subfamily
                loan_amount_requested = '{:.2f}'.format(obj.amount_requested)
                loan_term = str(obj.term) + " Dias"
                if obj.state == 1:
                    loan_status = 'Pendiente de revisión'
                elif obj.state == 2:
                    loan_status = 'Aprobado'
                else:
                    loan_status = 'Rechazado'      

            # Write each row
            row = writer.writerow([loan_request_no, client_names, client_rfc, client_family, client_subfamily, loan_amount_requested, loan_term, loan_status])

        return response

    export_loan_loan.short_description = "Exportar Registros Seleccionados"


    class Media:
        css = {
            'all': ('admin/css/sweetalert2.css',)
        }
        js = ('admin/js/vendor/jquery.min.js', 'admin/js/vendor/sweetalert2.min.js', 'admin/js/loan_text_area.js', )


    #To filter by Choice
    #def get_search_results(self, request, queryset, search_term):
    #    queryset, use_distinct = super().get_search_results(request, queryset, search_term)
    #
    #    keys = [key for key, value in LoanState.items() if search_term.lower() in key.lower()]
    #    for key in keys:
    #        queryset |= self.model.objects.filter(state=LoanState[key])
    #
    #    return queryset, use_distinct

    # Limit harvest to the first 5 with enough active_amount
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        obj_id = request.META['PATH_INFO'].rstrip('/').split('/')[-2]
        obj = self.get_object(request, obj_id)

        if db_field.name == "harvest":
            kwargs["queryset"] = Harvest.objects.filter(family=obj.client.family, active_amount__gte=obj.amount_requested).order_by('-active_amount')[:5]

        return super(LoanAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

# from rest_framework.authtoken.models import Token
# admin.site.unregister(Token)
@admin.register(BackupLoan)
class BackupLoanAdmin(admin.ModelAdmin):
    list_max_show_all = sys.maxsize
    list_per_page = 50

    list_display = ('request_no','rfc','names','father_last_name','mother_last_name','family','amount_total','term','state')

    readonly_fields = [
        'state',
        'request_no',
        'contract_no',
        'loan_create_date',
        'amount_requested',
        'aperture',
        'amount_total',
        'payment_date',
        'term',
        'client_personal_info',
        'names',
        'father_last_name',
        'mother_last_name',
        'gender',
        'birthdate',
        'nationality',
        'civil_status',
        'address',
        'outdoor_no',
        'interior_no',
        'postal_code',
        'suburb',
        'city',
        'state_country',
        'country',
        'house_type',
        'years_residence',
        'phone',
        'cellphone',
        'email',
        'birth_place',
        'curp',
        'rfc',
        'study_level',
        'client_labor_info',
        'family',
        'family_str',
        'actual_position',
        'labor_old',
        'labor_old_month',
        'salary',
        'family_income',
        'economic_dependent',
        'bank_name',
        'bank_clabe',
        'client_credit_info',
        'credit_bureau',
        'credit_lines',
        'tell_us',
        'reference',
        'reference_phone',
        'family_reference',
        'family_reference_phone',
        'client_documents_info',
        'notes',
        #'client_ine',
        #'client_bank_account',
        #'client_proof_address',
        #'client_additional_file',
        #'client_paysheet_one',
        #'client_paysheet_two',
        #'client_paysheet_three',
        #'client_selfie',
        'client_contract',
        #'ine','bank_account','proof_address','additional_file','paysheet_one','paysheet_two','paysheet_three','selfie','contract',
        'reason_for_rejection',
    ]
    exclude = ['option','created_at','ine','bank_account','proof_address','additional_file','paysheet_one','paysheet_two','paysheet_three','selfie','contract',]
    search_fields = ('request_no','rfc','names','term', 'option', 'state',)

    ordering = ('-id','request_no',)



    def client_personal_info(self, obj):
        return ''
    client_personal_info.short_description = 'Información Personal'


    def client_labor_info(self, obj):
        return ''
    client_labor_info.short_description = 'Datos laborales'


    def client_credit_info(self, obj):
        return ''
    client_credit_info.short_description = 'Datos crediticios'


    def client_documents_info(self, obj):
        return ''
    client_documents_info.short_description = 'Documentos'

    def loan_create_date(self, obj):
        if obj.created_at:
            try:
                date = obj.created_at - datetime.timedelta(hours=5)
                return date.strftime("%d/%m/%Y %H:%M:%S")
            except:
                return "-"
    loan_create_date.short_description = 'Fecha y Hora'


    def client_ine(self, obj):
        if obj.ine:
            return mark_safe('<a href="{}" target="_blank">Descargar</a>'.format(obj.ine.url))
        else:
            return '-'
    client_ine.short_description = 'Copia ID vigente'

    def client_bank_account(self, obj):
        if obj.bank_account:
            return mark_safe('<a href="{}" target="_blank">Descargar</a>'.format(obj.bank_account.url))
        else:
            return '-'        

    client_bank_account.short_description = 'Copia o foto de la cuenta de banco'

    def client_proof_address(self, obj):
        if obj.proof_address:
            return mark_safe('<a href="{}" target="_blank">Descargar</a>'.format(obj.proof_address.url))
        else:
            return '-'
        
    client_proof_address.short_description = 'Copia comprobante de domicilio'

    def client_additional_file(self, obj):
        if obj.additional_file:
            return mark_safe('<a href="{}" target="_blank">Descargar</a>'.format(obj.additional_file.url))
        else:
            return '-'
    client_additional_file.short_description = 'Documento Adicional'

    def client_paysheet_one(self, obj):
        if obj.paysheet_one:
            return mark_safe('<a href="{}" target="_blank">Descargar</a>'.format(obj.paysheet_one.url))
        else:
            return '-'
    client_paysheet_one.short_description = 'Último recibo de nómina'

    def client_paysheet_two(self, obj):
        if obj.paysheet_two:
            return mark_safe('<a href="{}" target="_blank">Descargar</a>'.format(obj.paysheet_two.url))
        else:
            return '-'
    client_paysheet_two.short_description = 'Penúltimo recibo de nómina'

    def client_paysheet_three(self, obj):
        if obj.paysheet_three:
            return mark_safe('<a href="{}" target="_blank">Descargar</a>'.format(obj.paysheet_three.url))
        else:
            return '-'
    client_paysheet_three.short_description = 'Antepenúltimo recibo de nómina'

    def client_selfie(self, obj):
        if obj.selfie:
            return mark_safe('<a href="{}" target="_blank">Descargar</a>'.format(obj.selfie.url))
        else:
            return '-'      
    client_selfie.short_description = 'Selfie'

    def client_contract(self, obj):
        if obj.contract:
            return mark_safe('<a href="{}" target="_blank">Descargar</a>'.format(obj.contract.url))
        else:
            return '-'
    client_contract.short_description = 'Contrato'


@admin.register(Requirements)
class RequirementsAdmin(admin.ModelAdmin):
    """ 
    loan_requeriments table Admin. 
    Allows to manipulate records in table

    Added-Dev on: [2020-12-16]
    Added-Prod on: 

    """
    list_max_show_all = sys.maxsize
    list_per_page = 30
    list_display = ('id','name','html_tag','status')
    readonly_fields = []
    exclude = ['created_at', 'modified_at']
    search_fields = ()
    ordering = ('-id',)


@admin.register(RequirementsFamily)
class RequirementsFamilyAdmin(admin.ModelAdmin):
    """ 
    loan_requeriments_family table Admin. 
    Allows to manipulate records in table

    Added-Dev on: [2020-12-16]
    Added-Prod on: 

    """
    list_max_show_all = sys.maxsize
    list_per_page = 30
    list_display = ['id', 'get_family_name', 'get_requirement_name', 'status']
    readonly_fields = []
    exclude = ['created_at', 'modified_at']
    search_fields = ()
    ordering = ('-id',)

    def get_family_name(self, obj):
        url = '/admin/families/family/{}'.format(obj.family.pk)
        return format_html("<a href='{}'>{}</a>", url,obj.family.tradename)
    get_family_name.short_description = 'Familia'
    get_family_name.admin_order_field = 'family__tradename'

    def get_requirement_name(self, obj):
        url = '/admin/loan/requirements/{}'.format(obj.requirement.pk)
        return format_html("<a href='{}'>{}</a>", url, obj.requirement.name)
    get_requirement_name.short_description = 'Requerimiento'
    get_requirement_name.admin_order_field = 'requirement__name'

    


@admin.register(RequirementsClient)
class RequirementsClientAdmin(admin.ModelAdmin):
    """ 
    loan_requeriments_client Admin. 
    Allows to manipulate records in table

    Added-Dev on: [2020-12-16]
    Added-Prod on: 

    """
    list_max_show_all = sys.maxsize
    list_per_page = 30
    list_display = ['id', 'get_client_name', 'get_requirement_name', 'status',]
    readonly_fields = []
    exclude = ['created_at', 'modified_at']
    search_fields = ()
    ordering = ('-id',)

    def get_client_name(self, obj):
        url = '/admin/clients/client/{}'.format(obj.client.pk)
        return format_html("<a href='{}'>{}</a>", url, obj.client.names + ' ' + obj.client.father_last_name + ' ' + obj.client.mother_last_name)
    get_client_name.short_description = 'Cliente'
    get_client_name.admin_order_field = 'client__names'

    def get_requirement_name(self, obj):
        url = '/admin/loan/requirements/{}'.format(obj.requirement.pk)
        return format_html("<a href='{}'>{}</a>", url, obj.requirement.name)
    get_requirement_name.short_description = 'Requerimiento'
    get_requirement_name.admin_order_field = 'requirement__name'