# -*- coding: utf-8 -*-
from django.apps import AppConfig


class LoanConfig(AppConfig):
    name = 'loan'
    verbose_name = 'Solicitudes'

