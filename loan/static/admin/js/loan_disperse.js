$(document).ready(function() {

    /*********************************************
        Get django cookie
  *********************************************/

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    // debugger
    // $("#disperse-model").on('click', function(event) {
    $(".disperse").on('click', function(event) {
        $(".disperse").attr("disabled", true);
        var dataId = $(this).data("id");
        var values = {
            'id': dataId,
        };
        event.preventDefault();
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/loan/change-disperse-status";
        console.log(baseUrl, values);
        ajaxRequest(values, baseUrl);

    });

    function ajaxRequest(data, url) {
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function(data, status) {
                location.reload();
            },
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        }); // end ajax call
    }

});