$(document).ready(function() {
    /*********************************************
        Get django cookie
  *********************************************/

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }


    // $("#payment-model").on('click', function(event) {
    $(".payment").on('click', function(event) {
        var dataId = $(this).data("id");
        var values = {
            'id': dataId,
            'date': null,
            'amount': null,
        };
        event.preventDefault();
        // debugger
        Swal.fire({
            title: 'Ingresa fecha de pago y monto',
            text: "Fecha de pago",
            html: '<input id="datepicker" type="date"><br><input id="amount" type="number" placeholder="Monto a pagar"><br><input id="validate" type="number" placeholder="Validar monto ">',
            // type: "question",
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: "Cancelar",
        }).then(function(result) {
            var date = new Date($('#datepicker').val());
            date = Date.parse(date);

            var amount = $('#amount').val();
            var validate = $('#validate').val();

            if (amount === validate) {
                if (result.value && date && amount) {
                    var getUrl = window.location;
                    values.date = date;
                    values.amount = amount;
                    var baseUrl = getUrl.protocol + "//" + getUrl.host + "/loan/change-payment-status";
                    console.log(baseUrl, values);
                    ajaxRequest(values, baseUrl, date);
                }
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Los montos ingresados no coinciden, favor de verificar',
                    text: "",
                    confirmButtonText: 'Confirmar',
                });
            }

        });

    });

    $(".notifications").on('click', function(event) {
        
        var values = {
            'date': null,
            'amount': null,
        };
        event.preventDefault();
        // debugger
        Swal.fire({
            title: 'Enviar Recordatorios',
            text: "¿Deseas enviar los recordatorios de pago?",
            //html: '<input id="datepicker" type="date"><br><input id="amount" type="number" placeholder="Monto a pagar"><br><input id="validate" type="number" placeholder="Validar monto ">',
            // type: "question",
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: "Cancelar",
        }).then(function(result) {


            if (result.value) {
                var getUrl = window.location;
                var baseUrl = getUrl.protocol + "//" + getUrl.host + "/loan/send-payment-notification";
                console.log(baseUrl, values);
                ajaxRequest(values, baseUrl);
            }
            

        });

    });

    function ajaxRequest(data, url) {
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function(data, status) {
                location.reload();
            },
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            error: function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        }); // end ajax call
    }
});