from rest_framework import serializers
from core.models import CustomUser
from clients.models import Client
from loan.models import Loan
from families.models import Family

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('first_name', 'middle_name', 'father_last_name', 'mother_last_name', 'password', 'role', 'email', 'phone', 'cellphone', 'is_active')


class LoanSerializer(serializers.Serializer):
    request_no = serializers.CharField()
    amount_requested = serializers.IntegerField()
    aperture = serializers.DecimalField(max_digits=7, decimal_places=2)
    amount_total = serializers.DecimalField(max_digits=7, decimal_places=2)
    option = serializers.IntegerField(max_value=2, min_value=1)
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all())
    term = serializers.IntegerField()
    payment_date = serializers.DateField()
    requirements = serializers.JSONField()

    def create(self):
        return Loan(**self.validated_data)


class ClientSerializer(serializers.Serializer):
    """ 
    ClientSerializer Serializer. 
    It is used to serialize and update client data. Called from loan-request-form Form in Angular Front End

    [related_tables]
        clients_client

    Added-Dev on:
    Added-Prod on: 
    """
    names = serializers.CharField()
    father_last_name = serializers.CharField()
    mother_last_name = serializers.CharField()
    birthdate = serializers.DateField()
    actual_position = serializers.CharField()
    cellphone = serializers.CharField()
    phone = serializers.CharField()
    bank_name = serializers.CharField()
    bank_clabe = serializers.CharField()
    address = serializers.CharField()
    salary = serializers.DecimalField(max_digits=8, decimal_places=2)
    #family = serializers.PrimaryKeyRelatedField(queryset=Family.objects.all())
    family_str = serializers.CharField()
    gender = serializers.CharField()
    nationality = serializers.CharField()
    civil_status = serializers.CharField()
    outdoor_no = serializers.CharField()
    interior_no = serializers.CharField(allow_blank=True)
    postal_code = serializers.CharField()
    suburb = serializers.CharField()
    city = serializers.CharField()
    state = serializers.CharField()
    country = serializers.CharField()
    house_type = serializers.CharField()
    years_residence = serializers.DecimalField(max_digits=7, decimal_places=2)
    birth_place = serializers.CharField()
    curp = serializers.CharField()
    study_level = serializers.CharField()
    labor_old = serializers.IntegerField()
    family_income = serializers.DecimalField(max_digits=8, decimal_places=2)
    economic_dependent = serializers.IntegerField()
    credit_bureau = serializers.CharField()
    credit_lines = serializers.CharField()
    tell_us = serializers.CharField()
    family_reference = serializers.CharField()
    family_reference_phone = serializers.CharField()
    reference = serializers.CharField()
    reference_phone = serializers.CharField()
    email = serializers.EmailField()
    labor_old_month = serializers.IntegerField()
    #notes = serializers.CharField(allow_blank=True,allow_null=True)


    def update(self, instance, validated_data):

        instance.names = validated_data.get('names', instance.names)
        instance.father_last_name = validated_data.get('father_last_name', instance.father_last_name)
        instance.mother_last_name = validated_data.get('mother_last_name', instance.mother_last_name)
        instance.birthdate = validated_data.get('birthdate', instance.birthdate)
        instance.actual_position = validated_data.get('actual_position', instance.actual_position)
        instance.cellphone = validated_data.get('cellphone', instance.cellphone)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.bank_name = validated_data.get('bank_name', instance.bank_name)
        instance.bank_clabe = validated_data.get('bank_clabe', instance.bank_clabe)
        instance.address = validated_data.get('address', instance.address)
        instance.salary = validated_data.get('salary', instance.salary)
        #instance.family = validated_data.get('family', instance.family)
        instance.family_str = validated_data.get('family_str', instance.family_str)
        instance.gender = validated_data.get('gender', instance.gender)
        instance.nationality = validated_data.get('nationality', instance.nationality)
        instance.civil_status = validated_data.get('civil_status', instance.civil_status)
        instance.outdoor_no = validated_data.get('outdoor_no', instance.outdoor_no)
        instance.interior_no = validated_data.get('interior_no', instance.interior_no)
        instance.postal_code = validated_data.get('postal_code', instance.postal_code)
        instance.suburb = validated_data.get('suburb', instance.suburb)
        instance.city = validated_data.get('city', instance.city)
        instance.state = validated_data.get('state', instance.state)
        instance.country = validated_data.get('country', instance.country)
        instance.house_type = validated_data.get('house_type', instance.house_type)
        instance.years_residence = validated_data.get('years_residence', instance.years_residence)
        instance.birth_place = validated_data.get('birth_place', instance.birth_place)
        instance.curp = validated_data.get('curp', instance.curp)
        instance.study_level = validated_data.get('study_level', instance.study_level)
        instance.labor_old = validated_data.get('labor_old', instance.labor_old)
        instance.family_income = validated_data.get('family_income', instance.family_income)
        instance.economic_dependent = validated_data.get('economic_dependent', instance.economic_dependent)
        instance.credit_bureau = validated_data.get('credit_bureau', instance.credit_bureau)
        instance.credit_lines = validated_data.get('credit_lines', instance.credit_lines)
        instance.tell_us = validated_data.get('tell_us', instance.tell_us)
        instance.family_reference = validated_data.get('family_reference', instance.family_reference)
        instance.family_reference_phone = validated_data.get('family_reference_phone', instance.family_reference_phone)
        instance.reference = validated_data.get('reference', instance.reference)
        instance.reference_phone = validated_data.get('reference_phone', instance.reference_phone)
        instance.email = validated_data.get('email', instance.email)
        instance.labor_old_month = validated_data.get('labor_old_month', instance.labor_old_month)
        #instance.notes = validated_data.get('notes', instance.notes)

        return instance

class MyInfoSerializer(serializers.Serializer):
    """ 
    MyInfoSerializer Serializer. 
    Copy of ClientSerializer. It is used to serialize and update data insisde section My Info in Angular Front End
    As My Info section doesnt requires all data to be required, the purpose of this section is only validate fields if they have value present

    [related_tables]
        clients_client

    Added-Dev on: [2021-01-09]
    Added-Prod on: 
    """

    ## All required fields
    names = serializers.CharField(required=True)
    father_last_name = serializers.CharField(required=True)
    mother_last_name = serializers.CharField(required=True)
    gender = serializers.CharField(required=True)
    birthdate = serializers.DateField(required=True)
    country = serializers.CharField(allow_blank=True, default="México")
    cellphone = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    labor_old = serializers.IntegerField(required=True)
    labor_old_month = serializers.IntegerField(required=True)


    ## All NON required fields
    

    # Person fields
    birth_place = serializers.CharField(allow_null=True)
    curp = serializers.CharField(allow_null=True)
    civil_status = serializers.CharField(allow_null=True)
    phone = serializers.CharField(allow_null=True)
    study_level = serializers.CharField(allow_null=True)

    # Family fields
    family_income = serializers.DecimalField(max_digits=8, decimal_places=2, default=0.00)
    economic_dependent = serializers.IntegerField(allow_null=True, default=0)
    family_reference = serializers.CharField(allow_null=True)
    family_reference_phone = serializers.CharField(allow_null=True)
    reference = serializers.CharField(allow_null=True)
    reference_phone = serializers.CharField(allow_null=True)

    # Adress fields
    address = serializers.CharField()
    outdoor_no = serializers.CharField(allow_null=True)
    interior_no = serializers.CharField(allow_null=True, allow_blank=True)
    postal_code = serializers.CharField(allow_null=True)
    suburb = serializers.CharField(allow_null=True)
    city = serializers.CharField(allow_null=True)
    state = serializers.CharField(allow_null=True)
    nationality = serializers.CharField(allow_null=True)
    house_type = serializers.CharField(allow_null=True)
    years_residence = serializers.DecimalField(allow_null=True, max_digits=7, decimal_places=2, default=0.00)
    
    # Job fields
    actual_position = serializers.CharField()
    salary = serializers.DecimalField(allow_null=True, max_digits=8, decimal_places=2, default=0.00)

    #Financial fields
    bank_name = serializers.CharField()
    bank_clabe = serializers.CharField()
    credit_bureau = serializers.CharField(allow_null=True)
    credit_lines = serializers.CharField(allow_null=True)
    tell_us = serializers.CharField(allow_blank=True, allow_null=True)

    def update(self, instance, validated_data):
        """ 
        MyInfoSerializer Serializer update function. 
        Validates each field that was created inside the Class. It is called by is_valid serializer method

        [related_tables]
            clients_client

        Added-Dev on: [2021-01-09]
        Added-Prod on: 
        """

        instance.names = validated_data.get('names', instance.names)
        instance.father_last_name = validated_data.get('father_last_name', instance.father_last_name)
        instance.mother_last_name = validated_data.get('mother_last_name', instance.mother_last_name)
        instance.birthdate = validated_data.get('birthdate', instance.birthdate)
        instance.actual_position = validated_data.get('actual_position', instance.actual_position)
        instance.cellphone = validated_data.get('cellphone', instance.cellphone)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.bank_name = validated_data.get('bank_name', instance.bank_name)
        instance.bank_clabe = validated_data.get('bank_clabe', instance.bank_clabe)
        instance.address = validated_data.get('address', instance.address)
        instance.salary = validated_data.get('salary', instance.salary)
        instance.family_str = validated_data.get('family_str', instance.family_str)
        instance.gender = validated_data.get('gender', instance.gender)
        instance.nationality = validated_data.get('nationality', instance.nationality)
        instance.civil_status = validated_data.get('civil_status', instance.civil_status)
        instance.outdoor_no = validated_data.get('outdoor_no', instance.outdoor_no)
        instance.interior_no = validated_data.get('interior_no', instance.interior_no)
        instance.postal_code = validated_data.get('postal_code', instance.postal_code)
        instance.suburb = validated_data.get('suburb', instance.suburb)
        instance.city = validated_data.get('city', instance.city)
        instance.state = validated_data.get('state', instance.state)
        instance.country = validated_data.get('country', instance.country)
        instance.house_type = validated_data.get('house_type', instance.house_type)
        instance.years_residence = validated_data.get('years_residence', instance.years_residence)
        instance.birth_place = validated_data.get('birth_place', instance.birth_place)
        instance.curp = validated_data.get('curp', instance.curp)
        instance.study_level = validated_data.get('study_level', instance.study_level)
        instance.labor_old = validated_data.get('labor_old', instance.labor_old)
        instance.family_income = validated_data.get('family_income', instance.family_income)
        instance.economic_dependent = validated_data.get('economic_dependent', instance.economic_dependent)
        instance.credit_bureau = validated_data.get('credit_bureau', instance.credit_bureau)
        instance.credit_lines = validated_data.get('credit_lines', instance.credit_lines)
        instance.tell_us = validated_data.get('tell_us', instance.tell_us)
        instance.family_reference = validated_data.get('family_reference', instance.family_reference)
        instance.family_reference_phone = validated_data.get('family_reference_phone', instance.family_reference_phone)
        instance.reference = validated_data.get('reference', instance.reference)
        instance.reference_phone = validated_data.get('reference_phone', instance.reference_phone)
        instance.email = validated_data.get('email', instance.email)
        instance.labor_old_month = validated_data.get('labor_old_month', instance.labor_old_month)

        return instance



class ClientLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ("rfc","names","email","cellphone","cellphone_aux","full_name_aux","salary_aux","email_aux")
