from django.shortcuts import render
from django.core.exceptions import MultipleObjectsReturned
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import views
from rest_framework.authtoken.models import Token

from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from django.core.mail import EmailMessage
from decouple import config

import base64
from uuid import uuid4
# import locale
# locale.setlocale(locale.LC_ALL,'es_ES.UTF-8')

import datetime, pytz
from dateutil.relativedelta import *
from random import randrange
from django.contrib.auth.hashers import make_password

# Model
from clients.models import Client, ClientToken, ClientDocuments, ClientAdjustment
#from clients.models import ClientToken
from loan.models import Loan, PendingPayment, Seeds, PartialPayment, ForDispersed, DispersedState, DispersedState, Requirements, RequirementsFamily, RequirementsClient
from core.models import CustomUser,Role
from families.models import Family, Period, Holidays, Paydays, CustomMessage, PromotionCode
from products.models import Product


from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import authentication_classes
from rest_framework.authentication import TokenAuthentication
from rest_framework import status
from rest_framework.parsers import FileUploadParser, JSONParser, MultiPartParser

from django.forms.models import model_to_dict
import json

from django.core import serializers
from django.http import HttpResponse

from .serializers import LoanSerializer, ClientSerializer, MyInfoSerializer
from .helpers import numero_to_letras

import numpy as np

# import pdfkit

from django.template.loader import render_to_string
from weasyprint import HTML
import tempfile

from io import BytesIO
from django.core.files import File

from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from decimal import Decimal

#Validaciones y Excepciones
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import validate_email
#Email
from django.core.mail import send_mail
#Clases
from rest_framework import mixins, permissions, status, viewsets
from rest_framework.decorators import action
#Serialeizadores
from .serializers import  ClientLogSerializer
#SMS
import nexmo
import phonenumbers

from django.conf import settings
from datetime import timedelta
from django.utils.timezone import make_aware



class ClientViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):

    parser_classes = (JSONParser, MultiPartParser)
    serializer_class = ClientLogSerializer
    permission_classes = ()
    queryset = ''
    
    @action(methods=["post"], detail=False)
    def update_info(self,request):

        try:
            client = Client.objects.get(user=request.user)
        except Client.DoesNotExist:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        # Client update Request
        client_info = request.data

        try:
            #Validar que tenga la variable de tell_us_notes para concatenar la informacion
            if client_info['tell_us_note']:
                client_info['tell_us'] = client_info['tell_us'] + ' ' + client_info['tell_us_note']
        except:
            pass                


        #try:
            # client_info['birthdate']
        #    born = datetime.datetime.strptime(client_info['birthdate'], "%Y-%m-%d").date()
        #    today = datetime.date.today()
        #    years_old = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
            
        #    if years_old < 18:
        #        return Response({'error': 'Error en la fecha de nacimiento, debe tener mas de 18 años de edad'},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        
        #except:
        #    return Response({'error': 'Error en la fecha de nacimiento'},status=status.HTTP_422_UNPROCESSABLE_ENTITY)


        if client_info['family_income'] == "":
            client_info['family_income'] = 0

        if client_info['interior_no'] is None:
            client_info['interior_no'] = ""   


        if 'control_myinfo' in client_info and client_info['control_myinfo']:
            ## control_myinfo is a flag from My Info Section in Angular Front end. Section allows many fields not required

            if client_info['actual_position'] is None or client_info['actual_position'] == "":
                client_info['actual_position'] = "Sin Puesto"

            if client_info['phone'] is None or client_info['phone'] == "":
                client_info['phone'] = "0000000000"

            if client_info['bank_name'] is None or client_info['bank_name'] == "":
                client_info['bank_name'] = "Sin Banco"
                
            if client_info['bank_clabe'] is None or client_info['bank_clabe'] == "":
                client_info['bank_clabe'] = "000000000000000000"
                
            if client_info['address'] is None or client_info['address'] == "":
                client_info['address'] = "Sin Direccion"


        if 'control_myinfo' in client_info and client_info['control_myinfo'] :
            # my-info form / Allows Null Values
            client_serializer = MyInfoSerializer(data=client_info)
        else :
            # loan-request form / Requires All Values
            client_serializer = ClientSerializer(data=client_info)

            
        if not client_serializer.is_valid():
            #print(client_serializer.errors)
            return Response(
                {'error': 'Error,los datos ingresados no son validos'},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )


        ## Update and Save Client
        client_updated = client_serializer.update(client, client_info)
        client_updated.save()

        return Response({"success":True, "message":"Hemos modificado tus datos"})

    #Funcion Para generar Pin de cliente
    def pin_generator(self):
        try:
            while True:
                pin = randrange(1000000)
                pin = format(pin, '06d')
                pin_clients = Client.objects.filter(pin=str(pin))
                if len(pin_clients) == 0:
                    break
        except:
            return False
        return pin
    
    #Verifica que el rfc corresponda a un cliente
    def rfc_exist(self,rfc):
        try:
            client = Client.objects.get(rfc=rfc)
        except:
            return False
        return client

    #Valida el rfc
    @action(methods=["post"], detail=False)
    def rfc_validation(self,request):
        rfc = request.data.get('rfc', None)
        client = self.rfc_exist(rfc)
        
        if client is False:
            activate = self.activate_client_account_step1(rfc)
            if activate is False:
                return Response({"success": False,"error": "Error al registrar el usuario"},status=status.HTTP_400_BAD_REQUEST)
            return Response({"success": False,"error": "Validando RFC."})

        if client.invited == False:
            return Response({"success": False,"error": "No te hemos enviado la invitación, contáctanos por Whatsapp para que puedas activar tu cuenta."},status=status.HTTP_400_BAD_REQUEST)
        
        if client.user_id:
            return Response({"success": False,"error": "Este RFC ya está activado, reestablece tu contraseña."},status=status.HTTP_400_BAD_REQUEST)

        #Valiacion para saber si el cliente no ha terminado de crear su cuenta
        if client.invited == True and client.created_on_website == True:
            if client.status == Client.CAPTURED_RFC:
                return Response({"success": False,"error": "CAPTURED_RFC"})
            elif client.status == Client.CAPTURED_INFORMATION:
                return Response({"success": False,"error": "CAPTURED_INFORMATION"})
            elif client.status == Client.CAPTURED_DOCUMENTS:
                return Response({"success": False,"error": "CAPTURED_INFORMATION"})
            elif client.status == Client.PIN_SENT:
                return Response({"success": False,"error": "PIN_SENT"})
            elif client.status == Client.USER_ACCOUNT:
                return Response({"success":True})


        return Response({"success":True})
    
    #Funcion para guardar datos auxiliares
    @action(methods=["post"], detail=False)
    def activate_account(self,request):
        rfc = request.data.get('rfc', None)
        receptor = request.data.get('receptor', None)
        salary = request.data.get('salary', None)
        name = request.data.get('name', None)
        receptor_type = request.data.get('receptor_type', None) # 0 = Email / 1 = cellphone number
        
        if rfc is None:
            return Response({"error": "Se requiere un RFC"},status=status.HTTP_400_BAD_REQUEST)

        client = self.rfc_exist(rfc)
        
        if client is False:
            return Response({"error": "Este RFC no corresponde a un cliente"},status=status.HTTP_400_BAD_REQUEST)     
        if client.user_id is not None:
            return Response({"error": "No cliente ya cuenta con un usuario"},status=status.HTTP_400_BAD_REQUEST)
        if receptor is None:
            return Response({"error": "Debe ingresar un email o un celular"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        if receptor_type is None:
            return Response({"error": "Este valor no es válido"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)  
        
        #SI EL RECEPTOR ES UN NUMERO DE CELULAR
        if int(receptor_type) == 1:
            try:
                cell_number = phonenumbers.parse(str(receptor), "MX")
            except: 
                return Response({"error": "El número telefónico ingresado no es válido"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            
            cell_number_validation = phonenumbers.is_possible_number(cell_number)
            if cell_number_validation == False:
                return Response({"error": "El número telefónico ingresado no es válido"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
    
            pin = self.pin_generator()
            if pin is False:
                return Response({"success": False,"error": "El pin no se ha podido generar. Intenta nuevamente"},status=status.HTTP_400_BAD_REQUEST)
            
            client.cellphone_aux = receptor
            client.full_name_aux =  name
            client.salary_aux =  salary
            client.is_active = True
            client.pin = str(pin) 
            client.save()

            send_pin = self.send_pin_cellphone(client)
            if send_pin is True:
                return Response({"success": True,"cellphone": client.cellphone_aux})
            else:
                print("error")
        
        elif int(receptor_type) == 0:
            try:
                validate_email(receptor)
            except:
                return Response({"error": "Email no válido"},status=status.HTTP_400_BAD_REQUEST)

            client.email_aux = receptor
            
            pin = self.pin_generator()
            if pin is False:
                return Response({"success": False,"error": "El pin no se ha podido generar. Intenta nuevamente"},status=status.HTTP_400_BAD_REQUEST)

            client.email_aux = receptor
            client.full_name_aux =  name
            client.salary_aux =  salary
            client.is_active = True
            client.pin = str(pin) 
            client.save()

            pin_64 = base64.b64encode(str(client.pin).encode())
            new_censored_email_64 = base64.b64encode(client.censored_email.encode())
            send_pin = self.send_pin_email(client,pin_64,new_censored_email_64)


            if send_pin is False:
                return Response({"success": False,"error": "El pin no se ha podido enviar"},status=status.HTTP_400_BAD_REQUEST)
            
            return Response({"success": True,"Email": client.email_aux})

        return Response({"error": "Intenta nuevamente"})
    
    #enviar el pin por correo     
    def send_pin_email(self,client,pin_64, censored_email_64):
        subject = 'PIN DE CONFIRMACIÓN'
        html_message = render_to_string(
            'base.html',
            {
                'title': 'PIN DE CONFIRMACIÓN',
                'content_template': 'content_pin.html',
                'contact_template': 'contact.html',
                'names': client.names,
                'father_last_name': client.father_last_name,
                'mother_last_name': client.mother_last_name,
                'pin': client.pin,
                'url': config('FRONT_URL') + "pin/" + pin_64.decode('utf-8').replace('=', '') + '/' + censored_email_64.decode('utf-8').replace('=', ''),
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = client.email_aux

        try:
            mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
            return True
        except:
            return False
    
    #enviar el pin por SMS
    def send_pin_cellphone(self,client):
        
        sms = nexmo.Client(key=settings.NEXMO_KEY, secret=settings.NEXMO_SECRET)
        try:
            sms.send_message({
            'from': 'Ixhua',
            'to': '52'+ client.cellphone_aux,
            'text': 'Hola '+ str(client.full_name_aux) + ' tu PIN de confirmacion es: '+ client.pin ,
            })
            return True
        except:
            return False
        
        return True  
    
    #validacion del pin
    @action(methods=["post"], detail=False)
    def pin_validation(self,request):
        pin = request.data.get('pin', None)
        try:
            client = Client.objects.get(pin=str(pin))
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este Pin"},status=status.HTTP_400_BAD_REQUEST)
        
        return Response({"success":True})
    
    def send_confirmation(self, client_email):
        subject = 'Tu cuenta ha sido creada'
        html_message = render_to_string(
            'base.html',
            {
                'title': 'Tu cuenta ha sido creada',
                'content_template': 'content_confirmation.html',
                'contact_template': 'contact.html',
                'url': config('FRONT_URL') + "auth/login/",
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = client_email

        try:
            mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
        except:
            return Response( {"error": "No se pudo enviar el correo"},status=status.HTTP_404_NOT_FOUND)

    #enviar email a ixhua con la infromacion del cliente cuando crea su contraseña
    def send_email_create_client_info(self,client):
        subject = 'Nuevo registro en ixhua.mx'
        code=" - "
        if client.promotion_code:
            code=client.promotion_code

        html_message = render_to_string(
            'create_client_info.html',
            {
                'names':client.names,
                'father_last_name':client.father_last_name,
                'mother_last_name':client.mother_last_name,
                'family':client.family.tradename,
                'product':client.product.name,
                'rfc':client.rfc,
                'family_str':client.family_str,
                'promotion_code':code,
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = config('CONTACT_EMAIL')

        try:
            mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
            return Response({"success": True})
        except:
            return Response({"error":"El email no pudo ser enviado"})

    #crear contraseña de usuario
    @action(methods=["post"], detail=False)
    def create_password(self,request):
        pin = request.data.get('pin', None)
        password = request.data.get('password', None)
        passwordConfirm = request.data.get('passwordConfirm', None)

        try:
            client = Client.objects.get(pin=str(pin))
        except Client.DoesNotExist:
            return Response( {"error": "Pin invalido"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        
        if passwordConfirm != password:
            return Response({"error": "Las Contraseñas no coinciden"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        
        if client.user == None:
            try:
                users = CustomUser.objects.get(email=client.email)
                return Response({"Error": "El correo que intenta registrar ya existe"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            except CustomUser.DoesNotExist:
                users = None

            try:
                user, created = CustomUser.objects.get_or_create(
                    email=client.email,
                    first_name=client.names,
                    father_last_name=client.father_last_name,
                    mother_last_name=client.mother_last_name,
                    role=Role.cliente.id,
                    phone=client.phone,
                    cellphone=client.cellphone
                )
                user.password = make_password(password)
                user.save()
                
                client.user = user
                client.save()
                
                token, created = Token.objects.get_or_create(user=client.user)
                client.pin = ""

                #if client.status == Client.PIN_SENT and client.created_on_website==True:
                try:
                    client.status = Client.USER_ACCOUNT
                except:
                    pass
                
                client.save()

                #envia el email a ixhua con los datos del cliente
                try:
                    self.send_email_create_client_info(client) 
                except:
                    pass

                if client.email_aux:
                    try:
                        self.send_confirmation(client.email_aux) 
                    except:
                        pass

                    try:
                        if client.email_aux is not None:
                            if client.email_aux != client.email:
                                self.send_email_validation(client)
                    except:
                        pass
                
                return Response({"success": True,"token": token.key})
            except:
                return Response({"error": "Esta cuenta no se puede activar"},status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"success": True,})

       
        return Response({"success": True})

    #enviar email cuando el cliente tenga dos correos distintos
    def send_email_validation(self,client):
        send_mail(
        'USUARIO CAMBIÓ DE CORREO',
        'EL USUARIO ' + client.rfc + ' ACTIVÓ SU CUENTA CON UN CORREO DIFERENTE AL QUE TENÍAMOS EN PLATAFORMA. ENTRA A SU PERFIL PARA VALIDAR.',
        config('EMAIL_HOST_USER'),
        [config('CONTACT_EMAIL')],
        )

    #enviar email cuando los clientes soliviten validacion de informacion
    @action(methods=["post"], detail=False)
    def send_email_info_validation(self,request):
        name = request.data.get('name', None)
        cell = request.data.get('cell', None)
        company = request.data.get('company', None)
        rfc = request.data.get('rfc', None)

        subject = 'VALIDACIÓN DE DATOS'
        html_message = render_to_string(
            'client_info_validation.html',
            {
                'name':name,
                'cell':cell,
                'company':company,
                'rfc':rfc
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = config('CONTACT_EMAIL')

        try:
            mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
            return Response({"success": True})
        except:
            return Response({"error":"El email no pudo ser enviado"})

    @action(methods=["post"], detail=False)
    def resend_pin(self,request):
        rfc = request.data.get('rfc', None)
        receptor = request.data.get('receptor', None)
        
        try:
            client = Client.objects.get(rfc=str(rfc))
        except ObjectDoesNotExist:
            return Response({"error": "El pin no fue enviado. No se pudo encontrar al cliente"},status=status.HTTP_400_BAD_REQUEST)

        if client.cellphone_aux == receptor:
            send_pin = self.send_pin_cellphone(client)
            if send_pin is True:
                return Response({"success": True,"cellphone": client.cellphone_aux})
            else:
                return Response({"error": "Error. El pin no pudo ser reenviado"},status=status.HTTP_400_BAD_REQUEST)

        elif client.email_aux == receptor:
            pin_64 = base64.b64encode(str(client.pin).encode())
            new_censored_email_64 = base64.b64encode(client.censored_email.encode())
            send_pin = self.send_pin_email(client,pin_64,new_censored_email_64)

            if send_pin is True:
                return Response({"success": True,"Email": client.email_aux})
            else:
                return Response({"error": "Error. El pin no pudo ser reenviado"},status=status.HTTP_400_BAD_REQUEST)
        
        else:
            return Response({"error": "Error. El pin no pudo ser reenviado"},status=status.HTTP_400_BAD_REQUEST)

        return Response({"success": True})

    @action(methods=["post"], detail=False)
    def close_session_expire(self,request):
        rfc = request.data.get('rfc', None)

        try:
            client = Client.objects.get(rfc=str(rfc))
        except ObjectDoesNotExist:
            return Response({"error": "No se pudo encontrar al cliente"},status=status.HTTP_400_BAD_REQUEST)

        try:
            send_mail(
            'SESION EXPIRADA ' + client.rfc,
            'LA SESION DEL USUARIO ' + client.rfc + ' HA EXPIRADO.',
            config('EMAIL_HOST_USER'),
            [config('CONTACT_EMAIL')],
            )
        except:
            pass

        return Response({"success": True})

    #validar email unico
    @action(methods=["post"], detail=False)
    def validate_email_onchange(self,request):
        email = request.data.get('email', None)
        other_rfc = request.data.get('rfc', None)

        try:
            client = Client.objects.get(email=email)
        except Client.DoesNotExist:
            return Response( {"success": True})

        if client.rfc != other_rfc:
            return Response( {"error": "El correo electronico ingresado ya fue registrado"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        
        return Response( {"success": True})

    #Cambiar contraseña del cliente
    @action(methods=["post"], detail=False)
    def change_password(self,request):
        rfc = request.data.get('rfc', None)
        oldPassword = request.data.get('oldPassword', None)
        newPassword = request.data.get('newPassword', None)
        confirmPassword = request.data.get('confirmPassword', None)

        if rfc is None:
            return Response( {"error": "Se requiere un RFC"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        if oldPassword is None:
            return Response( {"error": "Se requiere ingresar su contraseña"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        if newPassword is None:
            return Response( {"error": "Se requiere ingresar su nueva contraseña"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        if confirmPassword is None:
            return Response( {"error": "Se requiere confirmar su nueva contraseña"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        if newPassword != confirmPassword:
            return Response( {"error": "Las contraseñas no coinciden"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        try:
            client = Client.objects.get(rfc=str(rfc))
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)

        if client.user:

            if client.user.check_password(oldPassword):
                try:
                    client.user.password = make_password(newPassword)
                    client.user.save()
                except:
                    return Response( {"error": "Error. No fue posible cambiar la contraseña"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            else:
                return Response({"error": "Tu contraseña es incorrecta"},status=status.HTTP_404_NOT_FOUND)
        else:
            return Response({"error": "Este cliente no cuenta con un usuario"},status=status.HTTP_404_NOT_FOUND)

        return Response( {"success": True})

    #Mostrar mensaje personalizado para el cliente
    @action(methods=["post"], detail=False)
    def show_custom_message(self,request):
        rfc = request.data.get('rfc', None)
        message = None
        url = None
        try:
            client = Client.objects.get(rfc=str(rfc))
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)

        #if client.created_on_website is True:
        try:
            customMessage = CustomMessage.objects.get(family=client.family)
            message = customMessage.message
            url = customMessage.website
        except ObjectDoesNotExist:
            pass
        
        return Response({"success":True,"message":message,"website":url})

    @action(methods=["post"], detail=False)
    def cancel_account(self,request):
        rfc = request.data.get('rfc', None)

        #validar que el cliente exista
        try:
            client = Client.objects.get(rfc=rfc)
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)

        if client.user == None:
            return Response({"error": "No existe un usuario con este RFC"},status=status.HTTP_404_NOT_FOUND)
        
        try:
            loans = Loan.objects.filter(client=client).exclude(state=Loan.REJECTED)
            
            for loan in loans: 
                #validacion de solicitud con estatus pendiente     
                if loan.state == Loan.PENDING:
                    return Response({"error": "La cuenta no puede ser cancelada, existe una solicitud pendiente de revisión"},status=status.HTTP_400_BAD_REQUEST) 
                
                #validacion de solicitudes aprovadas
                elif loan.state == Loan.APPROVED:
                    try:
                        for_dispersed = ForDispersed.objects.get(loan=loan)    
                        #la cuenta no se puede canelar con una solicitud pendiente por dispersar
                        if for_dispersed.state == DispersedState.not_dispersed.id:
                            return Response({"error": "La cuenta no puede ser cancelada, existe una solicitud pendiente por dispersar"},status=status.HTTP_400_BAD_REQUEST) 
                    except ObjectDoesNotExist:
                        pass

                    try:
                        pending_payment = PendingPayment.objects.get(loan=loan)
                        #la cuenta no se puede cancelar sobre
                        if pending_payment.paid_out == False:
                            return Response({"error": "La cuenta no puede ser cancelada, existe una solicitud pendiente por liquidar"},status=status.HTTP_400_BAD_REQUEST) 
                    except ObjectDoesNotExist:
                        pass
        except ObjectDoesNotExist:
            pass

        #Cambia el estatus del usuario
        try:
            client.user.is_active = False
            client.status = Client.CANCEL_ACCOUNT
            client.is_active = False
            client.save()
            client.user.save()
            try:
                self.cancel_account_email(client)
            except:
                pass
        except:
            return Response({"error": "La cuenta no puede ser cancelada"},status=status.HTTP_400_BAD_REQUEST) 


        return Response( {"success": True})

    #enviar email cuando el cliente tenga dos correos distintos
    def cancel_account_email(self,client):
        subject = 'Un usuario ha cancelado su cuenta -  ' + str(client.rfc)
        html_message = render_to_string(
            'cancel_account_admin_notification.html',
            {
                'names': str(client.names),
                'phone': str(client.phone),
                'cellphone': str(client.cellphone),
                'email': str(client.email),
                
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = config('CONTACT_EMAIL')

        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)


    @action(methods=["post"], detail=False)
    def client_loan_requirements(self,request):
        """ 
        client_loan_requirements method. 
        Returns loan requirements for a specific user
        Requirements could be by:
        - client
        - family
        - general

        Added-Dev on: [2020-12-26]
        Added-Prod on: 

        """
        rfc = request.data.get('rfc', None)
        data = { "success": True, }

        try:
            client = Client.objects.get(rfc=rfc)
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)

        ## Retrieve Requirements
        try:
            reqs_client = RequirementsClient.objects.filter(client = client).values_list('requirement_id').exclude(status = 'IN')
            reqs_family = RequirementsFamily.objects.filter(family_id = client.family_id).values_list('requirement_id').exclude(status = 'IN')
            if reqs_client:
                ## 1 By Client Requirements
                reqs = Requirements.objects.filter(id__in = reqs_client).exclude(status = 'IN').order_by('html_tag')
            elif reqs_family:
                ## 2 By Family Requirements
                reqs = Requirements.objects.filter(id__in = reqs_family).exclude(status = 'IN').order_by('html_tag')
            else:
                ## 3 General Requirements
                reqs = Requirements.objects.exclude(status = 'IN').order_by('html_tag')

            for req in reqs:
                this_requirement = req.html_tag
                data.update({ this_requirement: True })
        
            if len(reqs) < 2:
                return Response({ "error": "No encontre requerimentos" },status=status.HTTP_404_NOT_FOUND)
            else:
                return Response(data)

        except ObjectDoesNotExist:
            pass

        return Response({ "error": "No encontre requerimentos" },status=status.HTTP_404_NOT_FOUND)


    @action(methods=["post"], detail=False)
    def client_document(self,request):
        rfc = request.data.get('rfc', None)
        #Files aux
        ine = None
        bank_account = None
        proof_address = None
        additional_file = None
        paysheet_one = None
        paysheet_two = None
        paysheet_three = None
        selfie = None
        client_type = None


        ine_date = True
        bank_account_date = True
        proof_address_date = True
        additional_file_date = True
        paysheet_one_date = True
        paysheet_two_date = True
        paysheet_three_date = True
        selfie_date = True

        try:
            client = Client.objects.get(rfc=rfc)
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)

        client_type = client.created_on_website

        try:
            documents = ClientDocuments.objects.get(client=client)
            
            if documents.ine:
                ine = {"url":documents.ine.url}
                if documents.ine_effective_date:
                    if datetime.date.today() > documents.ine_effective_date:
                        ine_date = False
            
            if documents.bank_account:
                bank_account = {"url":documents.bank_account.url}
                if documents.bank_account_effective_date:
                    if datetime.date.today() > documents.bank_account_effective_date:
                        bank_account_date = False
            
            if documents.proof_address:
                proof_address= {"url":documents.proof_address.url} 
                if documents.proof_address_effective_date:
                    if datetime.date.today() > documents.proof_address_effective_date:
                        proof_address_date = False
            
            if documents.additional_file:
                additional_file= {"url":documents.additional_file.url}
                if documents.additional_file_effective_date:
                    if datetime.date.today() > documents.additional_file_effective_date:
                        additional_file_date = False
            
            if documents.paysheet_one:
                paysheet_one= {"url":documents.paysheet_one.url}
                if documents.paysheet_one_effective_date:
                    if datetime.date.today() > documents.paysheet_one_effective_date:
                        paysheet_one_date = False
            
            if documents.paysheet_two:
                paysheet_two= {"url":documents.paysheet_two.url}
                if documents.paysheet_two_effective_date:
                    if datetime.date.today() > documents.paysheet_two_effective_date:
                        paysheet_two_date = False
            
            if documents.paysheet_three:
                paysheet_three= {"url":documents.paysheet_three.url} 
                if documents.paysheet_three_effective_date:
                    if datetime.date.today() > documents.paysheet_three_effective_date:
                        paysheet_three_date = False
            
            if documents.selfie:
                selfie= {"url":documents.selfie.url}
                if documents.selfie_effective_date:
                    if datetime.date.today() > documents.selfie_effective_date:
                        selfie_date = False            

        except ObjectDoesNotExist:
            pass

        return Response({
            "success": True,
            'ine':ine,
            'bank_account':bank_account,
            'proof_address':proof_address,
            'additional_file':additional_file,
            'paysheet_one':paysheet_one,
            'paysheet_two': paysheet_two,
            'paysheet_three': paysheet_three,
            'selfie': selfie,
            'client_type': client_type,
            'ine_date':ine_date,
            'bank_account_date':bank_account_date,
            'proof_address_date':proof_address_date,
            'additional_file_date':additional_file_date,
            'paysheet_one_date':paysheet_one_date,
            'paysheet_two_date':paysheet_two_date,
            'paysheet_three_date':paysheet_three_date,
            'selfie_date':selfie_date,
        })

    @action(methods=["post"], detail=False)
    def client_upload_documents(self,request):
        rfc = request.data.get('rfc', None)
        notes = request.data.get('notes', None)
        try:
            client = Client.objects.get(rfc=rfc)
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)

        allowed_files = ['image/jpeg', 'image/png', 'application/pdf']
        max_size = 20*1024*1024
        file_obj = request.FILES

        ine = None
        bank_account = None 
        proof_address = None
        additional_file = None
        paysheet_one = None
        paysheet_two = None
        paysheet_three = None
        selfie = None
        
        try:
            if file_obj['additional_file']:
                additional_file = file_obj['additional_file']
        except:
            pass

        try:
            if file_obj['ine']:
                ine = file_obj['ine']
        except:
            pass

        try:
            if file_obj['bank_account']:
                bank_account = file_obj['bank_account']
        except:
            pass

        try:
            if file_obj['proof_address']:
                proof_address = file_obj['proof_address']
        except:
            pass

        try:
            if file_obj['paysheet_one']:
                paysheet_one = file_obj['paysheet_one']
        except:
            pass

        try:
            if file_obj['paysheet_two']:
                paysheet_two = file_obj['paysheet_two']
        except:
            pass

        try:
            if file_obj['paysheet_three']:
                paysheet_three = file_obj['paysheet_three']
        except:
            pass

        try:
            if file_obj['selfie']:
                selfie = file_obj['selfie']
        except:
            pass


        
        errors = {}

        # Format validation
        if ine is not None:
            if ine.content_type not in allowed_files:
                errors['ine'] = "Formato inválido"
        if bank_account is not None:
            if bank_account.content_type not in allowed_files:
                errors['ibank_accountne'] = "Formato inválido"
        if proof_address is not None:
            if proof_address.content_type not in allowed_files:
                errors['proof_address'] = "Formato inválido"
        if additional_file is not None:
            if additional_file.content_type not in allowed_files:
                errors['additional_file'] = "Formato inválido"
        if paysheet_one is not None:
            if paysheet_one.content_type not in allowed_files:
                errors['paysheet_one'] = "Formato inválido"
        if paysheet_two is not None:
            if paysheet_two.content_type not in allowed_files:
                errors['paysheet_two'] = "Formato inválido"
        if paysheet_three is not None:
            if paysheet_three.content_type not in allowed_files:
                errors['paysheet_three'] = "Formato inválido"
        if selfie is not None:
            if selfie.content_type not in allowed_files:
                errors['selfie'] = "Formato inválido"


        # Size validation
        if ine is not None:
            if ine.size > max_size and 'ine' not in errors:
                errors['ine'] = "Tamaño inválido"

        if bank_account is not None:
            if bank_account.size > max_size and 'bank_account' not in errors:
                errors['bank_account'] = "Tamaño inválido"

        if proof_address is not None:
            if proof_address.size > max_size and 'proof_address' not in errors:
                errors['proof_address'] = "Tamaño inválido"

        if additional_file is not None:    
            if additional_file.size > max_size and 'additional_file' not in errors:
                errors['additional_file'] = "Tamaño inválido"

        if paysheet_one is not None:    
            if paysheet_one.size > max_size and 'paysheet_one' not in errors:
                errors['paysheet_one'] = "Tamaño inválido"
        
        if paysheet_two is not None:    
            if paysheet_two.size > max_size and 'paysheet_two' not in errors:
                errors['paysheet_two'] = "Tamaño inválido"
        
        if paysheet_three is not None:    
            if paysheet_three.size > max_size and 'paysheet_three' not in errors:
                errors['paysheet_three'] = "Tamaño inválido"

        if selfie is not None:    
            if selfie.size > max_size and 'selfie' not in errors:
                errors['selfie'] = "Tamaño inválido"

        documents, created = ClientDocuments.objects.get_or_create(client=client)

        if len(errors) > 0:
            return Response(
                    {
                        "errors": errors
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )
        if ine is not None:
            documents.ine = ine
            documents.ine_effective_date=None
        if bank_account is not None:
            documents.bank_account = bank_account
            documents.bank_account_effective_date=None
        if proof_address is not None:
            documents.proof_address = proof_address
            documents.proof_address_effective_date=None
        if additional_file is not None:
            documents.additional_file = additional_file
            documents.additional_file_effective_date=None
        if paysheet_one is not None:
            documents.paysheet_one = paysheet_one
            documents.paysheet_one_effective_date=None
        if paysheet_two is not None:
            documents.paysheet_two = paysheet_two
            documents.paysheet_two_effective_date=None
        if paysheet_three is not None:
            documents.paysheet_three = paysheet_three
            documents.paysheet_three_effective_date=None
        if selfie is not None:
            documents.selfie = selfie
            documents.selfie_effective_date=None
        
        documents.save()

        try:
            if notes:
                client.notes = notes
            else:
                client.notes = ""
            client.save()
        except:
            pass

        return Response({"success": True})

    @action(methods=["post"], detail=False)
    def delete_document(self,request):
        rfc = request.data.get('rfc', None)
        document_type = request.data.get('document_type', None)
        
        try:
            client = Client.objects.get(rfc=rfc)
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)

        try:
            documents = ClientDocuments.objects.get(client=client)
        except:
            return Response({"error": "No existe un registro de documentos para este cliente"},status=status.HTTP_404_NOT_FOUND)

        if document_type == 'ine':
            documents.ine = None
            documents.ine_effective_date=None
        elif document_type == 'bank_account':
            documents.bank_account = None
            documents.bank_account_effective_date=None
        elif document_type == 'proof_address':
            documents.proof_address = None
            documents.proof_address_effective_date=None
        elif document_type == 'additional_file':
            documents.additional_file = None
            documents.additional_file_effective_date=None
        elif document_type == 'paysheet_one':
            documents.paysheet_one = None
            documents.paysheet_one_effective_date=None
        elif document_type == 'paysheet_two':
            documents.paysheet_two = None
            documents.paysheet_two_effective_date=None
        elif document_type == 'paysheet_three':
            documents.paysheet_three = None
            documents.paysheet_three_effective_date=None
        elif document_type == 'selfie':
            documents.selfie = None
            documents.selfie_effective_date=None
        else:
            return Response({"error": "No se pudo elimiar el documento"},status=status.HTTP_400_BAD_REQUEST)


        documents.save()
        return Response({"success": True})

    #===================================================
    #           CREACION MANUAL DE CLIENTE
    #===================================================
    
    #Funcion para que el cliente pueda crear un registro manual
    def activate_client_account_step1(self,rfc):      
        if rfc is None:
            return False
            #return Response({"error": "Se requiere un RFC"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        try:
            client = Client.objects.get(rfc=str(rfc))
            return False
            #return Response({"error": "Este RFC ya corresponde a un cliente"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except ObjectDoesNotExist:
            client = None

        try:
            family = Family.objects.get(tradename=str(settings.PUBLIC_FAMILY_BIWEEKLY))
        except ObjectDoesNotExist:
            return False
            #return Response({"error": "No existe una empresa para este cliente."},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        try:
            product = Product.objects.get(name=str(settings.PUBLIC_PRODUCT))
        except ObjectDoesNotExist:
            return False
            #return Response({"error": "No existe un producto para este cliente."},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        try:
            new_client = Client.objects.create(
            rfc=rfc,
            names="Sin Nombre",
            father_last_name= "Sin Apellido Paterno",
            mother_last_name="Sin apellido Materno",
            birthdate=  datetime.date(2000,1,1),
            actual_position="Sin Puesto",
            admission_date = datetime.date.today(),
            cellphone="0000000000",
            phone="0000000000",
            email= str(rfc) + "@ixhua.site",
            bank_name="Sin Banco",
            bank_clabe="000000000000000000",
            address="Sin Direccion",
            salary=0,
            payment_frequency="Sin Informacion",
            credit_amount=1000.00,
            family=family,
            subfamily= str(settings.PUBLIC_PRODUCT),
            product= product,
            status=Client.CAPTURED_RFC,
            created_on_website= True,
            country='México'
            )
        except:
            return False
            #return Response({"error": "No fue posible hacer el registro"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

    #Funcion para actualizar la informacion ingresada en el segundo formulario
    @action(methods=["post"], detail=False)
    def activate_client_account_step2(self,request):
        names = request.data.get('name',None)
        firstLastName = request.data.get('firstLastName', None)
        secondLastName = request.data.get('secondLastName', None)
        cell = request.data.get('cell', None)
        email = request.data.get('email', None)
        company = request.data.get('company', None) 
        years = request.data.get('years', None)
        months = request.data.get('months', None)
        #salary = request.data.get('salary', None)
        payment_frequency = request.data.get('payment_frequency', None)
        rfc = request.data.get('rfc', None)
        code = request.data.get('code', None)

        #validar que el cliente exista
        try:
            client = Client.objects.get(rfc=rfc)
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)
        
        #validar que el email sea valido
        try:
            validate_email(email)
        except:
            return Response({"error": "Email no válido"},status=status.HTTP_400_BAD_REQUEST)

        #validar que el email ingresado no lo use otro cliente
        try:
            user_email_validate = Client.objects.get(email=email)
            if user_email_validate.rfc != rfc:
                return Response({"error": "El correo electrónico ingresado ya fue registrado"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except Client.DoesNotExist:
            pass
        
        #validar que cumpla con las reglas, que fue creado en el fontend y cumpla con el estatus
        if client.created_on_website is False or client.status is not Client.CAPTURED_RFC: 
            if client.status is not Client.CAPTURED_INFORMATION:
                return Response({"error": "El cliente no cumple con los requisitos sobre su estatus"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        #validar que el numero de celular sea un numero valido
        try:
            cell_number = phonenumbers.parse(str(cell), "MX")
        except: 
            return Response({"error": "El número telefónico ingresado no es válido"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        
        cell_number_validation = phonenumbers.is_possible_number(cell_number)
        if cell_number_validation == False:
            return Response({"error": "El número telefónico ingresado no es válido"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        #asignar familia dependiendo del tipo de pago
        if payment_frequency == 'Semanal':
            try:
                family = Family.objects.get(tradename=str(settings.PUBLIC_FAMILY_WEEKLY))
            except ObjectDoesNotExist:
                return Response({"error": "No existe una empresa para este cliente."},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        elif payment_frequency == 'Quincenal':
            try:
                family = Family.objects.get(tradename=str(settings.PUBLIC_FAMILY_BIWEEKLY))
            except ObjectDoesNotExist:
                return Response({"error": "No existe una empresa para este cliente."},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        elif payment_frequency == 'Mensual':
            try:
                family = Family.objects.get(tradename=str(settings.PUBLIC_FAMILY_MONTHLY))
            except ObjectDoesNotExist:
                return Response({"error": "No existe una empresa para este cliente."},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        else:
           return Response({"error": "No existe una empresa para este cliente."},status=status.HTTP_422_UNPROCESSABLE_ENTITY) 
        

        #consultar codigo para cambiar la familia y el producto por medio de un codigo
        if code:
            try:
                promotionCode = PromotionCode.objects.get(code = code)       
                if promotionCode.family:
                    family = promotionCode.family
                if promotionCode.product:
                    client.product = promotionCode.product    
                client.have_promotion_code = True
                client.promotion_code = code
            except:
                pass
                #return Response({"error": "No fue posible aplicar el código."},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        #actualizar los datos del cliente
        try:
            client.names = names
            client.father_last_name = firstLastName
            client.mother_last_name = secondLastName
            client.cellphone = cell
            client.email = email
            client.family_str = company
            client.family = family
            client.labor_old = years
            client.labor_old_month = months
            #client.salary = salary
            client.payment_frequency = payment_frequency
            client.status = Client.CAPTURED_INFORMATION
            client.email_aux = email
            client.cellphone_aux = cell
            client.full_name_aux = str(names) + ' ' + str(firstLastName) + ' ' + str(secondLastName)
            #client.salary_aux = salary
            client.save()
        except:
            return Response({"error": "No fue posible guardar la información"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        return Response( {"success": True})

    #Funcion para cargar los documentos
    @action(methods=["post"], detail=False)
    def activate_client_account_step3(self,request):

        allowed_files = ['image/jpeg', 'image/png', 'application/pdf']
        max_size = 20*1024*1024
        file_obj = request.FILES
        rfc = request.data.get('rfc', None)

        try:
            client = Client.objects.get(rfc=str(rfc))
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)

        if client.created_on_website is False or client.status is not Client.CAPTURED_INFORMATION:
            return Response({"error": "El cliente no cumple con los requisitos sobre su estatus"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        paysheet_one = None
        paysheet_two = None
        paysheet_three = None
        selfie = None


        try:
            if file_obj['paysheet_one']:
                paysheet_one = file_obj['paysheet_one']
        except:
            pass

        try:
            if file_obj['paysheet_two']:
                paysheet_two = file_obj['paysheet_two']
        except:
            pass

        try:
            if file_obj['paysheet_three']:
                paysheet_three = file_obj['paysheet_three']
        except:
            pass

        try:
            if file_obj['selfie']:
                selfie = file_obj['selfie']
        except:
            pass


        errors = {}

        # Format validation
        if paysheet_one is not None:
            if paysheet_one.content_type not in allowed_files:
                errors['paysheet_one'] = "Formato inválido"
        if paysheet_two is not None:
            if paysheet_two.content_type not in allowed_files:
                errors['paysheet_two'] = "Formato inválido"
        if paysheet_three is not None:
            if paysheet_three.content_type not in allowed_files:
                errors['paysheet_three'] = "Formato inválido"
        if selfie is not None:
            if selfie.content_type not in allowed_files:
                errors['selfie'] = "Formato inválido"


        # Size validation
        if paysheet_one is not None:    
            if paysheet_one.size > max_size and 'paysheet_one' not in errors:
                errors['paysheet_one'] = "Tamaño inválido"
        
        if paysheet_two is not None:    
            if paysheet_two.size > max_size and 'paysheet_two' not in errors:
                errors['paysheet_two'] = "Tamaño inválido"
        
        if paysheet_three is not None:    
            if paysheet_three.size > max_size and 'paysheet_three' not in errors:
                errors['paysheet_three'] = "Tamaño inválido"

        if selfie is not None:    
            if selfie.size > max_size and 'selfie' not in errors:
                errors['selfie'] = "Tamaño inválido"

        documents, created = ClientDocuments.objects.get_or_create(client=client)

        if len(errors) > 0:
            return Response(
                    {
                        "errors": errors
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        try:
            if paysheet_one is not None:
                documents.paysheet_one = paysheet_one
            if paysheet_two is not None:
                documents.paysheet_two = paysheet_two
            if paysheet_three is not None:
                documents.paysheet_three = paysheet_three
            if selfie is not None:
                documents.selfie = selfie
            client.status = Client.CAPTURED_DOCUMENTS
            
            client.save()
            documents.save()
        except:
            return Response({"error": "No se han podido guardar los documentos"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
    
        return Response( {"success": True})

    #Funcion para camiar el estatus y enviar pin a sms y correo
    @action(methods=["post"], detail=False)
    def activate_client_account_step4(self,request):
        rfc = request.data.get('rfc', None)

        try:
            client = Client.objects.get(rfc=str(rfc))
        except ObjectDoesNotExist:
            return Response({"error": "No existe un cliente con este RFC"},status=status.HTTP_404_NOT_FOUND)

        if client.user_id is not None:
            return Response({"error": "No cliente ya cuenta con un usuario"},status=status.HTTP_400_BAD_REQUEST)
        
        if client.created_on_website is False or client.status is not Client.CAPTURED_INFORMATION :
            return Response({"error": "El cliente no cumple con los requisitos sobre su estatus"},status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        
        pin = self.pin_generator()
        if pin is False:
            return Response({"error": "No fue posible generar el PIN"},status=status.HTTP_422_UNPROCESSABLE_ENTITY) 
        
        try:   
            client.status = Client.PIN_SENT
            client.pin = str(pin)
            client.save()
        except:
            return Response({"error": "No fue posible guardar el PIN"},status=status.HTTP_422_UNPROCESSABLE_ENTITY) 

        #Enviar pin por sms
        try:
            send_pin = self.send_pin_cellphone(client)
        except:
            pass

        #Enviar pin por email
        try:
            pin_64 = base64.b64encode(str(client.pin).encode())
            new_censored_email_64 = base64.b64encode(client.censored_email.encode())
            send_pin = self.send_pin_email(client,pin_64,new_censored_email_64)
        except:
            pass
        

        return Response( {"success": True})

    #==============================================
    #                  EMPRESA
    #==============================================

    @action(methods=["post"], detail=False)
    def business_message(self,request):
        name = request.data.get('name',None)
        cell = request.data.get('cell', None)

        if name is None or cell is None: 
            return Response({"error": "No fue posible recibir la información"},status=status.HTTP_400_BAD_REQUEST) 


        try:
            subject = 'NUEVA SOLICITUD EMPRESA'
            html_message = render_to_string(
                'business_message.html',
                {
                    'name': str(name),
                    'cell': str(cell),
                }
            )
            plain_message = strip_tags(html_message)
            from_email = config('EMAIL_HOST_USER')
            to = config('BUSINESS_CONTACT_EMAIL')

            mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
        except:
            return Response({"error": "No fue posible enviar la información"},status=status.HTTP_400_BAD_REQUEST) 
        
        return Response( {"success": True})


class RFCValidationView(views.APIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        rfc = request.data.get('rfc', None)

        try:
            while True:
                pin = randrange(1000000)
                pin_64 = base64.b64encode(str(pin).encode())
                #pin_64 = pin_64.replace('=', '')
                pin = format(pin, '06d')
                pin_clients = Client.objects.filter(pin=str(pin))
                if len(pin_clients) == 0:
                    break


            client = Client.objects.get(rfc=rfc)

            if client.invited == False:
                 return Response(
                    {
                        "success": False,
                        "error": "Ocurrió un error. Intenta de nuevo"
                    }
                )

            client.is_active = True
            if client.user == None:
                client.pin = str(pin)
                client.save()
                censored_email = client.email.split('@')
                new_censored_email = censored_email[0][0] + ('*'*(len(censored_email[0])-2)) + censored_email[0][len(censored_email[0]) - 1] + '@' + censored_email[1]
                new_censored_email_64 = base64.b64encode(new_censored_email.encode())
                # New mail
                self.send_pin(client, pin, pin_64, new_censored_email_64)
    #             email = EmailMessage(
    #                 'Confirma tu cuenta',
    # """
    # Hola {0} {1} {2}<br>
    # <br>
    # Tu PIN de confirmación es el siguiente: {3}<br>
    # <br>
    # Activa tu cuenta aquí: <a href="{4}" style="color:#1C7BFF; text-decoration: underline;">{4}</a> y dispón de tu crédito en menos de 24 horas. <br>
    # <br>
    # Cualquier duda o comentario, contáctanos vía email a <a href="mailto:contacto@ixhua.mx" style="color:#1C7BFF; text-decoration: underline;">contacto@ixhua.mx</a> o comunícate a nuestro Whatsapp 331-913-6707 <br>
    # <br>
    # Consulta nuestro aviso de privacidad aquí www.xxxxxxx.xxxxx.mx
    # """.format(client.names, client.father_last_name, client.mother_last_name, pin, config('FRONT_URL') + "pin/" + pin_64.decode('utf-8').replace('=', '') + '/' + new_censored_email_64.decode('utf-8').replace('=', ''))
    #                 ,
    #                 to=[client.email])
    #             email.content_subtype = "html"
    #             email.send()
                return Response(
                        {
                            "success": True,
                            "email": new_censored_email
                        }
                    )
            else:
                if client.user.check_password('PinValid1'):
                    client.pin = str(pin)
                    client.save()
                    censored_email = client.email.split('@')
                    new_censored_email = censored_email[0][0] + ('*'*(len(censored_email[0])-2)) + censored_email[0][len(censored_email[0]) - 1] + '@' + censored_email[1]
                    new_censored_email_64 = base64.b64encode(new_censored_email.encode())
                    # New mail
                    self.send_pin(client, pin, pin_64, new_censored_email_64)

    #                 email = EmailMessage(
    #                 'Confirma tu cuenta',
    # """
    # Hola {0} {1} {2} <br>
    # <br>
    # Tu PIN de confirmación es el siguiente: {3} <br>
    # <br>
    # Activa tu cuenta aquí: <a href="{4}" style="color:#1C7BFF; text-decoration: underline;">{4}</a> y dispón de tu crédito en menos de 24 horas. <br>
    # <br>
    # Cualquier duda o comentario, contáctanos vía email a <a href="mailto:contacto@ixhua.mx" style="color:#1C7BFF; text-decoration: underline;">contacto@ixhua.mx</a> o comunícate a nuestro Whatsapp 331-913-6707 <br>
    # <br>
    # Consulta nuestro aviso de privacidad aquí www.xxxxxxx.xxxxx.mx
    # """.format(client.names, client.father_last_name, client.mother_last_name, pin, config('FRONT_URL') + "pin/" + pin_64.decode('utf-8').replace('=', '') + '/' + new_censored_email_64.decode('utf-8').replace('=', ''))
    #                 ,
    #                 to=[client.email])
    #                 email.content_subtype = "html"
    #                 email.send()
                    return Response(
                            {
                                "success": True,
                                "email": new_censored_email
                            }
                        )
                else:
                    return Response(
                        {
                            "success": False,
                            "error": "Ya existe un usuario relacionado a ese RFC"
                        }
                    )

        except Client.DoesNotExist:
            return Response(
                    {
                        "success": False,
                        "error": "RFC inválido. Intenta de nuevo"
                    }
                )

    @classmethod
    def get_extra_actions(cls):
        return []


    def send_pin(self, client, pin, pin_64, censored_email_64):
        # Send PIN email
        subject = 'PIN DE CONFIRMACIÓN'
        html_message = render_to_string(
            'base.html',
            {
                'title': 'PIN DE CONFIRMACIÓN',
                'content_template': 'content_pin.html',
                'contact_template': 'contact.html',
                'names': client.names,
                'father_last_name': client.father_last_name,
                'mother_last_name': client.mother_last_name,
                'pin': pin,
                'url': config('FRONT_URL') + "pin/" + pin_64.decode('utf-8').replace('=', '') + '/' + censored_email_64.decode('utf-8').replace('=', ''),
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = client.email

        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)

class PINValidationView(views.APIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        pin = request.data.get('pin', None)
        pin_clients = Client.objects.filter(pin=str(pin))

        if pin_clients.count() > 0:
            # fist_name = pin_clients[0].names.split(' ')[0]
            # middle_name = ' '.join([name for name in pin_clients[0].names.split(' ')[1:]])
            first_name = pin_clients[0].names
            middle_name = ' '
            client = Client.objects.get(pin=str(pin))
            if client.user == None:
                users = CustomUser.objects.filter(email=pin_clients[0].email)
                if users.count() > 0:
                    return Response(
                            {
                                "success": False,
                                "error": "El correo que intenta registrar ya existe"
                            }, status = status.HTTP_409_CONFLICT
                        )
                try:
                    user, created = CustomUser.objects.get_or_create(
                        email=pin_clients[0].email,
                        first_name=first_name,
                        middle_name=middle_name,
                        father_last_name=pin_clients[0].father_last_name,
                        mother_last_name=pin_clients[0].mother_last_name,
                        role=5,
                        phone=pin_clients[0].phone,
                        cellphone=pin_clients[0].cellphone,
                        #password=make_password("PinValid1"),
                    )
                    user.password = make_password("PinValid1")
                    user.save()
                    client.user = user
                    client.save()
                except:
                    return Response(
                            {
                                "success": False,
                                "error": "No se puede activar esta cuenta"
                            }
                        )

                return Response(
                                {
                                    "success": True
                                }
                            )
            else:
                if client.user.check_password('PinValid1'):
                    return Response(
                                {
                                    "success": True
                                }
                            )
                else:
                    return Response(
                                    {
                                        "success": False,
                                        "error": "Código PIN inválido, favor de verificar"
                                    }
                                )

        else:
            return Response(
                        {
                            "success": False,
                            "error": "Código PIN inválido, favor de verificar"
                        }
                    )


    @classmethod
    def get_extra_actions(cls):
        return []


class PINChangePasswordView(views.APIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        pin = request.data.get('pin', None)
        password = request.data.get('password', None)
        try:
            client = Client.objects.get(pin=str(pin))
        except Client.DoesNotExist:
            return Response(
                    {
                        "success": False,
                        "error": "Pin invalido"
                    }
                )

        try:
            client.user.password=make_password(password)
            client.user.save()
            token, created = Token.objects.get_or_create(user=client.user)
            client.pin = ""
            client.save()
            self.send_confirmation(client.email)
            return Response(
                    {
                        "success": True,
                        "token": token.key
                    }
                )
        except:
            return Response(
                    {
                        "success": False,
                        "error": "Password no valido"
                    }
                )

    def send_confirmation(self, client_email):
        # Send confirmation email
        subject = 'Tu cuenta ha sido creada'
        html_message = render_to_string(
            'base.html',
            {
                'title': 'Tu cuenta ha sido creada',
                'content_template': 'content_confirmation.html',
                'contact_template': 'contact.html',
                'url': config('FRONT_URL') + "auth/login/",
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = client_email

        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
    #     email = EmailMessage(
    #                 'Tu cuenta ha sido creada',
    # """
    # ¡Bienvenido a IXHUA! <br>
    # <br>
    # Aprovecha y dispón de tu crédito en menos de 24 horas. <br>
    # <br>
    # Haz click aquí: <a href="{0}" style="color:#1C7BFF; text-decoration: underline;">{0}</a> <br>
    # <br>
    # Cualquier duda o comentario, contáctanos vía email a <a href="mailto:contacto@ixhua.mx" style="color:#1C7BFF; text-decoration: underline;">contacto@ixhua.mx</a> o comunícate a nuestro whatsapp 331-913-6707 Consulta nuestro aviso de privacidad aquí www.xxxxxxx.xxxxx.mx <br>
    # """.format(config('FRONT_URL') + "auth/login/")
    #                 ,
    #                 to=[client_email])
    #     email.content_subtype = "html"
    #     email.send()

    @classmethod
    def get_extra_actions(cls):
        return []


class LoginView(views.APIView):
    permission_classes = []


    def post(self, request, *args, **kwargs):
        rfc = request.data.get('rfc', None)
        password = request.data.get('password', None)

        try:
            client = Client.objects.get(rfc=str(rfc))
            if client.user == None:
                return Response(
                    {
                        "error": "Log In inválido. Por favor activa tu cuenta"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )
            else:
                if client.user.is_active is False:
                    return Response(
                        {
                            "error": "Esta cuenta ha sido deshabilitada, por favor contactanos al correo contacto@ixhua.mx"
                        }
                        , status=status.HTTP_401_UNAUTHORIZED
                    )

            if client.user.check_password(password):
                token, created = Token.objects.get_or_create(user=client.user)
                
                #save last login and login (front end)
                try:
                    now = datetime.datetime.now()
                    settings.TIME_ZONE  # 'UTC'
                    now = make_aware(now)
                    client.user.last_login = now
                    client.user.log_count += 1
                    client.user.save()
                except:
                    pass

                #send email
                try:
                    self.send_login_to_admin(client.rfc)
                except:
                    pass

                return Response(
                    {
                        "token": token.key,
                    }
                )
            else:
                return Response(
                    {
                        "error": "RFC o contraseña inválidos"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        except Client.DoesNotExist:
            return Response(
                    {
                        "error": "RFC o contraseña inválidos"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

    @classmethod
    def get_extra_actions(cls):
        return []

    def send_login_to_admin(self, client):
        send_mail(
        'Inicio de sesión: ' + str(client),
        'EL USUARIO ' + str(client) + ' HA INICIADO SESIÓN',
        config('EMAIL_HOST_USER'),
        [config('CONTACT_EMAIL')],
        )


class RecoveryPasswordMailView(views.APIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        rfc = request.data.get('rfc', None)

        try:
            client = Client.objects.get(rfc=str(rfc))
            if client.user == None:
                return Response(
                    {
                        "error": "Tu cuenta aún no ha sido activada. Por favor activa tu cuenta"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        except Client.DoesNotExist:
            return Response(
                    {
                        "error": "RFC inválidos"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        # Mail generation
        token, created = ClientToken.objects.get_or_create(client=client)

        if created:
            ts = int(datetime.datetime.now().timestamp()+ (24 * 3600))
            token.token = uuid4()
            token.expire = ts
            token.save()
        else:
            ts = int(datetime.datetime.now().timestamp()+ (24 * 3600))
            token.expire = ts
            token.save()

        ts_64 = base64.b64encode(str(token.expire).encode())
        url = config('FRONT_URL') + "auth/reset/" + token.token + '/' + ts_64.decode('utf-8'.replace('=', ''))
        self.send_recovery(client, url)

    #     email = EmailMessage(
    #                 'Confirma tu cuenta',
    # """
    # Hola {0} {1} {2} <br>
    # <br>
    # Restablece tu contraseña aquí: <a href="{3}" style="color:#1C7BFF; text-decoration: underline;">{3}</a> <br>
    # <br>
    # Cualquier duda o comentario, contáctanos vía email a <a href="mailto:contacto@ixhua.mx" style="color:#1C7BFF; text-decoration: underline;">contacto@ixhua.mx</a> o comunícate a nuestro Whatsapp 331-913-6707 <br>
    # <br>
    # Consulta nuestro aviso de privacidad aquí www.xxxxxxx.xxxxx.mx
    # """.format(client.names, client.father_last_name, client.mother_last_name, config('FRONT_URL') + "auth/reset/" + token.token + '/' + ts_64.decode('utf-8'.replace('=', '')))
    #                 ,
    #                 to=[client.email])
    #     email.content_subtype = "html"
    #     email.send()

        return Response(
                    {
                        "success": True, "email":client.email
                    }
                )


    def send_recovery(self, client, url):
        # Send recovery email
        subject = 'Confirma tu cuenta'
        html_message = render_to_string(
            'base.html',
            {
                'title': 'Confirma tu cuenta',
                'content_template': 'content_reset_password.html',
                'contact_template': 'contact.html',
                'names': client.names,
                'father_last_name': client.father_last_name,
                'mother_last_name': client.mother_last_name,
                'url': url,
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = client.email

        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)

    @classmethod
    def get_extra_actions(cls):
        return []


class RecoveryPasswordView(views.APIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        token = request.data.get('token', None)
        password = request.data.get('password', None)
        try:
            client_token = ClientToken.objects.get(token=token)
        except ClientToken.DoesNotExist:
            return Response(
                    {
                        "error": "Token inválido"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        if int(datetime.datetime.now().timestamp()) > client_token.expire:
            return Response(
                    {
                        "error": "Lo sentimos, este link ha expirado"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        try:
            client_token.client.user.password=make_password(password)
            client_token.client.user.save()
            ts = int(datetime.datetime.now().timestamp())
            client_token.expire = ts
            client_token.save()
        except:
            return Response(
                    {
                        "error": "No se pudo cambiar el password"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        token, created = Token.objects.get_or_create(user=client_token.client.user)

        return Response(
                    {
                        "token": token.key,
                    }
                )

    @classmethod
    def get_extra_actions(cls):
        return []


class WhoAmIView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        display_name = request.user.first_name

        if request.user.middle_name != None and request.user.middle_name != ' ' and request.user.middle_name != '':
            display_name += ' ' + request.user.middle_name

        display_name += ' ' + request.user.father_last_name

        if request.user.mother_last_name != None and request.user.mother_last_name != ' ' and request.user.mother_last_name != '':
            display_name += ' ' + request.user.mother_last_name

        return Response(
                    {
                        "success": True,
                        "user": {
                            "first_name": request.user.first_name,
                            "middle_name": request.user.middle_name,
                            "father_last_name": request.user.father_last_name,
                            "mother_last_name": request.user.mother_last_name,
                            "display_name": display_name,
                            "role": request.user.role,
                            "email": request.user.email,
                            "phone": request.user.phone,
                            "cellphone": request.user.cellphone,
                        }
                    }
                )

    @classmethod
    def get_extra_actions(cls):
        return []


class BasicLoanInfoView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            client = Client.objects.get(user=request.user)
            seeds = client.seeds
            total_loan = client.loan_max_amount
            salary_worked = client.salary_worked
            active_loan = client.amount_held
            available_loan = client.loan_amount_available
            min_loan = client.loan_min_amount
            loan_unit = client.loan_unit
            days_to_loan = client.days_to_loan
            days_worked = client.days_worked
            loan_requirements = None

            return Response({
                'salary_worked': salary_worked,
                'total_loan': total_loan,
                'active_loan': active_loan,
                'available_loan': available_loan,
                'min_loan': min_loan,
                'loan_unit': loan_unit,
                'days_worked': days_worked,
                "days_to_loan": days_to_loan,
                'seeds': {
                    'gray': seeds.gray,
                    'green': seeds.green,
                    'yellow': seeds.yellow,
                },
                'loan_requirements': {
                    'selfie': 1,
                    'bank_account': 1,
                }
            })
        except Client.DoesNotExist:
            return Response(
                {"error": "Token inválido, usuario no es un cliente"},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )


    @classmethod
    def get_extra_actions(cls):
        return []


def calculate_cat(days, amount, total):
    tir_list = [0] * (days-1)
    tir_list.insert(0,-amount)
    tir_list.append(total)
    tir = np.irr(tir_list)
    cat = ((1+tir)**360) -1

    return cat * 100


def calculate_interest(client):
    loans = Loan.objects.filter(client=client)
    seed = Seeds.objects.filter(loan__in=list(loans)).order_by('-pk').first()
    adjustment = ClientAdjustment.objects.filter(client=client).order_by('-pk').first()


    if adjustment is None and seed is None:
        gray = 0
        green = 5
        yellow = 0
    elif adjustment is None:
        gray = seed.gray
        green = seed.green
        yellow = seed.yellow
    elif seed is None:
        gray = adjustment.gray
        green = adjustment.green
        yellow = adjustment.yellow
    else:
        if adjustment.created_at > seed.created_at:
            gray = adjustment.gray
            green = adjustment.green
            yellow = adjustment.yellow
        else:
            gray = seed.gray
            green = seed.green
            yellow = seed.yellow

    if yellow > 0:
        interest = 18 - yellow
    elif gray > 0:
        interest = 18 + gray
    else:
        interest = 18

    # if seed is not None:
    #     if yellow > 0:
    #         interest = 18 - yellow
    #     elif gray > 0:
    #         interest = 18 + gray
    #     else:
    #         interest = 18
    # else:
    #     interest = 18


    interest = interest / 100
    interest = (interest * 12) / 360
    return interest

class LoanCalculatorView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            amount = int(request.GET.get('amount', 0))
            client = Client.objects.get(user=request.user)
            options = client.get_options(amount)
            def option_resource(option):
                return {
                    'date': option['date'],
                    'term': option['term'],
                    'interest': float("{0:.2f}".format(option['interest'])),
                    'tfa': float("{0:.2f}".format(option['tfa'])),
                    'aperture': float("{0:.2f}".format(option['aperture'])),
                    'total': float("{0:.2f}".format(option['total'])),
                    'cat': float("{0:.2f}".format(option['cat'])),
                    'tfa_letters': numero_to_letras(option['tfa'], False),
                }
            options = list(map(option_resource, options))
            rate = options[0]['tfa'] if len(options) > 0 else 0
        except Client.DoesNotExist:
            return Response(
                {"error": "Token inválido, usuario no es un cliente"},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )
        return Response({
            'options': options,
            'extra_data': {
                'account_type': 'NOMINA',
                'amount': float("{0:.2f}".format(amount)),
                'amount_letters': numero_to_letras(amount, True),
                'regular_interest_anual': rate,
            }
        })

class MyLoanCalculatorView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        amount = int(request.GET.get('amount', -1))
        try:
            client = Client.objects.get(user=request.user)
        except ClientToken.DoesNotExist:
            return Response(
                    {
                        "error": "Token inválido, usuario no es un cliente"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        total_loan = client.credit_amount
        active_loan = 0
        loan_payment = 0

        loans = Loan.objects.filter(client=client, state=2)

        for loan in loans:
            active_loan += loan.amount_requested
            payments = PartialPayment.objects.filter(loan=loan)

            for payment in payments:
                loan_payment += payment.amount
                if loan_payment > loan.amount_requested:
                    loan_payment = loan.amount_requested

            active_loan -= loan_payment
            loan_payment = 0

        available_loan = total_loan - active_loan

        option1, option2, days1, days2 = self.get_options(client.family)
        interest = calculate_interest(client)

        interest_to_pay1 = (amount*interest*days1) + (amount*interest*days1)*(0.16)
        full_payment1 = amount + interest_to_pay1
        interest_to_pay2 = (amount*interest*days2) + (amount*interest*days2)*(0.16)
        full_payment2 = amount + interest_to_pay2

        cat1 = calculate_cat(days1, amount, (amount*interest*days1) + amount)
        cat2 = calculate_cat(days2, amount, (amount*interest*days2) + amount)

        amount_letters = self.numero_to_letras(amount)

        interest1 = round(interest * 100 * 360, 2)
        interest2 = round(interest * 100 * 360, 2)

        interest_to_pay1_letters = self.numero_to_letras(interest1, False)
        interest_to_pay2_letters = self.numero_to_letras(interest2, False)

        # # Rendered pdf
        # loan = Loan.objects.get(request_no='201908002')
        # # all_data = request.data.get('extra_data', None)
        # contract_data = hardcodedData()
        # # contract_data = addData(contract_data, client, all_data)

        # html_string = render_to_string('contract.html', contract_data)
        # html = HTML(string=html_string)

        # result = html.write_pdf(stylesheets=['templates/contract.css'])

        # # loan.contract_no = contract_no
        # # loan.save()
        # loan.contract.save('out.pdf', File(BytesIO(result)))



        if available_loan >= amount:
            return Response(
                        {
                            "option1": option1.strftime('%d/%m/%Y'),
                            "interest_to_pay1": float("{0:.2f}".format(interest_to_pay1)),
                            "full_payment1": float("{0:.2f}".format(full_payment1)),
                            "cat1" : float("{0:.2f}".format(cat1)),
                            "option2": option2.strftime('%d/%m/%Y'),
                            "interest_to_pay2": float("{0:.2f}".format(interest_to_pay2)),
                            "full_payment2": float("{0:.2f}".format(full_payment2)),
                            "cat2" : float("{0:.2f}".format(cat2)),
                            "tfa": float("{0:.2f}".format((interest*360*100))),
                            'extra_data': {
                                "account_type": "NOMINA",
                                "amount": float("{0:.2f}".format(amount)),
                                "amount_letters": amount_letters,
                                "term1": days1,
                                "term2": days2,
                                "interest1": interest1,
                                "interest2": interest2,
                                "interest_to_pay1_letters": interest_to_pay1_letters,
                                "interest_to_pay2_letters": interest_to_pay2_letters,
                                "regular_interest_anual": float("{0:.2f}".format(interest*100*360)),
                            },
                        }
                    )
        else:
            return Response(
                    {
                        "error": "El monto solicitado es mayor que el permitido"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )


    def utc_to_time(self, naive, timezone="America/Mexico_City"):
        return naive.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(timezone))


    def diff_timezone(self, timezone="America/Mexico_City"):
        utcnow = pytz.timezone('utc').localize(datetime.datetime.utcnow())
        here = utcnow.astimezone(pytz.timezone('America/Mexico_City')).replace(tzinfo=None)
        there = utcnow.astimezone(pytz.timezone('UTC')).replace(tzinfo=None)

        offset = relativedelta(there, here)

        return offset.hours


    def get_options(self, family):
        diff = self.diff_timezone()
        hour = datetime.datetime.now()
        today = datetime.date.today()

        limit = 18.5

        hour_limit = datetime.datetime(
            year=today.year,
            month=today.month,
            day=today.day,
        )

        hour_limit = hour_limit + datetime.timedelta(hours = limit) + datetime.timedelta (hours = diff)
        days = 0

        today_plus = today

        if hour > hour_limit:
            today_plus = today_plus + datetime.timedelta (days = 1)
            days += 1

        while True:
            original_date = today_plus
            today_plus, days = self.check_weekend(today_plus, days)
            today_plus, days = self.check_holidays(today_plus, days)

            if original_date == today_plus:
                break


        today_plus = today_plus + datetime.timedelta (days = (6-days))

        options = Paydays.objects.filter(day__gte=today_plus - datetime.timedelta (days = days), family=family).order_by('day')[:2]

        if len(options) < 2:
            return Response(
                    {
                        "error": "No hay fechas disponibles"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        next_payday = options[0].day
        next_next_payday = options[1].day

        # Seleccionar el algoritmo indicado
        # if family.period == Period.decennial.id:
        #     next_payday, next_next_payday = self.decennial(today, family)
        # elif family.period == Period.biweekly.id:
        #     next_payday, next_next_payday = self.biweekly(today, family)
        # elif family.period == Period.fortnightly.id:
        #     next_payday, next_next_payday = self.fortnightly(today)
        # elif family.period == Period.monthly.id:
        #     next_payday, next_next_payday = self.monthly(today, family)


        # Verifica si es fin de semana o dia festivo
        # while True:
        #     original_date = next_payday
        #     next_payday = self.check_weekend(next_payday)
        #     next_payday = self.check_holidays(next_payday)

        #     if original_date == next_payday:
        #         break


        # while True:
        #     original_date = next_next_payday
        #     next_next_payday = self.check_holidays(next_next_payday)
        #     next_next_payday = self.check_weekend(next_next_payday)

        #     if original_date == next_next_payday:
        #         break

        # Termina Verifica si es fin de semana o dia festivo


        days1 = (next_payday - today).days - days
        days2 = (next_next_payday - today).days - days


        return next_payday, next_next_payday, days1, days2


    def check_weekend(self, date, days):
        if date.weekday() == 5:
            date = date + datetime.timedelta (days = 2)
            days +=2
        elif date.weekday() == 6:
            date = date + datetime.timedelta (days = 1)
            days +=1

        return date, days

    def check_holidays(self, date, days):
        holidays = Holidays.objects.filter(day=date)
        if holidays.count() > 0:
            date = date + datetime.timedelta (days = 1)
            days +=1
        return date, days


    # Decenal
    def decennial(self, today, family):
        days_month_today = ((datetime.date(today.year, today.month, 1) + relativedelta(months=+1)) - datetime.date(today.year, today.month, 1)).days
        days_next_month_today = ((datetime.date(today.year, today.month, 1) + relativedelta(months=+2)) - (datetime.date(today.year, today.month, 1)) + relativedelta(months=+2)).days
        days_month_family = ((datetime.date(family.payday.year, family.payday.month, 1) + relativedelta(months=+1)) - datetime.date(family.payday.year, family.payday.month, 1)).days

        days = []

        # Determina que dias del mes podrian pagar en caso ideal 30 dias
        if family.payday.day <= 13:
            days.append(family.payday.day)
            days.append(family.payday.day+10)
            days.append(family.payday.day+20)
        elif family.payday.day <= 20:
            days.append(family.payday.day-10)
            days.append(family.payday.day)
            days.append(family.payday.day+10)
        else:
            if family.payday.day != days_month_family:
                days.append(family.payday.day-20)
                days.append(family.payday.day-10)
                days.append(family.payday.day)
            else:
                days.append(10)
                days.append(20)
                days.append(30)


        if today.day < days[0]:
            next_payday = datetime.date(today.year, today.month, days[0])
            next_next_payday = next_payday + relativedelta(days=+10)
        elif today.day < days[1]:
            next_payday = datetime.date(today.year, today.month, days[1])

            if next_payday.day == 20:
                next_next_payday = datetime.date(next_payday.year, today.month, days_month_today)
            else:
                if days_month_today != 30:
                    next_next_payday = next_payday + relativedelta(days=+10)
                else:
                    next_next_payday = next_payday + relativedelta(days=+(10+(days_month_today-30)))
        elif today.day < days[2]:
            if days[2] == 30:
                next_payday = datetime.date(today.year, today.month, days_month_today)
            else:
                next_payday = datetime.date(today.year, today.month, days[2])

            if days_month_today != 30:
                next_next_payday = next_payday + relativedelta(days=+(10+(days_month_today-30)))
            else:
                next_next_payday = next_payday + relativedelta(days=+10)


        if (next_payday - today).days <= 5:
            next_payday = next_next_payday

            if next_payday.day == 20:
                next_next_payday = datetime.date(next_payday.year, today.month, days_month_today)
            else:
                if next_payday.day < 20:
                    next_next_payday = next_payday + relativedelta(days=+(10))
                else:
                    next_next_payday = next_payday + relativedelta(days=+(10+(days_month_today-30)))


        return next_payday, next_next_payday


    # Catorcenal
    def biweekly(self, today, family):
        days_diff = (today - family.payday).days
        biweekly = int(days_diff/14) + 1

        # Determina los 2 siguientes dias de pago sin validar cuantos dias faltan para el mas cercano
        next_payday = family.payday + relativedelta(weeks=+(biweekly*2))
        next_next_payday = next_payday + relativedelta(weeks=+2)

        # Se recorre al siguiente periodo de pago si faltan menos de 6 dias
        if (next_payday - today).days <= 5:
            next_payday = next_next_payday
            next_next_payday = next_payday + relativedelta(weeks=+2)

        return next_payday, next_next_payday


    # Quincenal
    def fortnightly(self, today):
        if today.day > 15:
            year = today.year if today.month < 12 else today.year + 1
            month = today.month + 1 if today.month < 12 else 1
            next_payday = datetime.date(year, month, 1) - datetime.timedelta (days = 1)
        else:
            next_payday = datetime.date (today.year, today.month, 15)

        if (next_payday - today).days <=5:
            if next_payday.day > 15:
                year = next_payday.year if next_payday.month < 12 else next_payday.year + 1
                month = next_payday.month + 1 if next_payday.month < 12 else 1
                next_payday = datetime.date (year, month, 1) + datetime.timedelta (days = 14)
                year = next_payday.year if next_payday.month < 12 else next_payday.year + 1
                month = next_payday.month + 1 if next_payday.month < 12 else 1
                next_next_payday = datetime.date (year, month, 1) - datetime.timedelta (days = 1)
            else:
                year = next_payday.year if next_payday.month < 12 else next_payday.year + 1
                month = next_payday.month + 1 if next_payday.month < 12 else 1
                next_payday = datetime.date (year, month, 1) - datetime.timedelta (days = 1)
                next_next_payday = datetime.date (year, month, 1) + datetime.timedelta (days = 14)
        else:
            if next_payday.day > 15:
                next_next_payday = datetime.date (next_payday.year, next_payday.month + 1, 1) + datetime.timedelta (days = 14)
            else:
                year = next_payday.year if next_payday.month < 12 else next_payday.year + 1
                month = next_payday.month + 1 if next_payday.month < 12 else 1
                next_next_payday = datetime.date (year, month, 1) - datetime.timedelta (days = 1)

        return next_payday, next_next_payday


    # Mensual
    def monthly(self, today, family):
        # Determina los 2 siguientes dias de pago sin validar cuantos dias faltan para el mas cercano
        if family.payday.day <= today.day:
            next_payday = datetime.date(today.year, today.month, family.payday.day) + relativedelta(months=+1)
            next_next_payday = next_payday + relativedelta(months=+1)
        else:
            next_payday = datetime.date(today.year, today.month, family.payday.day)
            next_next_payday = next_payday + relativedelta(months=+1)

        # Se recorre al siguiente periodo de pago si faltan menos de 6 dias
        if (next_payday - today).days <= 5:
            next_payday = next_next_payday
            next_next_payday = next_payday + relativedelta(months=+1)


        return next_payday, next_next_payday


    def numero_to_letras(self, numero, money=True):
        indicador = [("",""),("MIL","MIL"),("MILLON","MILLONES"),("MIL","MIL"),("BILLON","BILLONES")]
        entero = int(numero)
        decimal = int(round((round(numero, 2) - entero)*100))
        decimal_copy = decimal
        contador = 0
        numero_letras = ""
        decimal_letras = ""

        while entero >0:
            a = entero % 1000
            if contador == 0:
                en_letras = self.convierte_cifra(a,1).strip()
            else :
                en_letras = self.convierte_cifra(a,0).strip()
            if a==0:
                numero_letras = en_letras+" "+numero_letras
            elif a==1:
                if contador in (1,3):
                    numero_letras = indicador[contador][0]+" "+numero_letras
                else:
                    numero_letras = en_letras+" "+indicador[contador][0]+" "+numero_letras
            else:
                numero_letras = en_letras+" "+indicador[contador][1]+" "+numero_letras
            numero_letras = numero_letras.strip()
            contador = contador + 1
            entero = int(entero / 1000)

        contador = 0
        while decimal >0:
            a = decimal % 1000
            if contador == 0:
                en_letras = self.convierte_cifra(a,1).strip()
            else :
                en_letras = self.convierte_cifra(a,0).strip()
            if a==0:
                decimal_letras = en_letras+" "+decimal_letras
            elif a==1:
                if contador in (1,3):
                    decimal_letras = indicador[contador][0]+" "+decimal_letras
                else:
                    decimal_letras = en_letras+" "+indicador[contador][0]+" "+decimal_letras
            else:
                decimal_letras = en_letras+" "+indicador[contador][1]+" "+decimal_letras
            decimal_letras = decimal_letras.strip()
            contador = contador + 1
            decimal = int(decimal / 1000)

        total_letras = ""

        if money:
            total_letras += numero_letras + " PESOS {}/100 M.N.".format(decimal_copy)
        else:
            if decimal_copy < 10:
                decimal_letras = " CERO " + decimal_letras

            total_letras += numero_letras + " PUNTO {} POR CIENTO".format(decimal_letras)

        total_letras = total_letras.replace("  ", " ")

        return total_letras


    def convierte_cifra(self, numero, sw):
        lista_centana = ["",("CIEN","CIENTO"),"DOSCIENTOS","TRESCIENTOS","CUATROCIENTOS","QUINIENTOS","SEISCIENTOS","SETECIENTOS","OCHOCIENTOS","NOVECIENTOS"]
        lista_decena = ["",("DIEZ","ONCE","DOCE","TRECE","CATORCE","QUINCE","DIECISEIS","DIECISIETE","DIECIOCHO","DIECINUEVE"),
                        ("VEINTE","VEINTI"),("TREINTA","TREINTA Y "),("CUARENTA" , "CUARENTA Y "),
                        ("CINCUENTA" , "CINCUENTA Y "),("SESENTA" , "SESENTA Y "),
                        ("SETENTA" , "SETENTA Y "),("OCHENTA" , "OCHENTA Y "),
                        ("NOVENTA" , "NOVENTA Y ")
                    ]
        lista_unidad = ["",("UN" , "UNO"),"DOS","TRES","CUATRO","CINCO","SEIS","SIETE","OCHO","NUEVE"]
        centena = int (numero / 100)
        decena = int((numero -(centena * 100))/10)
        unidad = int(numero - (centena * 100 + decena * 10))
        #print "centena: ",centena, "decena: ",decena,'unidad: ',unidad

        texto_centena = ""
        texto_decena = ""
        texto_unidad = ""

        #Validad las centenas
        texto_centena = lista_centana[centena]
        if centena == 1:
            if (decena + unidad)!=0:
                texto_centena = texto_centena[1]
            else :
                texto_centena = texto_centena[0]

        #Valida las decenas
        texto_decena = lista_decena[decena]
        if decena == 1 :
            texto_decena = texto_decena[unidad]
        elif decena > 1 :
            if unidad != 0 :
                texto_decena = texto_decena[1]
            else:
                texto_decena = texto_decena[0]
        #Validar las unidades
        #print "texto_unidad: ",texto_unidad
        if decena != 1:
            texto_unidad = lista_unidad[unidad]
            if unidad == 1:
                texto_unidad = texto_unidad[sw]

        return "%s %s %s" %(texto_centena,texto_decena,texto_unidad)


    @classmethod
    def get_extra_actions(cls):
        return []

class ClientInfoView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            client = Client.objects.get(user=request.user)
        except ClientToken.DoesNotExist:
            return Response(
                    {
                        "error": "Token inválido, usuario no es un cliente"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        loan = Loan.objects.filter(client=client)
        data = serializers.serialize('json', [client,])
        struct = json.loads(data)

        # Remove unneeded fields
        struct[0]['fields'].pop("admission_date")
        struct[0]['fields'].pop("payment_frequency")
        struct[0]['fields'].pop("pin")
        struct[0]['fields'].pop("credit_amount")
        struct[0]['fields'].pop("subfamily")
        struct[0]['fields'].pop("user")
        struct[0]['fields'].pop("tell_us")
        struct[0]['fields']['family'] = client.family.tradename

        data = {
            "first": False,
            "form": struct[0]['fields']
        }

        data = json.dumps(data)

        if loan.count() == 0:

            if client.created_on_website == True:

                if struct[0]['fields']['actual_position'] == "Sin Puesto":
                    struct[0]['fields'].pop("actual_position")
                
                if struct[0]['fields']['bank_name'] == "Sin Banco":
                    struct[0]['fields'].pop("bank_name")
                
                if struct[0]['fields']['bank_clabe'] == "000000000000000000":
                    struct[0]['fields'].pop("bank_clabe")
                
                if struct[0]['fields']['address'] == "Sin Direccion":
                    struct[0]['fields'].pop("address")

                if struct[0]['fields']['phone'] == "0000000000":
                    struct[0]['fields'].pop("phone")


                return Response({"first": True,"form": struct[0]['fields']})

            return Response(
                            {
                                "first": True,
                                "form": {
                                    "rfc": client.rfc,
                                    #"email": client.email,
                                    "family": client.family.tradename,
                                }
                            }
                        )
        else:
            return HttpResponse(
                data, content_type='application/json'
            )


    @classmethod
    def get_extra_actions(cls):
        return []


class FamiliesInfoView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            client = Client.objects.get(user=request.user)
        except ClientToken.DoesNotExist:
            return Response(
                    {
                        "error": "Token inválido, usuario no es un cliente"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        families = Family.objects.filter()
        families_list = []

        for family in families:
            #families_list.append(family.tradename)
            families_list.append(
                {
                    "id": family.pk,
                    "name": family.tradename,
                }
            )

        return Response(
                            {
                                "families": families_list
                            }
                        )

    @classmethod
    def get_extra_actions(cls):
        return []


class ClientFileUploadView(views.APIView):
    parser_classes = (JSONParser, MultiPartParser)
    permission_classes = (IsAuthenticated,)

    def post(self, request,  format=None):
        try:
            client = Client.objects.get(user=request.user)
        except ClientToken.DoesNotExist:
            return Response(
                    status=status.HTTP_401_UNAUTHORIZED
                )

        allowed_files = ['image/jpeg', 'image/png', 'application/pdf']
        max_size = 20*1024*1024
        file_obj = request.FILES

        ine = None
        bank_account = None #file_obj['bank_account']
        proof_address = None
        additional_file = None
        
        try:
            if file_obj['additional_file']:
                additional_file = file_obj['additional_file']
        except:
            pass

        try:
            if file_obj['ine']:
                ine = file_obj['ine']
        except:
            pass

        try:
            if file_obj['bank_account']:
                bank_account = file_obj['bank_account']
        except:
            pass

        try:
            if file_obj['proof_address']:
                proof_address = file_obj['proof_address']
        except:
            pass

        
        errors = {}

        # Format validation
        if ine is not None:
            if ine.content_type not in allowed_files:
                errors['ine'] = "Formato inválido"
        if bank_account is not None:
            if bank_account.content_type not in allowed_files:
                errors['ibank_accountne'] = "Formato inválido"
        if proof_address is not None:
            if proof_address.content_type not in allowed_files:
                errors['proof_address'] = "Formato inválido"
        if additional_file is not None:
            if additional_file.content_type not in allowed_files:
                errors['additional_file'] = "Formato inválido"


        # Size validation
        if ine is not None:
            if ine.size > max_size and 'ine' not in errors:
                errors['ine'] = "Tamaño inválido"

        if bank_account is not None:
            if bank_account.size > max_size and 'bank_account' not in errors:
                errors['bank_account'] = "Tamaño inválido"

        if proof_address is not None:
            if proof_address.size > max_size and 'proof_address' not in errors:
                errors['proof_address'] = "Tamaño inválido"

        if additional_file is not None:    
            if additional_file.size > max_size and 'additional_file' not in errors:
                errors['additional_file'] = "Tamaño inválido"

        documents, created = ClientDocuments.objects.get_or_create(client=client)

        if len(errors) > 0:
            return Response(
                    {
                        "errors": errors
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )
        if ine is not None:
            documents.ine = ine
        if bank_account is not None:
            documents.bank_account = bank_account
        if proof_address is not None:
            documents.proof_address = proof_address
        if additional_file is not None:
            documents.additional_file = additional_file
        
        documents.save()

        return Response(
            {
                "success": True
            }
        )


class LoanRequestView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            client = Client.objects.get(user=request.user)
        except Client.DoesNotExist:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        today = datetime.date.today()
        # Loan creation
        loan_info = request.data.get('loan', None)
        # Create tentative request no.
        max_loan = Loan.objects.filter().order_by('-pk').first()
        
        if max_loan is None:
            request_no = today.strftime('%Y') + today.strftime('%m') + '001'
            contract_no = 'I2019110001'
        else:
            if max_loan.contract_no is None:
                contract_no = 'I2019110001'
            else:
                if len(max_loan.contract_no) != 11:
                    contract_no = 'I2019110001'
                else:
                    if max_loan.contract_no[5:7] != today.strftime('%m'):
                        contract_no = 'I' + today.strftime('%Y') + today.strftime('%m') + '0001'
                    else:
                        last = int(max_loan.contract_no[7:]) + 1
                        contract_no = 'I' + today.strftime('%Y') + today.strftime('%m') + str(last).zfill(4)

            max_no = int(max_loan.request_no[7:]) + 1
            request_no = today.strftime('%Y') + today.strftime('%m') + '00' + str(max_no)


        loan_info['client'] = client.pk
        loan_info['amount_requested'] = loan_info['amount']

        options = client.get_options(loan_info['amount'], loan_info['option'])

        if len(options) != 1:
            return Response(
                {'errors': 'Opción inválida'},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )

        option = options[0]
        loan_info['payment_date'] = option['date']
        loan_info['term'] = option['term']
        loan_info['aperture'] = option['aperture']
        loan_info['amount_total'] = option['total']
        loan_info['request_no'] = request_no

        # Loan Requirements START
        data_reqs = {}
        reqs_client = RequirementsClient.objects.filter(client = client).values_list('requirement_id').exclude(status = 'IN')
        reqs_family = RequirementsFamily.objects.filter(family_id = client.family_id).values_list('requirement_id').exclude(status = 'IN')
        if reqs_client:
            ## 1 By Client Requirements
            reqs = Requirements.objects.filter(id__in = reqs_client).exclude(status = 'IN').order_by('html_tag')
        elif reqs_family:
            ## 2 By Family Requirements
            reqs = Requirements.objects.filter(id__in = reqs_family).exclude(status = 'IN').order_by('html_tag')
        else:
            ## 3 General Requirements
            reqs = Requirements.objects.exclude(status = 'IN').order_by('html_tag')

        for req in reqs:
            this_requirement = req.html_tag
            data_reqs.update({ this_requirement: True })

        loan_info['requirements'] = data_reqs
        # Loan Requirements END

        # Serialize Loan Data
        loan_serializer = LoanSerializer(data=loan_info)

        if not loan_serializer.is_valid():
            # print(loan_serializer.errors)
            return Response(
                {'errors': 'Informacion del prestamo invalido', 'loan_info': loan_info, "a": loan_serializer.errors},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )
            #print(loan_serializer.errors)

        # Client update
        client_info = request.data.get('client', None)
        if client_info['family_income'] == "":
            client_info['family_income'] = 0

        if client_info['interior_no'] is None:
            client_info['interior_no'] = ""

        # Serialize Client Data
        client_serializer = ClientSerializer(data=client_info)

        if not client_serializer.is_valid():
            # print(client_serializer.errors)
            return Response(
                {'errors': 'Informacion del cliente invalido'},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )


        # Loan Requirements Validation START
        # uses data_reqs json variable
        documents = ClientDocuments.objects.filter(client=client).first()
        if documents is None:
            return Response(
                {'errors': 'No se encontró ningun documento'},
                status=status.HTTP_404_NOT_FOUND
            )
        if 'ine' in data_reqs and (documents.ine is None or documents.ine == ''):
            return Response(
                {'errors': 'No se encontró identificacion oficial'},
                status=status.HTTP_404_NOT_FOUND,
            )

        if 'bank_account' in data_reqs and (documents.bank_account is None or documents.bank_account == ''):
            return Response(
                {'errors': 'No se encontró copia de cuenta de banco'},
                status=status.HTTP_404_NOT_FOUND,
            )

        if 'proof_address' in data_reqs and (documents.proof_address is None or documents.proof_address == ''):
            return Response(
                {'errors': 'No se encontró comprobante de domicilio'},
                status=status.HTTP_404_NOT_FOUND
            )
        # Loan Requirements Validation END

        # Save loan/client after validation
        loan = loan_serializer.create() 
        client_updated = client_serializer.update(client, client_info)
        client_updated.save()

        # Rendered pdf
        all_data = request.data.get('extra_data', None)
        contract_data = hardcodedData()
        contract_data = addData(contract_data, client_updated, all_data)
        
        loan.contract_no = contract_no
        loan.save()
        
        
        #Sobreesribir el numero de contrato y numero de solicitud para que se relaicone con el id de la solicitud
        try:
            loan.request_no = today.strftime('%Y') + today.strftime('%m') + '00' + str(loan.id)
            loan.contract_no = 'I' + today.strftime('%Y') + today.strftime('%m') + str(loan.id).zfill(7)
            contract_data['contract']['contract_no']=  'I' + today.strftime('%Y') + today.strftime('%m') + str(loan.id).zfill(7) 
            loan.save()
        except:
            pass

        #PDF
        html_string = render_to_string('contract.html', contract_data)
        html = HTML(string=html_string)
        result = html.write_pdf(stylesheets=['templates/contract.css'])
   
        loan.contract.save('out.pdf', File(BytesIO(result)))

        try:
            self.admin_loan_notification(client_updated.rfc,loan.request_no,loan.amount_requested,client_updated.family.tradename)
        except:
            pass

        return Response({'success': True})

    def utc_to_time(self, naive, timezone="America/Mexico_City"):
        return naive.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(timezone))


    def diff_timezone(self, timezone="America/Mexico_City"):
        utcnow = pytz.timezone('utc').localize(datetime.datetime.utcnow())
        here = utcnow.astimezone(pytz.timezone('America/Mexico_City')).replace(tzinfo=None)
        there = utcnow.astimezone(pytz.timezone('UTC')).replace(tzinfo=None)

        offset = relativedelta(there, here)

        return offset.hours


    def get_options(self, option, family):
        diff = self.diff_timezone()
        hour = datetime.datetime.now()
        today = datetime.date.today()

        limit = 18.5

        hour_limit = datetime.datetime(
            year=today.year,
            month=today.month,
            day=today.day,
        )

        hour_limit = hour_limit + datetime.timedelta(hours = limit) + datetime.timedelta (hours = diff)
        days = 0

        today_plus = today

        if hour > hour_limit:
            today_plus = today_plus + datetime.timedelta (days = 1)
            days += 1

        while True:
            original_date = today_plus
            today_plus, days = self.check_weekend(today_plus, days)
            today_plus, days = self.check_holidays(today_plus, days)

            if original_date == today_plus:
                break


        today_plus = today_plus + datetime.timedelta (days = (6-days))

        options = Paydays.objects.filter(day__gte=today_plus - datetime.timedelta (days = days), family=family).order_by('day')[:2]

        if len(options) < 2:
            return Response(
                    {
                        "error": "No hay fechas disponibles"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        next_payday = options[0].day
        next_next_payday = options[1].day

        # Seleccionar el algoritmo indicado
        # if family.period == Period.decennial.id:
        #     next_payday, next_next_payday = self.decennial(today, family)
        # elif family.period == Period.biweekly.id:
        #     next_payday, next_next_payday = self.biweekly(today, family)
        # elif family.period == Period.fortnightly.id:
        #     next_payday, next_next_payday = self.fortnightly(today)
        # elif family.period == Period.monthly.id:
        #     next_payday, next_next_payday = self.monthly(today, family)


        # Verifica si es fin de semana o dia festivo
        # while True:
        #     original_date = next_payday
        #     next_payday = self.check_weekend(next_payday)
        #     next_payday = self.check_holidays(next_payday)

        #     if original_date == next_payday:
        #         break


        # while True:
        #     original_date = next_next_payday
        #     next_next_payday = self.check_holidays(next_next_payday)
        #     next_next_payday = self.check_weekend(next_next_payday)

        #     if original_date == next_next_payday:
        #         break

        # Termina Verifica si es fin de semana o dia festivo


        days1 = (next_payday - today).days - days
        days2 = (next_next_payday - today).days - days


        if option == 1:
            return next_payday, days1
        else:
            return next_next_payday, days2


    def check_weekend(self, date, days):
        if date.weekday() == 5:
            date = date + datetime.timedelta (days = 2)
            days +=2
        elif date.weekday() == 6:
            date = date + datetime.timedelta (days = 1)
            days +=1

        return date, days

    def check_holidays(self, date, days):
        holidays = Holidays.objects.filter(day=date)
        if holidays.count() > 0:
            date = date + datetime.timedelta (days = 1)
            days +=1
        return date, days


    # Decenal
    def decennial(self, today, family):
        days_month_today = ((datetime.date(today.year, today.month, 1) + relativedelta(months=+1)) - datetime.date(today.year, today.month, 1)).days
        days_next_month_today = ((datetime.date(today.year, today.month, 1) + relativedelta(months=+2)) - (datetime.date(today.year, today.month, 1)) + relativedelta(months=+2)).days
        days_month_family = ((datetime.date(family.payday.year, family.payday.month, 1) + relativedelta(months=+1)) - datetime.date(family.payday.year, family.payday.month, 1)).days

        days = []

        # Determina que dias del mes podrian pagar en caso ideal 30 dias
        if family.payday.day <= 13:
            days.append(family.payday.day)
            days.append(family.payday.day+10)
            days.append(family.payday.day+20)
        elif family.payday.day <= 20:
            days.append(family.payday.day-10)
            days.append(family.payday.day)
            days.append(family.payday.day+10)
        else:
            if family.payday.day != days_month_family:
                days.append(family.payday.day-20)
                days.append(family.payday.day-10)
                days.append(family.payday.day)
            else:
                days.append(10)
                days.append(20)
                days.append(30)


        if today.day < days[0]:
            next_payday = datetime.date(today.year, today.month, days[0])
            next_next_payday = next_payday + relativedelta(days=+10)
        elif today.day < days[1]:
            next_payday = datetime.date(today.year, today.month, days[1])

            if next_payday.day == 20:
                next_next_payday = datetime.date(next_payday.year, today.month, days_month_today)
            else:
                if days_month_today != 30:
                    next_next_payday = next_payday + relativedelta(days=+10)
                else:
                    next_next_payday = next_payday + relativedelta(days=+(10+(days_month_today-30)))
        elif today.day < days[2]:
            if days[2] == 30:
                next_payday = datetime.date(today.year, today.month, days_month_today)
            else:
                next_payday = datetime.date(today.year, today.month, days[2])

            if days_month_today != 30:
                next_next_payday = next_payday + relativedelta(days=+(10+(days_month_today-30)))
            else:
                next_next_payday = next_payday + relativedelta(days=+10)


        if (next_payday - today).days <= 5:
            next_payday = next_next_payday

            if next_payday.day == 20:
                next_next_payday = datetime.date(next_payday.year, today.month, days_month_today)
            else:
                if next_payday.day < 20:
                    next_next_payday = next_payday + relativedelta(days=+(10))
                else:
                    next_next_payday = next_payday + relativedelta(days=+(10+(days_month_today-30)))


        return next_payday, next_next_payday


    # Catorcenal
    def biweekly(self, today, family):
        days_diff = (today - family.payday).days
        biweekly = int(days_diff/14) + 1

        # Determina los 2 siguientes dias de pago sin validar cuantos dias faltan para el mas cercano
        next_payday = family.payday + relativedelta(weeks=+(biweekly*2))
        next_next_payday = next_payday + relativedelta(weeks=+2)

        # Se recorre al siguiente periodo de pago si faltan menos de 6 dias
        if (next_payday - today).days <= 5:
            next_payday = next_next_payday
            next_next_payday = next_payday + relativedelta(weeks=+2)

        return next_payday, next_next_payday


    # Quincenal
    def fortnightly(self, today):
        if today.day > 15:
            year = today.year if today.month < 12 else today.year + 1
            month = today.month + 1 if today.month < 12 else 1
            next_payday = datetime.date(year, month, 1) - datetime.timedelta (days = 1)
        else:
            next_payday = datetime.date (today.year, today.month, 15)

        if (next_payday - today).days <=5:
            if next_payday.day > 15:
                year = next_payday.year if next_payday.month < 12 else next_payday.year + 1
                month = next_payday.month + 1 if next_payday.month < 12 else 1
                next_payday = datetime.date (year, month, 1) + datetime.timedelta (days = 14)
                year = next_payday.year if next_payday.month < 12 else next_payday.year + 1
                month = next_payday.month + 1 if next_payday.month < 12 else 1
                next_next_payday = datetime.date (year, month, 1) - datetime.timedelta (days = 1)
            else:
                year = next_payday.year if next_payday.month < 12 else next_payday.year + 1
                month = next_payday.month + 1 if next_payday.month < 12 else 1
                next_payday = datetime.date (year, month, 1) - datetime.timedelta (days = 1)
                next_next_payday = datetime.date (year, month, 1) + datetime.timedelta (days = 14)
        else:
            if next_payday.day > 15:
                next_next_payday = datetime.date (next_payday.year, next_payday.month + 1, 1) + datetime.timedelta (days = 14)
            else:
                year = next_payday.year if next_payday.month < 12 else next_payday.year + 1
                month = next_payday.month + 1 if next_payday.month < 12 else 1
                next_next_payday = datetime.date (year, month, 1) - datetime.timedelta (days = 1)

        return next_payday, next_next_payday


    # Mensual
    def monthly(self, today, family):
        # Determina los 2 siguientes dias de pago sin validar cuantos dias faltan para el mas cercano
        if family.payday.day <= today.day:
            next_payday = datetime.date(today.year, today.month, family.payday.day) + relativedelta(months=+1)
            next_next_payday = next_payday + relativedelta(months=+1)
        else:
            next_payday = datetime.date(today.year, today.month, family.payday.day)
            next_next_payday = next_payday + relativedelta(months=+1)

        # Se recorre al siguiente periodo de pago si faltan menos de 6 dias
        if (next_payday - today).days <= 5:
            next_payday = next_next_payday
            next_next_payday = next_payday + relativedelta(months=+1)


        return next_payday, next_next_payday

    #Send email to admin about the loan
    def admin_loan_notification(self,client,loan_number,loan_amount,family):
        subject = 'SOLICITUD POR REVISAR -  ' + str(loan_number)
        html_message = render_to_string(
            'loan_admin_notification.html',
            {
                'rfc': str(client),
                'loan': str(loan_number),
                'amount_requested': str(loan_amount),
                'family': str(family),
                
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = config('LOAN_EMAIL')

        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)


class MyLoansView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            client = Client.objects.get(user=request.user)
        except ClientToken.DoesNotExist:
            return Response(
                    {
                        "error": "Token inválido, usuario no es un cliente"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        # Desde pagos puendientes
        loans = PendingPayment.objects.filter(loan__client=client).order_by('pk')

        data = []
        for loan in loans:
            status_data = 'Dummy'

            if loan.paid_out:
                if loan.loan.payment_date + datetime.timedelta(days = 1) < loan.payday:
                    status_data = "Pago atrasado"
                else:
                    status_data = "Pago puntual"
            else:
                if loan.loan.payment_date + datetime.timedelta(days = 1) > datetime.date.today():
                    status_data = "Vigente"
                else:
                    status_data = "Moroso"

            if status_data == "Pago atrasado":
                delayed_days = loan.payday - loan.loan.payment_date
                data.append(
                    {
                        "id":loan.loan.id,
                        "request_no": loan.loan.request_no,
                        "amount": loan.loan.amount_total,
                        "request_date": loan.loan.created_at.strftime('%d/%m/%Y'),
                        "payment_date": loan.loan.payment_date.strftime('%d/%m/%Y'),
                        "status": status_data,
                        "delayed_days": delayed_days.days
                    }
                )
            else:
                data.append(
                    {
                        "id":loan.loan.id,
                        "request_no": loan.loan.request_no,
                        "amount": loan.loan.amount_total,
                        "request_date": loan.loan.created_at.strftime('%d/%m/%Y'),
                        "payment_date": loan.loan.payment_date.strftime('%d/%m/%Y'),
                        "status": status_data,
                    }
                )

        # Solo pagos pendientes
        loans_pending = Loan.objects.filter(
            state__in=[1,2],
            client=client
            ).exclude(
                pk__in=list(
                    loans.values_list('loan', flat=True
                    )
                )
            ).order_by('pk')

        for loan in loans_pending:
            status_data = 'En tramite'
            data.append(
                {   
                    "id":loan.id,
                    "request_no": loan.request_no,
                    "amount": loan.amount_total,
                    "request_date": loan.created_at.strftime('%d/%m/%Y'),
                    "payment_date": loan.payment_date.strftime('%d/%m/%Y'),
                    "status": status_data,
                }
            )

        loans_rejected = Loan.objects.filter(
            state__in=[3],
            client=client
            ).exclude(
                pk__in=list(
                    loans.values_list('loan', flat=True
                    )
                )
            ).order_by('pk')


        for loan in loans_rejected:
            status_data = 'Rechazado'
            data.append(
                {   
                    "id":loan.id,
                    "request_no": loan.request_no,
                    "amount": loan.amount_total,
                    "request_date": loan.created_at.strftime('%d/%m/%Y'),
                    "payment_date": loan.payment_date.strftime('%d/%m/%Y'),
                    "status": status_data,
                    "reason_for_rejection": loan.reason_for_rejection
                }
            )
        
        try:
            data = sorted(data, key=lambda k: k['id'], reverse=True)
        except:
            pass
        
        return Response(
            {
                "data": data
            }
        )


class DeleteMeView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request):
        try:
            client = Client.objects.get(user=request.user)
        except ClientToken.DoesNotExist:
            return Response(
                    {
                        "error": "Token inválido, usuario no es un cliente"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )



        try:
            # loans = Loan.objects.filter(state=1, client=client).order_by('pk')

            # for loan in loans:
            #     loan.delete()

            # client.is_active = False
            # client.save()

            request.user.delete()
            client.delete()

            return Response(
                {
                    "sucess": True
                }
            )
        except:
            return Response(
                    {
                        "sucess": False,
                        "error": "No es posible cancelar su cuenta en estos momentos, por favor inténtelo más tarde"
                    }
                    , status=status.HTTP_409_CONFLICT
                )


class ContractDataView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            client = Client.objects.get(user=request.user)
        except ClientToken.DoesNotExist:
            return Response(
                    {
                        "error": "Token inválido, usuario no es un cliente"
                    }
                    , status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )

        today = datetime.date.today()

        # Create tentative request no.
        max_loan = Loan.objects.filter().order_by('-pk').first()

        if max_loan is None:
            today = datetime.date.today()
            request_no = today.strftime('%Y') + today.strftime('%m') + '001'
            contract_no = 'I2019110001'
        else:
            if max_loan.contract_no is None:
                contract_no = 'I2019110001'
            else:
                if len(max_loan.contract_no) != 11:
                    contract_no = 'I2019110001'
                else:
                    if max_loan.contract_no[5:7] != today.strftime('%m'):
                        contract_no = 'I' + today.strftime('%Y') + today.strftime('%m') + '0001'
                    else:
                        last = int(max_loan.contract_no[7:]) + 1
                        last_str = str(last)
                        contract_no = 'I' + today.strftime('%Y') + today.strftime('%m') + str(last).zfill(4)

            today = datetime.date.today()
            max_no = int(max_loan.request_no[7:]) + 1
            request_no = today.strftime('%Y') + today.strftime('%m') + '00' + str(max_no)


        return Response(
                {
                    "request_no": request_no,
                    "contract_no": contract_no,
                    "sign_date": today,
                    "name": "{} {} {}".format(client.names, client.father_last_name, client.mother_last_name), # Posiblemente no lo tenga que mandar
                    "rfc": client.rfc, # Posiblemente no lo tenga que mandar
                    # "curp": client.curp,
                    "birthdate": client.birthdate, # Posiblemente no lo tenga que mandar
                    # "nationality": client.nationality,
                    # "gender": client.gender,
                    "email": client.email, # Posiblemente no lo tenga que mandar
                    "address": client.address, # Posiblemente no lo tenga que mandar
                    "phone": client.phone, # Posiblemente no lo tenga que mandar
                    "cellphone": client.cellphone, # Posiblemente no lo tenga que mandar
                    "family": client.family.business_name,
                }
            )


def hardcodedData():
    data = {
        'contract': {
            'product': 'CRÉDITO IXHUA',
            'creditType': 'CRÉDITO SIMPLE',
            'anualMoratory': '540%',
            'ixhua': {
                'companyName': 'IXHUA',
                'website': 'www.ixhua.mx',
                'socialReason': 'IXHUA TECH, S.A.P.I. DE C.V.',
                'contact': 'contacto@ixhua.mx',
                'address': 'Real de  Acueducto  240  int.  8,  Colonia Puerta  de  Hierro,  Zapopan,  Jalisco. C.P. 45116',
                'phone': '33 19136707',
                'rfc': 'MPR1608268Z5',
                'nationality': 'MEXICANA',
                'legalRepresentative': 'JORGE ALBERTO DORANTES PADILLA',
                'bank': 'BBVA',
                'bankSocialReason': 'BBVA BANCOMER S.A.',
                'bankAccount': '0113675203',
                'clabe': '012320001136752038',
                'accountHolder': 'IXHUA TECH, S.A.P.I. DE C.V.',
            },
            'profeco': {
                'companyName': 'PROFECO',
                'address': 'Av. José Vasconcelos 208,  Col. Condesa, Del. Cuauhtémoc, Ciudad de México. C.P. 06140',
                'phone': '(55) 5625 6700',
                'phoneCitizensAttention': '800 468 8722',
                'website': 'https://www.gob.mx/profeco'
                }
        }
    }
    return data


def addData(data, client, all_data):
    today = datetime.date.today()
    # Create tentative request no.
    max_loan = Loan.objects.filter().order_by('-pk').first()

    if max_loan is None:
        today = datetime.date.today()
        request_no = today.strftime('%Y') + today.strftime('%m') + '001'
        contract_no = 'I2019110001'
    else:
        if max_loan.contract_no is None:
            contract_no = 'I2019110001'
        else:
            if len(max_loan.contract_no) != 11:
                contract_no = 'I2019110001'
            else:
                if max_loan.contract_no[5:7] != today.strftime('%m'):
                    contract_no = 'I' + today.strftime('%Y') + today.strftime('%m') + '0001'
                else:
                    last = int(max_loan.contract_no[7:]) + 1
                    last_str = str(last)
                    contract_no = 'I' + today.strftime('%Y') + today.strftime('%m') + str(last).zfill(4)


        today = datetime.datetime.now()
        #AJUSTAR HORA EN EL CAMBIO DE HORARIO
        today = today - timedelta(hours=6)
        max_no = int(max_loan.request_no[7:]) + 1
        request_no = today.strftime('%Y') + today.strftime('%m') + '00' + str(max_no)

    # List months
    months = {
        '01': 'Enero',
        '02': 'Febrero',
        '03': 'Marzo',
        '04': 'Abril',
        '05': 'Mayo',
        '06': 'Junio',
        '07': 'Julio',
        '08': 'Agosto',
        '09': 'Septiembre',
        '10': 'Octubre',
        '11': 'Noviembre',
        '12': 'Diciembre',
    }

    # Contract Data
    data['contract']['contract_no'] = contract_no
    data['contract']['request_no'] = request_no
    data['contract']['sign_date'] = today.strftime('%d/%m/%Y %H:%M:%S')
    data['contract']['sign_date_day'] = today.strftime('%d')
    data['contract']['sign_date_month'] = months[today.strftime('%m')]
    data['contract']['sign_date_year'] = today.strftime('%Y')

    # Loan Data
    data['contract']['amount'] = "{0:.2f}".format(float(all_data['amount']))
    data['contract']['amountLetters'] = all_data['amountLetters']
    data['contract']['annualRegularInterest'] = all_data['annualRegularInterest']
    data['contract']['term'] = all_data['term']
    data['contract']['interest'] = all_data['interest']
    data['contract']['interestToPayInLetters'] = all_data['interestToPayInLetters']
    data['contract']['payments'] = 1
    data['contract']['paymentDate'] = all_data['paymentDate']
    data['contract']['interestToPay'] = "{0:.2f}".format(float(all_data['interestToPay']))
    data['contract']['aperture'] = "{0:.2f}".format(float(all_data['aperture']))
    data['contract']['totalAmount'] = "{0:.2f}".format(float(all_data['totalAmount']))
    data['contract']['cat'] = all_data['cat']
    data['contract']['tfa'] = all_data['tfa']

    # Client data
    data['contract']['name'] = all_data['name'].upper()
    data['contract']['rfc'] = all_data['rfc'].upper()
    # data['contract']['birthdate'] = all_data['birthdate']
    data['contract']['birthdate'] = datetime.datetime.strptime(all_data['birthdate'], '%Y-%m-%d').strftime('%d/%m/%Y')
    data['contract']['email'] = all_data['email'].upper()

    data['contract']['address'] = all_data['address'].upper()
    data['contract']['interior_no'] = all_data['interior_no'].upper()
    data['contract']['outdoor_no'] = all_data['outdoor_no'].upper()
    data['contract']['suburb'] = all_data['suburb'].upper()
    data['contract']['postal_code'] = all_data['postal_code'].upper()
    data['contract']['city'] = all_data['city'].upper()
    data['contract']['state'] = all_data['state'].upper()

    data['contract']['phone'] = all_data['phone']
    data['contract']['cellphone'] = all_data['cellphone']
    data['contract']['company'] = all_data['family'].upper()
    data['contract']['curp'] = all_data['curp'].upper()
    data['contract']['nationality'] = all_data['nationality'].upper()
    data['contract']['gender'] = all_data['gender'].upper()
    data['contract']['clabe'] = all_data['clabe'].upper()
    data['contract']['bankName'] = all_data['bankName'].upper()
    data['contract']['accountType'] = all_data['accountType'].upper()
    data['contract']['loanDestination'] = all_data['loanDestination'].upper()


    return data
