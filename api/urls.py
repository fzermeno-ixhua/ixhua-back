#from .views import UserViewSet, YourView, ShareView
from .views import *
from django.urls import path
from rest_framework.routers import DefaultRouter


app_name = "api"

router = DefaultRouter()
# Main urls
router.register(r"client", ClientViewSet, "client")



api_patterns = [
    path('rfc_validation/', RFCValidationView.as_view()),
    path('pin_validation/', PINValidationView.as_view()),
    path('pin_change_password/', PINChangePasswordView.as_view()),
    path('whoami/', WhoAmIView.as_view()),
    path('login/', LoginView.as_view()),
    path('reset_password_mail/', RecoveryPasswordMailView.as_view()),
    path('reset_password/', RecoveryPasswordView.as_view()),
    path('basic_loan_info/', BasicLoanInfoView.as_view()),
    path('loan_calculator/', LoanCalculatorView.as_view()),
    path('client_info/', ClientInfoView.as_view()),
    path('families_info/', FamiliesInfoView.as_view()),
    path('client_upload_file/', ClientFileUploadView.as_view()),
    path('loan_request/', LoanRequestView.as_view()),
    path('my_loans/', MyLoansView.as_view()),
    path('delete_me/', DeleteMeView.as_view()),
    path('contract_data/', ContractDataView.as_view()),
]

api_patterns += router.urls



