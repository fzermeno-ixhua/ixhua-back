from import_export import resources, fields
from import_export.widgets import ForeignKeyWidget
from .models import Client
from families.models import Family
from products.models import Product

class ClientResource(resources.ModelResource):
    rfc = fields.Field(attribute="rfc", column_name="RFC",)
    names = fields.Field(attribute="names", column_name="NOMBRE (S)",)
    father_last_name = fields.Field(attribute="father_last_name", column_name="APELLIDO PATERNO",)
    mother_last_name = fields.Field(attribute="mother_last_name", column_name="APELLIDO MATERNO",)
    birthdate = fields.Field(attribute="birthdate", column_name="FECHA NACIMIENTO",)
    actual_position = fields.Field(attribute="actual_position", column_name="PUESTO ACTUAL",)
    admission_date = fields.Field(attribute="admission_date", column_name="FECHA DE INGRESO",)
    cellphone = fields.Field(attribute="cellphone", column_name="CELULAR",)
    phone = fields.Field(attribute="phone", column_name="TELEFONO FIJO",)
    email = fields.Field(attribute="email", column_name="EMAIL",)
    bank_name = fields.Field(attribute="bank_name", column_name="BANCO AL CUAL SE PAGA SU NOMINA",)
    bank_clabe = fields.Field(attribute="bank_clabe", column_name="CUENTA CLABE A LA CUAL SE PAGA SU NOMINA",)
    address = fields.Field(attribute="address", column_name="DOMICILIO",)
    salary = fields.Field(attribute="salary", column_name="MONTO NETO SUELDO MENSUAL",)
    payment_frequency = fields.Field(attribute="payment_frequency", column_name="FRECUENCIA DE PAGO (QUINCENAL, SEMANAL) DIAS DE PAGO",)
    credit_amount = fields.Field(attribute="credit_amount", column_name="MONTO DE CREDITO",)
    subfamily = fields.Field(attribute="subfamily", column_name="SUBFAMILIA",)
    family = fields.Field(
                          attribute="family",
                          column_name="FAMILIA",
                          widget=ForeignKeyWidget(Family, "tradename"), )
    product = fields.Field(attribute="product", column_name="PRODUCTO", widget=ForeignKeyWidget(Product, "name"))


    class Meta:
        model = Client
        import_id_fields = ('rfc',)
        fields = ('rfc',
                  'names',
                  'father_last_name',
                  'mother_last_name',
                  'birthdate',
                  'actual_position',
                  'admission_date',
                  'cellphone',
                  'phone',
                  'email',
                  'bank_name',
                  'bank_clabe',
                  'address',
                  'salary',
                  'payment_frequency',
                  'credit_amount',
                  'family',
                  'subfamily',
                  'product',
                  )


    def get_instance(self, instance_loader, row):
        try:
            client = Client.objects.get(rfc=row['RFC'])
        except Client.DoesNotExist:
            return None
        return client


    def after_save_instance(self, instance, using_transactions, dry_run):
        """
        Override to add additional logic. Does nothing by default.
        """
        if not dry_run:
            if instance.user is not None:
                instance.user.email = instance.email
                instance.user.first_name = instance.names
                instance.user.father_last_name = instance.father_last_name
                instance.user.mother_last_name = instance.mother_last_name
                instance.user.phone = instance.phone
                instance.user.cellphone = instance.cellphone
                instance.user.save()
            else:
                instance.invited = True
            instance.save()


class ExportClientResource(resources.ModelResource):
    """ 
    PersonAdmin class export resource. 
    Overrides default export resoruce

    [related_classes] PersonAdmin

    Added-Dev on: [2020-12-23]
    Added-Prod on: 

    """

    id_client = fields.Field(attribute="id", column_name="ID",)
    rfc = fields.Field(attribute="rfc", column_name="RFC",)
    names = fields.Field(attribute="names", column_name="NOMBRE (S)",)
    father_last_name = fields.Field(attribute="father_last_name", column_name="APELLIDO PATERNO",)
    mother_last_name = fields.Field(attribute="mother_last_name", column_name="APELLIDO MATERNO",)
    birthdate = fields.Field(attribute="birthdate", column_name="FECHA NACIMIENTO",)
    actual_position = fields.Field(attribute="actual_position", column_name="PUESTO ACTUAL",)
    admission_date = fields.Field(attribute="admission_date", column_name="FECHA DE INGRESO",)
    cellphone = fields.Field(attribute="cellphone", column_name="CELULAR",)
    phone = fields.Field(attribute="phone", column_name="TELEFONO FIJO",)
    email = fields.Field(attribute="email", column_name="EMAIL",)
    bank_name = fields.Field(attribute="bank_name", column_name="BANCO AL CUAL SE PAGA SU NOMINA",)
    bank_clabe = fields.Field(attribute="bank_clabe", column_name="CUENTA CLABE A LA CUAL SE PAGA SU NOMINA",)
    address = fields.Field(attribute="address", column_name="DOMICILIO",)
    zipcode = fields.Field(attribute="postal_code", column_name="CODIGO POSTAL",)
    city = fields.Field(attribute="suburb", column_name="CIUDAD",)
    salary = fields.Field(attribute="salary", column_name="MONTO NETO SUELDO MENSUAL",)
    payment_frequency = fields.Field(attribute="payment_frequency", column_name="FRECUENCIA DE PAGO (QUINCENAL, SEMANAL) DIAS DE PAGO",)
    credit_amount = fields.Field(attribute="credit_amount", column_name="MONTO DE CREDITO",)
    subfamily = fields.Field(attribute="subfamily", column_name="SUBFAMILIA",)
    family = fields.Field(
                          attribute="family",
                          column_name="FAMILIA",
                          widget=ForeignKeyWidget(Family, "tradename"), )
    product = fields.Field(attribute="product", column_name="PRODUCTO", widget=ForeignKeyWidget(Product, "name"))


    class Meta:
        model = Client
        fields = ('id_client',
                  'rfc',
                  'names',
                  'father_last_name',
                  'mother_last_name',
                  'birthdate',
                  'actual_position',
                  'admission_date',
                  'cellphone',
                  'phone',
                  'email',
                  'bank_name',
                  'bank_clabe',
                  'address',
                  'zipcode',
                  'city',
                  'salary',
                  'payment_frequency',
                  'credit_amount',
                  'family',
                  'subfamily',
                  'product',
                  )