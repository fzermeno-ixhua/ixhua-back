from datetime import datetime, timedelta
from decimal import Decimal
from math import ceil
import uuid
from django.db import models
from django.core.validators import MinLengthValidator, MaxValueValidator, RegexValidator
from decouple import config
from storages.backends.s3boto3 import S3Boto3Storage
import pytz
import base64

from loan.models import Loan, Seeds
from families.models import Paydays, Holidays
from products.models import Product

class DownloadableS3Boto3Storage(S3Boto3Storage):

    def _save_content(self, obj, content, parameters):
        """
        The method is called by the storage for every file being uploaded to S3.
        Below we take care of setting proper ContentDisposition header for
        the file.
        """
        filename = obj.key.split('/')[-1]
        parameters.update({'ContentDisposition': f'attachment; filename="{filename}"'})
        return super()._save_content(obj, content, parameters)

# Create your models here.
class Client(models.Model):

    NO_STATUS = 0
    CAPTURED_RFC = 1
    CAPTURED_INFORMATION = 2
    CAPTURED_DOCUMENTS = 3
    PIN_SENT = 4
    USER_ACCOUNT = 5
    CANCEL_ACCOUNT = 6

    STATUS=((NO_STATUS,'Sin Estatus'),
        (CAPTURED_RFC,'Sin Datos (Solo RFC)'),
        (CAPTURED_INFORMATION,'Con datos y RFC'),
        (CAPTURED_DOCUMENTS,'Con datos, RFC, documentos y selfie'),
        (PIN_SENT, 'Con datos, RFC y PIN'),
        (USER_ACCOUNT,'Cuenta activada con usuario'),
        (CANCEL_ACCOUNT,'Cuenta cancelada')
    )

    KIBAN_BANK_CODE=(
        ('40133','Actinver'),
        ('40062','Afirme'),
        ('40103','American Express'),
        ('40127','Banca Azteca SA'),
        ('40042','Banca MIFEL'),
        ('40030','Banco del Bajío'),
        ('40072','Banco Mercantil del Norte SA (Banorte)'),
        ('40113','Banco Ve Por Más'),
        ('40137','Bancoppel'),
        ('37019','Banjercito'),
        ('37009','Banobras'),
        ('40058','Banregio'),
        ('37166','Bansefi'),
        ('40060','BANSI'),
        ('40145','BASE'),
        ('40012','BBVA Bancomer'),
        ('40143','CIBANCO'),
        ('40002','Citibanamex'),
        ('40021','HSBC'),
        ('40036','Inbursa'),
        ('40136','Intercam Grupo Financiero'),
        ('90600','MONEX'),
        ('90613','Multiva'),
        ('40156','Sabadell SA'),
        ('40014','Santander Serfin'),
        ('40044','Scotiabank Inverlat'),

    )

    # Datos Personales
    rfc = models.CharField(verbose_name="RFC", max_length=13, null=False, unique=True, validators=[MinLengthValidator(12)])
    curp = models.CharField(verbose_name="CURP", max_length=18, blank=True, null=True, validators=[MinLengthValidator(18)])
    names = models.CharField(verbose_name="Nombres", max_length=200, null=False)
    father_last_name = models.CharField(verbose_name="Apellido paterno", max_length=200, null=False)
    mother_last_name = models.CharField(verbose_name="Apellido materno", max_length=200, null=False)
    gender = models.CharField(verbose_name="Género", max_length=10, null=True, blank=True)
    birthdate = models.DateField(verbose_name="Fecha Nacimiento", null=False)
    birth_place = models.CharField(verbose_name="Lugar de nacimiento", max_length=50, null=True, blank=True)
    nationality = models.CharField(verbose_name="Nacionalidad", max_length=20, null=True, blank=True)
    civil_status = models.CharField(verbose_name="Estado civil", max_length=30, null=True, blank=True)
    cellphone = models.CharField(verbose_name="Celular", null=True, unique=False ,max_length=10,validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    phone = models.CharField(verbose_name="Telefono fijo", null=False, max_length=10, validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    email = models.EmailField(unique=True, null=False)
    study_level = models.CharField(verbose_name="Grado de estudios", max_length=50, null=True, blank=True)
    

    # Datos Geograficos
    address = models.CharField(verbose_name="Domicilio", max_length=255, null=False)
    outdoor_no = models.CharField(verbose_name="No. ext.", max_length=50, null=True, blank=True)
    interior_no = models.CharField(verbose_name="No. int.", max_length=50, null=True, blank=True)
    postal_code = models.CharField(verbose_name="Codigo Postal", max_length=5, validators=[MinLengthValidator(5), RegexValidator('^[0-9]*$')], null=True, blank=True)
    suburb = models.CharField(verbose_name="Colonia", max_length=255, null=True, blank=True)
    city = models.CharField(verbose_name="Municipio", max_length=255, null=True, blank=True)
    state = models.CharField(verbose_name="Estado", max_length=255, null=True, blank=True)
    country = models.CharField(verbose_name="Pais", max_length=255, null=True, blank=True)
    house_type = models.CharField(verbose_name="Tipo de vivienda", max_length=20, null=True, blank=True)
    years_residence = models.DecimalField(verbose_name="Años de residencia", validators=[MaxValueValidator(99)],decimal_places=2,max_digits=7,default=0)

    # Datos laborales
    #labor_old = models.CharField(verbose_name="Antigüedad", max_length=2, validators=[RegexValidator('^[0-9]*$')])
    family_str = models.CharField(verbose_name="Familia ingresada por el cliente", max_length=255, null=True, blank=True)
    actual_position = models.CharField(verbose_name="Puesto Actual", max_length=50, null=True)
    labor_old = models.PositiveIntegerField(verbose_name="Antigüedad",  validators=[MaxValueValidator(99)], default=0)
    labor_old_month = models.PositiveIntegerField(verbose_name="Antigüedad (Meses)",  validators=[MaxValueValidator(11)], default=0)
    salary = models.DecimalField(verbose_name="Salario Mensual (MXN)", decimal_places=2, null=False, max_digits=8)
    payment_frequency = models.CharField(verbose_name="Frecuencia de Pago", max_length=50, null=True)
    family_income = models.DecimalField(verbose_name="Ingreso familiar acumulado (MXN)", decimal_places=2, max_digits=10, default=0)
    economic_dependent = models.IntegerField(verbose_name="Dependiente económicos", default=0)

    # Datos crediticios
    bank_name = models.CharField(verbose_name="Nombre del Banco", max_length=50, null=False)
    bank_clabe = models.CharField(verbose_name="CLABE", max_length=20, null=False)
    credit_bureau = models.CharField(verbose_name="¿Has presentado atrasos en Buró de Crédito?", max_length=20, null=True, blank=True)
    credit_lines = models.CharField(verbose_name="¿Cuentas con alguna línea de crédito?", max_length=255, null=True, blank=True)
    tell_us = models.TextField(verbose_name="¡Platícanos! ¿Para qué usarías tu préstamo?", null=True, blank=True)
    notes = models.TextField(verbose_name="Datos adicionales", max_length=500, null=True, blank=True)
    
    #Datos para validacion
    full_name_aux = models.CharField(verbose_name="Nombre Completo (Auxiliar)", max_length=200, null=True, blank=True)
    cellphone_aux = models.CharField(verbose_name="Celular (Auxiliar)", null=True, unique=False,blank=True ,max_length=10,validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    salary_aux = models.DecimalField(verbose_name="Salario Mensual Libre (Auxiliar)", decimal_places=2, null=True,blank=True, max_digits=8)
    email_aux = models.EmailField(verbose_name="Correo Electronico (Auxiliar)",unique=False, null=True,blank=True)
    
    #Datos de referencias
    reference = models.CharField(verbose_name="Referencia no familiar", max_length=200, null=True, blank=True)
    reference_phone = models.CharField(verbose_name="Teléfono referencia no familiar", null=True, unique=False,blank=True ,max_length=10,validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    family_reference = models.CharField(verbose_name="Referencia familiar", max_length=200, null=True, blank=True)
    family_reference_phone = models.CharField(verbose_name="Teléfono referencia familiar", null=True, unique=False,blank=True ,max_length=10,validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    
    ## Datos internos
    admission_date = models.DateField(verbose_name="Fecha de Ingreso", null=False)
    family = models.ForeignKey('families.Family', on_delete=models.CASCADE, verbose_name="Familia", null=False)
    subfamily = models.CharField(verbose_name="Subfamilia", max_length=50, null=True)
    pin = models.CharField(verbose_name="Pin", max_length=6, null=True, blank=True)
    user = models.ForeignKey('core.CustomUser', on_delete=models.SET_NULL, null=True, blank=True)
    product = models.ForeignKey('products.Product', verbose_name="Producto", on_delete=models.PROTECT, null=True, blank=True)
    credit_amount = models.DecimalField(verbose_name="Monto de Credito (MXN)", decimal_places=2, max_digits=7)
    credit_limit_max = models.DecimalField(verbose_name="Monto de Credito Maximo", decimal_places=2, max_digits=7, default=500)
    credit_limit_increase = models.DecimalField(verbose_name="Aumento en Monto (MXN)", decimal_places=2, max_digits=5, default=200)
    updated_at = models.DateTimeField(auto_now=True)


    is_active = models.BooleanField(
        default=True,
        verbose_name="Activo"
    )

    # Bandera para envio de invitacion
    invited = models.BooleanField(default=True, verbose_name="Invitado")

    #helpers
    pay_date = None

    #Bandera para identificar si el cliente fue creado en el admin o manualmente en el frontend
    created_on_website = models.BooleanField(default=False, verbose_name="Creado en el sitio web")

    #Estatus para clientes que fueron creados manualmente desde el frontend
    status = models.IntegerField(choices=STATUS, default=NO_STATUS)

    #codigos de promocion
    have_promotion_code = models.BooleanField(default=False,verbose_name="Tiene código de promoción") 
    promotion_code = models.CharField(verbose_name="Código de promoción", max_length=10, null=True, blank=True)
    
    class Meta:
        ordering = ['names']
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    def __unicode__(self):
        return u"{0}".format(self.rfc)

    @property
    def _product(self):
        "Return asigned product or default conditions"
        return self.product or Product.default()

    @property
    def loan_unit(self):
        "Return the type of unit of the loan max limit"
        return self._product.loan_unit

    @property
    def seeds(self):
        "Return the seeds of the client"
        seeds = Seeds.objects.filter(loan__client=self).order_by('-pk').first()
        adjust = ClientAdjustment.objects.filter(client=self).order_by('-pk').first()
        if seeds is None and adjust is None:
            return ClientAdjustment(green=5, gray=0, yellow=0)
        if seeds is not None and (adjust is None or seeds.created_at > adjust.created_at):
            return seeds
        return adjust

    @property
    def bank_code_kiban(self):
        "Return the bank code from kiban"
        #Aqui se guarda el estado de nacimiento
        code = "00000"
        print(self.bank_name)
        try:
        #busca la coincidencia del nombre del banco para encontrar el codigo
            for x in Client.KIBAN_BANK_CODE:
                if x[1] == self.bank_name:
                    #si existe una coincidencia guarda el estado
                    code = x[0]
                    break
        except:
            code = '00000'
            pass
        
        return code

    def dates_options_set(self, option_number=None):
        "Return the next n pay dates"
        index = option_number
        if option_number is not None:
            index -= 1
        product = self._product
        fromdate = Client.resolution_date() + timedelta(days=product.days_threshold)
        offset = max(index or 0, 0)
        if index is not None and not 0 <= index < product.loan_options:
            limit = 0
        elif index is not None:
            limit = 1
        else:
            limit = product.loan_options

        return Paydays.objects \
            .filter(family=self.family_id, day__gte=fromdate) \
            .order_by('day')[offset : offset + limit] \
            .all()

    @staticmethod
    def resolution_date():
        "return the loan resolution date"
        ref = datetime.now(pytz.timezone('America/Mexico_City'))
        while True:
            aux = Client.workday_or_next(ref)
            if aux.date() == ref.date():
                return ref.date()
            ref = aux

    @staticmethod
    def workday_or_next(date):
        "if the date is not a workday returns the next"
        aux = date
        minutes = aux.hour * 60 + aux.minute
        #If date is later than 18:30
        if minutes >= 18.5 * 60:
            aux = aux + timedelta(days=1, hours=-18.5)
        #If date is weekend
        if aux.weekday() in [5, 6]:
            return aux + timedelta(days=7 - aux.weekday())

        holiday = Holidays.objects.filter(day=aux.date()).first()
        #If date is holiday
        if holiday is not None:
            aux = aux + timedelta(days=1)

        return aux

    def get_options(self, amount, option_number=None):
        "Returns the set of loan options"
        if not self.loan_min_amount <= amount <= self.loan_amount_available:
            return []

        seeds = self.seeds
        if seeds.gray >= 5:
            return []

        dates = self.dates_options_set(option_number)
        if len(dates) == 0:
            return dates

        product = self._product
        date = Client.resolution_date()

        def format_option(paydate):
            days = (paydate.day - date).days
            res = product.calculate_fees(amount, days, seeds)
            res['date'] = paydate.day
            res['term'] = days
            return res

        return list(map(format_option, dates))

    @property
    def loans_approved_set(self):
        "Return approved loans"
        return Loan.objects.filter(client=self, state=Loan.APPROVED).prefetch_related('payments')

    @property
    def loans_active_set(self):
        "Return pending and approved loans"
        if self.client_loans:
            return [loan for loan in self.client_loans.all() if loan.state in [Loan.APPROVED, Loan.PENDING] ]
        return Loan.objects.filter(
            client=self, state__in=[Loan.APPROVED, Loan.PENDING]
        ).prefetch_related('payments')

    @property
    def amount_held(self):
        "Return the amount debt by the client"
        return sum(loan.amount_held for loan in self.loans_active_set)

    @property
    def days_worked(self):
        "Return the worked days number since last pay day"
        try:
            today = datetime.now(pytz.timezone('America/Mexico_City')).date()
            if self.pay_date is None:
                self.pay_date = Paydays.objects.filter(
                    family=self.family_id, day__lte=today
                ).order_by('-day')[:1].get()
            delta = today - self.pay_date.day
            return delta.days
        except Paydays.DoesNotExist:
            return 0

    @property
    def salary_daily(self):
        "Return the client's daily salary"
        return self.salary * 12 / 360

    @property
    def salary_worked(self):
        "Return the proportional amount worked by the client since last payday"
        return self.days_worked * self.salary_daily

    @property
    def loan_min_amount(self):
        "Return the min loan amount based in the product the client has asigned"
        return self._product.loan_min

    @property
    def loan_max_amount(self):
        "Return the max loan amount based in the product the client has asigned"
        product = self._product
        if product.loan_unit == product.AMOUNT:
            return min(self.credit_amount, product.loan_max)
        if product.loan_unit == product.PERCENT:
            salary_amount = Decimal(0.01) * product.loan_max * self.salary_worked
            return min(self.credit_amount, salary_amount)
        return 0

    @property
    def loan_amount_available(self):
        "Return the loan amount that the client can dipose"
        value = max(self.loan_max_amount - self.amount_held, 0)
        real_value = value - (value%50)
        return real_value

    @property
    def days_to_loan(self):
        "Return the number the days that the client must wait to have access to a loan"
        product = self._product
        if product.loan_unit != product.PERCENT:
            return 0
        if self.loan_min_amount <= self.loan_amount_available:
            return 0
        return ceil(
            self.loan_min_amount / (Decimal(0.01) * product.loan_max * self.salary_daily)
        ) - self.days_worked

    @property
    def censored_email(self):
        censored_email = self.email.split('@')
        new_censored_email = censored_email[0][0] + ('*'*(len(censored_email[0])-2)) + censored_email[0][len(censored_email[0]) - 1] + '@' + censored_email[1]
        return new_censored_email
    @property
    def censored_cellphone(self):
        censored_cellphone = list(self.cellphone)
        new_censored_cellphone = censored_cellphone[0] + censored_cellphone[1] + censored_cellphone[2] + ('*'*(len(self.cellphone)-5)) + censored_cellphone[8] + censored_cellphone[9]
        
        return new_censored_cellphone

    def __str__(self):
        return u"{0}".format(self.rfc)




class ClientToken(models.Model):
    token = models.CharField(verbose_name="Token", max_length=36, null=True, blank=True)
    expire = models.BigIntegerField(verbose_name="Expiracion", null=True, default=0)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, null=True)

    class Meta:
        ordering = ['expire']
        verbose_name = 'Token'
        verbose_name_plural = 'Tokens'

    def __unicode__(self):
        return u"{0}".format(self.token)

    def __str__(self):
        return u"{0}".format(self.token)

class ClientDocuments(models.Model):
    sthree = S3Boto3Storage()

    def file_name(self,filename):
        ext = filename.split('.')[-1]
        name = "%s/%s.%s" % (config('INSTANCE'), uuid.uuid4(), ext)

        return name

    #client = models.ForeignKey(Client, on_delete=models.CASCADE, null=False, unique=True)
    client = models.OneToOneField(Client, on_delete=models.CASCADE, null=False)
    ine = models.FileField(verbose_name="Copia ID vigente", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    bank_account = models.FileField(verbose_name="Copia o foto de la cuenta de banco", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    proof_address = models.FileField(verbose_name="Copia comprobante de domicilio", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    additional_file = models.FileField(verbose_name="Documento Adicional", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    paysheet_one =models.FileField(verbose_name="Último recibo de nómina", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    paysheet_two =models.FileField(verbose_name="Penúltimo recibo de nómina", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    paysheet_three =models.FileField(verbose_name="Antepenúltimo recibo de nómina", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    selfie =models.FileField(verbose_name="Selfie", blank=True, null=True, upload_to=file_name, storage=DownloadableS3Boto3Storage())
    
    ine_effective_date = models.DateField(verbose_name="Fecha de vigencia - ID", null=True, blank=True)
    bank_account_effective_date = models.DateField(verbose_name="Fecha de vigencia - Cuenta de banco", null=True, blank=True)
    proof_address_effective_date = models.DateField(verbose_name="Fecha de vigencia - Comprobante de domicilio", null=True, blank=True)
    additional_file_effective_date = models.DateField(verbose_name="Fecha de vigencia - Documento Adicional", null=True, blank=True)
    paysheet_one_effective_date = models.DateField(verbose_name="Fecha de vigencia - Último recibo de nómina", null=True, blank=True)
    paysheet_two_effective_date = models.DateField(verbose_name="Fecha de vigencia - Penúltimo recibo de nómina", null=True, blank=True)
    paysheet_three_effective_date = models.DateField(verbose_name="Fecha de vigencia - Antepenúltimo recibo de nómina", null=True, blank=True)
    selfie_effective_date = models.DateField(verbose_name="Fecha de vigencia - Selfie", null=True, blank=True)


    class Meta:
        ordering = ['client']
        verbose_name = 'Document'
        verbose_name_plural = 'Documents'

    def __unicode__(self):
        return u"{0}".format(self.client)

    def __str__(self):
        return u"{0}".format(self.client)


class ClientAdjustment(models.Model):
    client = models.ForeignKey(Client, verbose_name="Cliente", on_delete=models.CASCADE, null=False)
    adjustment = models.IntegerField(verbose_name="Ajuste")
    gray = models.IntegerField(verbose_name="Semillas Grises")
    green = models.IntegerField(verbose_name="Semillas Verdes")
    yellow = models.IntegerField(verbose_name="Semillas Amarillas")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de ajuste")

    class Meta:
        verbose_name = 'Ajuste'
        verbose_name_plural = 'Ajustes'

    def __unicode__(self):
        return u"{0}".format(self.client)

    def __str__(self):
        return u"{0}".format(self.client)
