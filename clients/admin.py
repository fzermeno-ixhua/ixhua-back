from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import Client, ClientToken, ClientDocuments, ClientAdjustment
from loan.models import Loan, Seeds, RequirementsClient
from .resources import ClientResource, ExportClientResource
from django.contrib.admin import SimpleListFilter
from django.utils.safestring import mark_safe
from loan.views import calculate_seed
import sys
from openpyxl import Workbook

from datetime import datetime
from datetime import timedelta
from django.http import HttpResponse

@admin.register(ClientAdjustment)
class ClientAdjustmentAdmin(admin.ModelAdmin):
    list_display = ('client', 'adjustment', 'gray', 'green', 'yellow', 'created_at')
    list_max_show_all = sys.maxsize
    list_per_page = 10
    list_display_links = None
    # readonly_fields = ('gray', 'green', 'yellow', 'created_at')
    readonly_fields = ('gray', 'green', 'yellow')
    exclude = ['created_at']
    # exclude = ['gray', 'green', 'yellow', 'created_at']
    search_fields = (
        'client__rfc',
    )

    def save_model(self, request, obj, form, change):
        # obj.end_date = obj.initial_date.replace(obj.initial_date.year + 1)
        # if not obj.id:
        loans = Loan.objects.filter(client=obj.client)
        seed = Seeds.objects.filter(loan__in=list(loans)).order_by('-pk').first()
        adjustment = ClientAdjustment.objects.filter(client=obj.client).order_by('-pk').first()


        if adjustment is None and seed is None:
            gray = 0
            green = 5
            yellow = 0
        elif adjustment is None:
            gray = seed.gray
            green = seed.green
            yellow = seed.yellow
        elif seed is None:
            gray = adjustment.gray
            green = adjustment.green
            yellow = adjustment.yellow
        else:
            if adjustment.created_at > seed.created_at:
                gray = adjustment.gray
                green = adjustment.green
                yellow = adjustment.yellow
            else:
                gray = seed.gray
                green = seed.green
                yellow = seed.yellow


        if obj.adjustment > 0:
            for x in range(obj.adjustment):
                gray, green, yellow = calculate_seed(gray, green, yellow, True)
                obj.client.user.is_active = True
                obj.client.user.save()
        elif obj.adjustment < 0:
            for x in range(-obj.adjustment):
                gray, green, yellow = calculate_seed(gray, green, yellow, False)

        if gray == 5:
            obj.client.user.is_active = False
            obj.client.user.save()

        obj.gray = gray
        obj.green = green
        obj.yellow = yellow

        obj.save()


#Section Filter
class SectionFilter(SimpleListFilter):
    title = 'Activo'

    parameter_name = 'showName'
    default_status = 1

    def lookups(self, request, model_admin):
        return (
            (0, 'Activos'),
            (1, 'Inactivos'),
        )

    def default_value(self):
        return 1

    def choices(self, cl):  # Overwrite this method to prevent the default "All"
        from django.utils.encoding import force_text
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == force_text(lookup),
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }


    def queryset(self, request, queryset):
        #import pdb; pdb.set_trace()
        if self.value() is None:
            return queryset.filter(is_active=True)
        if self.value() == '0' or self.value() == None:
            return queryset.filter(is_active=True)
        elif self.value() == '1':
            return queryset.filter(is_active=False)


class RequirementsClientInline(admin.TabularInline):
    """ 
    loan_requeriments_client Inline. 
    Allows to add requirements to Client inside Client record

    [class_related]: PersonAdmin

    Added-Dev on: [2020-12-19]
    Added-Prod on: 

    """
    model = RequirementsClient
    exclude = ['created_at', 'modified_at']
    extra = 1
    ordering = ('status',)


@admin.register(Client)
class PersonAdmin(ImportExportModelAdmin):
    """ 
    clients_client  table Admin. 
    Allows to manipulate records in table

    Added-Dev on: []
    Added-Prod on: 

    """
    actions = ['export_loan_amount_available']
    resource_class = ClientResource
    list_max_show_all = sys.maxsize
    list_per_page = 20
    #list_display = ('rfc', 'names', 'father_last_name','mother_last_name', 'birthdate', 'actual_position', 'admission_date', 'is_active', 'cellphone', 'phone', 'email', 'bank_name', 'bank_clabe', 'address', 'salary', 'payment_frequency', 'credit_amount', 'family', 'family', 'subfamily')
    list_display = ('rfc', 'names', 'father_last_name','mother_last_name','family','is_active','created_on_website','status','email_actions',)
    search_fields = ('rfc', 'names', 'father_last_name','mother_last_name','family__tradename','is_active','status')
    ordering = ('names', )
    readonly_fields = ('pin', 'user')
    #exclude = ['pin']
    # list_filter = ('is_active', SectionFilter)
    list_filter = (SectionFilter, )
    inlines = [RequirementsClientInline,]

    def get_export_resource_class(self):
        """
        Returns ResourceClass to use for export [resources.py].
        """
        return ExportClientResource
    
    def email_actions(self, obj):
        return mark_safe(
            '<a style="float: none;" class="button default email" data-id={0} data-action="approved" >Enviar correo de invitación</a>&nbsp;'.format(
                obj.id,
            )
        )

    def export_loan_amount_available(PersonAdmin, request, queryset):

        client_qs = queryset.prefetch_related('client_loans','client_loans__payments').all()
        workbook = Workbook()
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',)
        response['Content-Disposition'] = 'attachment; filename={date}-monto-disponible.xlsx'.format(date=datetime.now().strftime('%Y-%m-%d'))

        # Get active worksheet/tab
        worksheet = workbook.active
        worksheet.title = 'reporte'

        #Define the titles for columns
        columns = [
        'ID',
        'RFC ',
        'Nombres',
        'Apellido Paterno',
        'Apellido Materno',
        'Empresa',
        'Email',
        'Celular',
        'Total Autorizado',
        'Monto retirado',
        'Total Disponible'
        ]
        row_num = 1


        for col_num, column_title in enumerate(columns, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = column_title

        for client in client_qs:
            row_num += 1
            amount_held_sum = 0 
            paid = 0
            comisions = 0
            capital_paid = 0
            result = 0
            
            #vars 
            clientID = None
            clientRfc = None
            clientNames = None
            clientFatherLastName = None
            clientMotherLastName = None
            clientFamilyTradeName = None
            clientEmail = None
            clientCellphone = None
            clientMaxAmount = None
            clientHeldSum = None
            clientAmountAvailable = None



            #Loan info filter
            try:
                for b in client.client_loans.filter(state__in=[Loan.APPROVED, Loan.PENDING]):
                    #amount_held_sum += b.amount_held        
                    paid = b.amount_paid
                    comisions = b.amount_total - b.amount_requested
                    capital_paid = max(paid - comisions, 0)
                    amount_held_sum += max(b.amount_requested - capital_paid, 0)
            except:
                amount_held_sum = 0
            # Define the data for each cell in the row 
           
            try:
                clientID = client.id
            except:
                clientID = 0

            try:
                clientRfc = client.rfc
            except:
                clientRfc = ""

            try:
                clientNames = client.names
            except:
                clientRfc = ""

            try:
                clientFatherLastName = client.father_last_name
            except:
                clientFatherLastName = ""

            try:
                clientMotherLastName = client.mother_last_name
            except:
                clientMotherLastName = ""

            try:
                clientFamilyTradeName = client.family.tradename
            except:
                clientFamilyTradeName = ""

            try:
                clientEmail = client.email
            except:
                clientEmail = ""

            try:
                clientCellphone = client.cellphone
            except:
                clientCellphone = ""

            try:
                clientMaxAmount = client.loan_max_amount
            except:
                clientCellphone = ""

            try:
                clientAmountAvailable = client.loan_amount_available
            except:
                clientAmountAvailable = ""

            try:
                clientAmountAvailable = client.loan_amount_available
            except:
                clientAmountAvailable = ""


            row = [
                clientID,
                clientRfc,
                clientNames,
                clientFatherLastName,
                clientMotherLastName,
                clientFamilyTradeName,
                clientEmail,
                clientCellphone,
                clientMaxAmount,
                amount_held_sum,
                client.loan_amount_available,
            ]

            # Assign the data for each cell of the row 
            for col_num, cell_value in enumerate(row, 1):
                cell = worksheet.cell(row=row_num, column=col_num)
                cell.value = cell_value


        workbook.save(response)
        return response
    export_loan_amount_available.short_description = "Reporte de Monto Disponible"

    class Media:
        css = {
            'all': ('admin/css/sweetalert2.css',)
        }
        js = ('admin/js/vendor/jquery.min.js', 'admin/js/send_invitation.js', 'admin/js/vendor/sweetalert2.min.js')

# @admin.register(ClientToken)
# class ClientTokenAdmin(ImportExportModelAdmin):
#     list_per_page = 10
#     list_display = ('token', 'expire', 'client',)

@admin.register(ClientDocuments)
class ClientDocumentsAdmin(ImportExportModelAdmin):
    list_max_show_all = sys.maxsize
    list_per_page = 10
    #list_display = ('token', 'expire', 'client',)
    list_display = ('client', 'client_fullnames','client_family', 'client_subfamily','client_public')
    fields = (
        'client', 
        'ine',
        'ine_effective_date',
        'bank_account',
        'bank_account_effective_date',
        'proof_address',
        'proof_address_effective_date',
        'additional_file',
        'additional_file_effective_date',
        'paysheet_one',
        'paysheet_one_effective_date',
        'paysheet_two',
        'paysheet_two_effective_date',
        'paysheet_three',
        'paysheet_three_effective_date',
        'selfie',
        'selfie_effective_date',
        )
    search_fields = ('client__rfc','client__names','client__father_last_name','client__mother_last_name','client__subfamily','client__family_str',)
    def client_fullnames(self, obj):
        if obj.client:
            return obj.client.names + ' ' + obj.client.father_last_name + ' ' + obj.client.mother_last_name
    client_fullnames.short_description = 'Nombre completo'
    client_fullnames.admin_order_field = 'client__names'

    def client_subfamily(self, obj):
        if obj.client:
            if obj.client.family:
                return obj.client.subfamily
    client_subfamily.short_description = 'Subfamilia'
    client_subfamily.admin_order_field = 'client__subfamily'

    def client_family(self, obj):
        if obj.client:
            if obj.client.family:
                return obj.client.family
    client_family.short_description = 'Familia'
    client_family.admin_order_field = 'client__family'

    def client_public(self, obj):
        if obj.client:
            if obj.client.created_on_website is True:
                return 'Ixhua Public'
            else:
                return '-'
    client_public.short_description = 'Ixhua Public'
    client_public.admin_order_field = 'client__public'
