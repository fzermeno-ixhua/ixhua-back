# Generated by Django 3.0.4 on 2020-03-26 20:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0030_auto_20200325_2323'),
    ]

    operations = [
        migrations.RenameField(
            model_name='client',
            old_name='full_name_axu',
            new_name='full_name_aux',
        ),
    ]
