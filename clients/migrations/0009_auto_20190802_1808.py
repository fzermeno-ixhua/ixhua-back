# Generated by Django 2.0.2 on 2019-08-02 18:08

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0008_auto_20190731_1424'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='birth_place',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Lugar de nacimiento'),
        ),
        migrations.AddField(
            model_name='client',
            name='civil_status',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Estado civil'),
        ),
        migrations.AddField(
            model_name='client',
            name='country',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Pais'),
        ),
        migrations.AddField(
            model_name='client',
            name='credit_bureau',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='¿Has presentado atrasos en Buró de Crédito?'),
        ),
        migrations.AddField(
            model_name='client',
            name='credit_lines',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='¿Cuentas con alguna línea de crédito?'),
        ),
        migrations.AddField(
            model_name='client',
            name='curp',
            field=models.CharField(max_length=18, null=True, validators=[django.core.validators.MinLengthValidator(18)], verbose_name='RFC'),
        ),
        migrations.AddField(
            model_name='client',
            name='economic_dependent',
            field=models.IntegerField(default=0, verbose_name='Dependiente económicos'),
        ),
        migrations.AddField(
            model_name='client',
            name='family_income',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Ingreso familiar acumulado'),
        ),
        migrations.AddField(
            model_name='client',
            name='gender',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Nacionalidad'),
        ),
        migrations.AddField(
            model_name='client',
            name='house_type',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Tipo de vivienda'),
        ),
        migrations.AddField(
            model_name='client',
            name='interior_no',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='No. int.'),
        ),
        migrations.AddField(
            model_name='client',
            name='labor_old',
            field=models.PositiveIntegerField(default=0, validators=[django.core.validators.MaxValueValidator(99)], verbose_name='Antigüedad'),
        ),
        migrations.AddField(
            model_name='client',
            name='nationality',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Género'),
        ),
        migrations.AddField(
            model_name='client',
            name='outdoor_no',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='No. ext.'),
        ),
        migrations.AddField(
            model_name='client',
            name='postal_code',
            field=models.CharField(blank=True, max_length=5, null=True, validators=[django.core.validators.MinLengthValidator(5), django.core.validators.RegexValidator('^[0-9]*$')], verbose_name='Codigo Postal'),
        ),
        migrations.AddField(
            model_name='client',
            name='state',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Estado'),
        ),
        migrations.AddField(
            model_name='client',
            name='study_level',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Grado de estudios'),
        ),
        migrations.AddField(
            model_name='client',
            name='suburb',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Colonia'),
        ),
        migrations.AddField(
            model_name='client',
            name='tell_us',
            field=models.TextField(blank=True, null=True, verbose_name='¡Platícanos! ¿Para qué usarías tu préstamo?'),
        ),
        migrations.AddField(
            model_name='client',
            name='years_residence',
            field=models.PositiveIntegerField(default=0, validators=[django.core.validators.MaxValueValidator(99)], verbose_name='Años de residencia'),
        ),
    ]
