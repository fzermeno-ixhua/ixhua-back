# Generated by Django 2.0.2 on 2019-08-05 17:12

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0013_auto_20190805_1556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='curp',
            field=models.CharField(blank=True, max_length=18, null=True, validators=[django.core.validators.MinLengthValidator(18)], verbose_name='CURP'),
        ),
    ]
