# Generated by Django 2.0.2 on 2019-10-24 20:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0022_auto_20191007_1731'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='invited',
            field=models.BooleanField(default=True, verbose_name='Invitado'),
        ),
    ]
