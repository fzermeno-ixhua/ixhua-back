# Generated by Django 2.0.2 on 2019-07-31 14:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0007_clienttoken'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clienttoken',
            name='client',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='clients.Client'),
        ),
    ]
