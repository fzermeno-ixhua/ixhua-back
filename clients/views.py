from django.shortcuts import render
from .models import Client
from loan.models import Loan, PartialPayment

from django.http import HttpResponse
import json
from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from decouple import config



# Create your views here.
def SendClientInvitation(request):

    if request.POST:
        id = request.POST.get('id')
        client = Client.objects.get(pk=id)
        total_loan = client.credit_amount
        active_loan = 0
        loan_payment = 0

        loans = Loan.objects.filter(client=client, state=2)
        
        for loan in loans:
            active_loan += loan.amount_requested
            payments = PartialPayment.objects.filter(loan=loan)
            
            for payment in payments:
                loan_payment += payment.amount
                if loan_payment > loan.amount_requested:
                    loan_payment = loan.amount_requested
            
            active_loan -= loan_payment
            loan_payment = 0

        
        available_loan = total_loan - active_loan

        subject = 'Tienes un crédito pre - autorizado'
        html_message = render_to_string(
            'index.html',
            {
                'names': client.names,
                'last_names': client.father_last_name + ' ' + client.mother_last_name,
                'credit_amount': '{:20,.2f}'.format(available_loan),
                'url': config('FRONT_URL')
            }
        )
        plain_message = strip_tags(html_message)
        from_email = config('EMAIL_HOST_USER')
        to = client.email

        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
        
        # Set invited as True
        client.invited = True
        client.save()
        
    return HttpResponse(json.dumps({'success': True}), content_type="application/json")
