from django.urls import path
from .views import SendClientInvitation

client_patterns = ([
    path('send-client-invitation', SendClientInvitation, name="send-client-invitation"),
], 'clients')