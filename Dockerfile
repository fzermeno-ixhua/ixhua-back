FROM python:3.7
LABEL MAINTAINER PixanApps
ADD . /usr/src/app
WORKDIR /usr/src/app
COPY Pipfile ./
COPY Pipfile.lock ./
RUN pip install pipenv
RUN pipenv install --system --deploy --ignore-pipfile

CMD ["sh", "-c", "python manage.py collectstatic --noinput; python manage.py migrate; gunicorn project.wsgi:application --bind 0.0.0.0:8000 --workers 3 & celery -A project worker -l info"]
