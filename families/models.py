from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator, MaxLengthValidator, RegexValidator
from dj.choices import Choices, Choice
from products.models import Product
import datetime


class Period(Choices):
    decennial = Choice("Decenal")
    biweekly = Choice("Catorcenal")
    fortnightly = Choice("Quincenal")
    monthly = Choice("Mensual")

class Family(models.Model):
    """ 
    families_family table model
    Defines families for binding clients

    [related_tables]

    Added-Dev on:
    Added-Prod on: 
    """
    business_name = models.CharField(verbose_name="Razón Social", max_length=255, null=True)
    tradename = models.CharField(verbose_name="Nombre Comercial", max_length=255, null=True)
    rfc = models.CharField(verbose_name="RFC", max_length=13, null=False, unique=False, validators=[MinLengthValidator(12)])
    address = models.CharField(verbose_name="Calle y número", max_length=255, null=False)
    postal_code = models.CharField(verbose_name="Codigo Postal", max_length=5, validators=[MinLengthValidator(5), RegexValidator('^[0-9]*$')])
    state = models.CharField(verbose_name="Estado", max_length=255)
    suburb = models.CharField(verbose_name="Colonia", max_length=255)
    folio = models.CharField(verbose_name="Folio", max_length=255)
    cellphone = models.CharField(verbose_name="Celular", null=True, max_length=10,validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    email = models.EmailField(verbose_name="Email")
    subfamily = models.TextField(verbose_name="Subfamilia", null=False)
    employees_no = models.IntegerField(verbose_name="Numero de empleados", default=0)
    qualification = models.CharField(verbose_name="Calificacion", max_length=255)
    is_active = models.BooleanField(verbose_name="Activo", default=True)
    payday = models.DateField(
        verbose_name="Fecha de pago de nómina",
        null=False, default=datetime.date(2019, 1, 15),
        help_text="Si el día de pago de la familia cae en día feriado se recomienda seleccionar el día hábil anterior más cercano a la fecha de pago, de lo contrario puede que las fechas de pago siguientes se desfasen fuera de lo esperado.")
    period = models.IntegerField(verbose_name="Periodo de pago", choices=Period(), default=Period.fortnightly.id)
    url_stream_provider = models.URLField(verbose_name = "URL Streaming", null = True, blank = True, help_text = "URL del stream provider")
    created_at = models.DateTimeField(default=datetime.datetime.now)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"{0}".format(self.tradename)

    def __str__(self):
        return u"{0}".format(self.tradename)

    class Meta:
        ordering = ['business_name']
        verbose_name = 'Familia'
        verbose_name_plural = 'Familias'


class Holidays(models.Model):
    day = models.DateField(verbose_name="Fecha", null=False)
    description = models.CharField(verbose_name="Descripción", max_length=100, null=True, blank=True)


    def __unicode__(self):
        return u"{0}".format(self.day)

    def __str__(self):
        return u"{0}".format(self.day)

    class Meta:
        ordering = ['day']
        verbose_name = 'Día no laborable'
        verbose_name_plural = 'Días no laborales'

class Paydays(models.Model):
    day = models.DateField(verbose_name="Fecha", null=False)
    family = models.ForeignKey(Family, verbose_name="Familia", on_delete=models.CASCADE, null=True, blank=True)
    notes = models.CharField(verbose_name="Notas", max_length=100, null=True, blank=True)

    @property
    def formatted_date(self):
        return self.day.strftime('%d/%m/%Y')

    def __unicode__(self):
        return u"{0}".format(self.day)

    def __str__(self):
        return u"{0}".format(self.day)

    class Meta:
        ordering = ['day']
        verbose_name = 'Día de pago'
        verbose_name_plural = 'Días de pago'
        unique_together = ('day', 'family',)


class CustomMessage(models.Model):
    family = models.ForeignKey(Family, verbose_name="Familia", on_delete=models.CASCADE, null=True, blank=True)
    message = models.CharField(verbose_name="Mensaje", max_length=400, null=True, blank=True)
    website= models.URLField(verbose_name="URL", max_length=400, null=True, blank=True)

    def __unicode__(self):
        return u"{0}".format(self.message)

    def __str__(self):
        return u"{0}".format(self.message)

    class Meta:
        ordering = ['family']
        verbose_name = 'Mensaje Personalizado'
        verbose_name_plural = 'Mensajes Personalizados'

class PromotionCode(models.Model):
    code = models.CharField(verbose_name="Código", max_length=10, null=True, blank=True, help_text="El código debe ser máximo de 10 caracteres y estar en mayúsculas.")
    family = models.ForeignKey(Family, verbose_name="Familia", on_delete=models.CASCADE, null=True, blank=True)
    product = models.ForeignKey('products.Product', verbose_name="Producto", on_delete=models.CASCADE, null=True, blank=True)
    
    def __unicode__(self):
        return u"{0}".format(self.code)

    def __str__(self):
        return u"{0}".format(self.code)

    class Meta:
        ordering = ['code']
        verbose_name = 'Código'
        verbose_name_plural = 'Códigos'

