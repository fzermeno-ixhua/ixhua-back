from import_export import resources, fields
from import_export.widgets import ForeignKeyWidget
from .models import Paydays
from families.models import Family


class PaydaysResource(resources.ModelResource):
    day = fields.Field(attribute="day", column_name="FECHA",)
    family = fields.Field(
                          attribute="family",
                          column_name="FAMILIA",
                          widget=ForeignKeyWidget(Family, "tradename"), )
    notes = fields.Field(attribute="notes", column_name="ANOTACION",)


    class Meta:
        model = Paydays
        #exclude = ('id',)
        import_id_fields = ('day', 'family',)
        fields = ('day',
                  'family',
                  'notes',
                  )

