from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Family
from .forms import FamilyForm
from django.http import HttpResponse
import json

# Create your views here.

def ActivateFamily(request):
    if request.POST:
        id = request.POST.get('id')
        isActive = True if request.POST.get('action') == 'activar' else False
        family = Family.objects.get(pk=id)
        family.is_active = isActive
        family.save()
    return HttpResponse(json.dumps({'success': True}), content_type="application/json")

class FamilyListView(ListView):
    model = Family
    paginate_by = 10

class FamilyDetailView(DetailView):
    model = Family


    def get (self,request,*args,**kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)

        if "active" in request.GET:
            if request.GET['active'] == 'Activar':
                self.object.active = True
                self.object.save()
            elif request.GET['active'] == 'Desactivar':
                self.object.active = False
                self.object.save()

        return self.render_to_response(context) 
    

class FamilyCreate(CreateView):
    model = Family
    form_class = FamilyForm
    
    def get_success_url(self):
        return reverse_lazy('families:families') + '?created'

class FamilyDelete(DeleteView):
    model = Family
    success_url = reverse_lazy('families:families')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
    
    def get_success_url(self):
        return reverse_lazy('families:families') + '?deleted'

class FamilyUpdate(UpdateView):
    model = Family
    form_class = FamilyForm
    template_name_suffix = '_update_form'
    
    def get_success_url(self):
        return reverse_lazy('families:families') + '?updated'

    
    