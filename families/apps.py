from django.apps import AppConfig


class FamiliesConfig(AppConfig):
    name = 'families'
    verbose_name = 'Gestor de familias'
