from django.contrib import admin
from .models import Family, Holidays, Paydays, CustomMessage,PromotionCode
from loan.models import RequirementsFamily
from harvest.models import Harvest 
from django.utils.safestring import mark_safe
from import_export.admin import ImportExportModelAdmin
from .resources import PaydaysResource
import sys
from django.utils.translation import gettext_lazy as _ 
from datetime import date


class TemporalityPaydaysFilter(admin.SimpleListFilter):
    """ 
    PaydaysAdmin Filter. 
    Adds aditional filter by year (last|next) to Paydays List

    [class_related]: PaydaysAdmin


    Added-Dev on: [2020-12-22]
    Added-Prod on: 

    """
    # Human-readable title which will be displayed in the right admin sidebar just above the filter options.
    title = _('Temporalidad')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'payday_temporality'

    
    def lookups(self, request, model_admin):
        # Tuple (1.URL query option selected, 2. Human readable word)

        return (
            ('nextmonth', _('Mes Siguiente')),
            ('lastyear', _('Año Anterior')),
            ('nextyear', _('Año Siguiente')),
        )

    def queryset(self, request, queryset):
        #Returns the filtered queryset based on the value

        today = date.today()
        this_lastyear = today.year - 1
        this_nextyear = today.year + 1
        this_nextmonth = today.month + 1
        if self.value() == 'lastyear':
            return queryset.filter(day__gte=date(this_lastyear, 1, 1),
                                    day__lte=date(this_lastyear, 12, 31))
        if self.value() == 'nextyear':
            return queryset.filter(day__gte=date(this_nextyear, 1, 1),
                                    day__lte=date(this_nextyear, 12, 31))
        if self.value() == 'nextmonth':
            if this_nextmonth > 12:
                return queryset.filter(day__gte=date(this_nextyear, 1, 1),
                                        day__lte=date(this_nextyear, 1, 31))
            else:    
                return queryset.filter(day__gte=date(today.year, this_nextmonth, 1),
                                        day__lte=date(thoday.year, this_nextmonth, 31))




# Register your models here.
@admin.register(Paydays)
class PaydaysAdmin(ImportExportModelAdmin):
    resource_class = PaydaysResource
    list_max_show_all = sys.maxsize
    list_per_page = 54 #52 weeks a year + 2
    list_display = (
        'day',
        'family',
        'notes'
    )
    #today = datetime.date.today()
    #Paydays.objects.filter(day__year=today.year)
    #qs = Paydays.objects.annotate(year=ExtractYear('day')).filter(year = today.year)
    search_fields = ('family__business_name', 'family__tradename')
    list_filter = ('day',TemporalityPaydaysFilter)


@admin.register(CustomMessage)
class CustomMessageAdmin(ImportExportModelAdmin):
    list_max_show_all = sys.maxsize
    list_per_page = 10
    list_display = (
        'family',
        'message',
        'website'
    )


@admin.register(Holidays)
class HolidaysAdmin(admin.ModelAdmin):
    list_max_show_all = sys.maxsize
    list_per_page = 10
    list_display = (
        'day',
        'description',
    )

class RequirementsFamilyInline(admin.TabularInline):
    """ 
    loan_requeriments_family Inline. 
    Allows to add requirements to the family inside Family record

    [class_related]: FamilyAdmin


    Added-Dev on: [2020-12-19]
    Added-Prod on: 

    """
    model = RequirementsFamily
    exclude = ['created_at', 'modified_at']
    extra = 1
    ordering = ('status',)


class FamilyAdmin(admin.ModelAdmin):
    list_max_show_all = sys.maxsize
    list_per_page = 10
    list_display = ('business_name', 'tradename', 'employees_no', 'harvest_count', 'qualification','is_active',)
    search_fields = ('business_name', 'tradename', 'employees_no', 'qualification','is_active', )
    ordering = ('business_name',)
    readonly_fields = ('detail_actions', )
    exclude = ['is_active']
    inlines = [RequirementsFamilyInline,]

    def detail_actions(self, obj):
        if obj.id:
            action = 'Desactivar' if obj.is_active else 'Activar'
            model = 'Family'
            message = 'esta familia'
            return mark_safe(
                '<a id="activate-model" style="float: none;" class="button default" data-id={0} data-action={1}  data-model={2} data-label={3} data-message="{4}" href="{0}">{1}</a>&nbsp;'
                '<a id="cancel-save" class="deletelink" style="background: none;" data-url="{5}" href="#">Cancelar</a>'.format(
                    obj.pk,
                    action,
                    model,
                    obj._meta.app_label,
                    message,
                    '/admin/{}/{}'.format(obj._meta.app_label, obj._meta.model_name))
            )
        return mark_safe('<a id="cancel-save" class="btn" data-url="{}" href="#">Cancel</a>'.format(
            '/admin/{0}/{1}'.format(obj._meta.app_label, obj._meta.model_name)))
    detail_actions.short_description = 'Acciones'

    def harvest_count(self, obj):
        if obj.id:
            harvest = Harvest.objects.filter(family = obj.id)
            return len(harvest)
    
    harvest_count.short_description = 'No. de cosechas activas'

    class Media:
        css = {
            'all': ('admin/css/sweetalert.css',)
        }
        js = ('admin/js/vendor/jquery.min.js', 'admin/js/vendor/sweetalert.min.js', 'admin/js/admin.js', )

admin.site.register(Family, FamilyAdmin)


@admin.register(PromotionCode)
class PromotionCodeAdmin(admin.ModelAdmin):
    list_max_show_all = sys.maxsize
    list_per_page = 10
    search_fields = ('code','family__tradename')
    list_display = (
        'code',
        'family',
        'product',
    )


