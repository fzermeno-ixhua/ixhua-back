# Generated by Django 2.2.3 on 2021-01-05 23:48

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('families', '0019_family_url_stream_provider'),
    ]

    operations = [
        migrations.AddField(
            model_name='family',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='family',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
