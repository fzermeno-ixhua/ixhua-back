from .views import FamilyListView, FamilyDetailView, FamilyCreate, FamilyDelete, FamilyUpdate
from django.urls import path

families_patterns = ([
    path('', FamilyListView.as_view(), name='families'),
    path('create/', FamilyCreate.as_view(), name='create'),
    path('<int:pk>/<slug:slug>/', FamilyDetailView.as_view(), name='family'),
    path('delete/<int:pk>/', FamilyDelete.as_view(), name='delete'),
    path('update/<int:pk>/', FamilyUpdate.as_view(), name='update'),
], 'families')