from django import forms
from .models import Family

class FamilyForm(forms.ModelForm):

    class Meta:
        model = Family
        fields = ['business_name', 'tradename', 'rfc', 'address', 'postal_code', 'state', 'suburb', 'folio', 'cellphone', 'email', 'subfamily', 'qualification']
        #fields = ['business_name', 'tradename', 'rfc', 'address', 'postal_code', 'state', 'suburb', 'folio', 'cellphone', 'email', 'qualification']

        widgets = {
            'business_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Razón Social '}),
            'tradename': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre Comercial'}),
            'rfc': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'RFC'}),
            'address': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Calle y número'}),
            'postal_code': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'C.P.'}),
            'state': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Estado'}),
            'suburb': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Colonia'}),
            'folio': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Folio'}),
            'cellphone': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Celular'}),
            'email': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'E-mail'}),
            'subfamily': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Subfamilias separadas por coma ","'}),
            'qualification': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Calificación'}),
        }
        # labels = {
        #     'business_name': '', 'tradename': '', 'rfc': ''
        # }

    def __init__(self, *args, **kwargs):
        super(FamilyForm, self).__init__(*args, **kwargs)
        self.fields['business_name'].required = False
        self.fields['tradename'].required = False
        self.fields['rfc'].required = False
        self.fields['address'].required = False
        self.fields['postal_code'].required = False
        self.fields['state'].required = False
        self.fields['suburb'].required = False
        self.fields['folio'].required = False
        self.fields['cellphone'].required = False
        self.fields['email'].required = False
        self.fields['subfamily'].required = False
        self.fields['qualification'].required = False
    
    def clean_rfc(self):
        if len(self.cleaned_data['rfc']) == 0:
            self.add_error('rfc', 'Por favor ingrese RFC')
            return None
        elif len(self.cleaned_data['rfc']) < 12:
            self.add_error('rfc', 'El RFC debe tener al menos 12 caracteres')
            return None
        elif len(self.cleaned_data['rfc']) > 13:
            self.add_error('rfc', 'El RFC debe tener menos de 13 caracteres')
            return None
        
        family = Family.objects.exclude(id=self.instance.pk).filter(rfc=self.cleaned_data['rfc'])
        
        if len(family) > 0:
            self.add_error('rfc', 'Ya existe una familia con ese RFC, por favor verifique.')
            return None

        return self.cleaned_data['rfc']

    def clean_address(self):
        if len(self.cleaned_data['address']) == 0:
            self.add_error('address', 'Por favor ingrese Calle y número')
            return None

        return self.cleaned_data['address']

    def clean_postal_code(self):
        if len(str(self.cleaned_data['postal_code'])) == 0:
            self.add_error('postal_code', 'Por favor ingrese C.P.')
            return None
        elif len(str(self.cleaned_data['postal_code'])) < 5:
            self.add_error('postal_code', 'El C.P. debe tener al menos 5 digitos')
            return None
        elif len(str(self.cleaned_data['postal_code'])) > 5:
            self.add_error('postal_code', 'El C.P. debe tener menos de 5 digitos')
            return None
        
        return self.cleaned_data['postal_code']

    def clean_state(self):
        if len(str(self.cleaned_data['state'])) == 0:
            self.add_error('state', 'Por favor ingrese Estado')
            return None

        return self.cleaned_data['state']

    def clean_suburb(self):
        if len(str(self.cleaned_data['suburb'])) == 0:
            self.add_error('suburb', 'Por favor ingrese Colonia')
            return None

        return self.cleaned_data['suburb']

    def clean_cellphone(self):
        if len(str(self.cleaned_data['cellphone'])) != 10:
            self.add_error('cellphone', 'El Celular debe tener 10 digitos')
            return None

        return self.cleaned_data['cellphone']
    
    def clean_subfamily(self):
        subfamily = self.cleaned_data['subfamily']
        subfamily = subfamily.replace(' ', '')
        list_subfamily = subfamily.split(',')

        if len(list_subfamily) == 0:
            self.add_error('subfamily', 'Por favor ingrese Subfamilias')
            return None

        for item in list_subfamily:
            if item not in self.subfamily_list():
                self.add_error('subfamily', 'Por favor revise la subfamilia {}'.format(item))
                return None

        return self.cleaned_data['subfamily']


    def subfamily_list(self):
        list_subfamily = []
        for x in range(1, 1001):
            list_subfamily.append('SF00{}'.format(x))
            
        return list_subfamily


