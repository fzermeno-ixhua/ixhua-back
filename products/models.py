"Models declaration"
from django.db import models
from django.core.validators import MinValueValidator
import numpy as np
from decimal import Decimal

# Create your models here.
class Product(models.Model):
    "Class that represents a financial product"
    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    AMOUNT = '$'
    PERCENT = '%'
    FEE_UNITS = (
        (AMOUNT, "$ Monto fijo"),
        (PERCENT, '% Porcentaje del préstamo')
    )
    LOAN_UNITS = (
        (AMOUNT, "$ Monto fijo"),
        (PERCENT, '% Porcentaje sueldo trabajado al solicitar')
    )
    name = models.CharField(
        verbose_name="Nombre",
        max_length=255,
    )
    opening_unit = models.CharField(
        verbose_name="Unidad comisión por apertura",
        max_length=1,
        choices=FEE_UNITS, default=AMOUNT,
    )
    opening_fee = models.DecimalField(
        verbose_name="Comision por apertura",
        max_digits=7, decimal_places=2, default=0,
        validators=[MinValueValidator(0)],
    )
    monthly_rate = models.DecimalField(
        verbose_name="Tasa mensual (%)",
        max_digits=4, decimal_places=2,
        validators=[MinValueValidator(0)]
    )
    loan_min = models.DecimalField(
        verbose_name="Préstamo min. ($)",
        max_digits=9, decimal_places=2,
        validators=[MinValueValidator(0)]
    )
    loan_unit = models.CharField(
        verbose_name="Unidad préstamo max.",
        max_length=1,
        choices=LOAN_UNITS, default=AMOUNT,
    )
    loan_max = models.DecimalField(
        verbose_name="Préstamo max.",
        max_digits=9, decimal_places=2,
        validators=[MinValueValidator(0)]
    )
    days_threshold = models.PositiveIntegerField(
        verbose_name="Umbral días siguiente fecha",
        default=5,
        validators=[MinValueValidator(0)]
    )
    loan_options = models.PositiveIntegerField(
        verbose_name="No. opciones",
        default=2,
        validators=[MinValueValidator(1)]
    )
    seeds_rate = models.DecimalField(
        verbose_name="Tasa semillas (%)",
        help_text="Tasa que suman o restan las semillas, 0 = tasa fija",
        max_digits=4, decimal_places=2, default=1,
        validators=[MinValueValidator(0)],
    )

    @staticmethod
    def montly_to_daily_rate(monthly_rate):
        "Converts monthly rate to daily"
        return monthly_rate * 12 / 360

    @staticmethod
    def monthly_to_tfa(monthly_rate):
        "Converts monthly rate to anual"
        return monthly_rate * 12

    @staticmethod
    def tax_rate():
        "Gets the current tax rate base 1"
        #TO DO fetch from database
        return Decimal(1.16)

    @staticmethod
    def calculate_cat(amount, days, total):
        "Calculate the cat"
        tir_list = [0] * (days - 1)
        tir_list.insert(0, -amount)
        tir_list.append(total)
        tir = np.irr(tir_list)
        cat = ((1 + tir) ** 360) - 1

        return cat * 100

    def current_rate(self, seeds):
        "Return the current monthly rate based on seeds; value between 0 and 100"
        if self.seeds_rate == 0:
            return self.monthly_rate

        if seeds.gray > 0:
            return self.monthly_rate + max(seeds.gray, 5) * self.seeds_rate

        if seeds.yellow > 0:
            return max(self.monthly_rate - max(seeds.yellow, 5) * self.seeds_rate, 0)

        return self.monthly_rate

    def calculate_interest(self, amount, days, seeds):
        "Calculate the interest to pay based on"
        rate = self.current_rate(seeds)
        daily_rate = Product.montly_to_daily_rate(rate)
        interest = Decimal(0.01) * daily_rate * days * amount
        tfa = Product.monthly_to_tfa(rate)
        return (interest, tfa)

    def calculate_aperture(self, amount):
        "calculate the operture fee"
        if self.opening_unit == Product.PERCENT:
            return  Decimal(0.01) * self.opening_fee * amount
        if self.opening_unit == Product.AMOUNT:
            return self.opening_fee
        return 0

    def calculate_fees(self, amount, days, seeds):
        "Calculate the fees"
        aperture = self.calculate_aperture(amount)
        interest, tfa = self.calculate_interest(amount, days, seeds)
        total_tax_free = amount + interest + aperture
        aperture *= Product.tax_rate()
        interest *= Product.tax_rate()
        total = amount + interest + aperture
        cat = Product.calculate_cat(amount, days, total_tax_free)
        return {
            'interest': round(interest, 2),
            'tfa': round(tfa, 2),
            'aperture': round(aperture, 2),
            'total': round(total, 2),
            'cat': round(cat, 2),
        }

    @staticmethod
    def default():
        "Default financial conditions"
        return Product(
            name='default',
            opening_unit='$', opening_fee=0,
            monthly_rate=18, seeds_rate=1,
            loan_unit='$', loan_min=2000, loan_max=5000,
            loan_options=2
        )

    def __str__(self):
        return u"{0}".format(self.name)
