from django.contrib import admin
from .models import Product

@admin.register(Product)
class ProductsAdmin(admin.ModelAdmin):
    list_display = ('name', 'monthly_rate', 'seeds_rate')
