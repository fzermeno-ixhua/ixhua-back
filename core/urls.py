from django.urls import path
from .views import HomePageView, LoginView, CustomUserListView, CustomUserCreate, CustomUserDetailView, CustomUserDelete, CustomUserUpdate, ActivateModel

# urlpatterns = [
#     path('', HomePageView.as_view(), name="home"),
#     #path('login', LoginView.as_view(), name="login"),
# ]

users_patterns = ([
    #path('', CustomUserListView.as_view(), name='users'),
    #path('create/', CustomUserCreate.as_view(), name='create'),
    #path('<int:pk>/', CustomUserDetailView.as_view(), name='user'),
    #path('delete/<int:pk>/', CustomUserDelete.as_view(), name='delete'),
    #path('update/<int:pk>/', CustomUserUpdate.as_view(), name='update'),
    path('activate-model', ActivateModel, name="active_model"),
], 'users')