from django.contrib.auth import backends
#from django.contrib.auth.models import User
#from core.managers import CustomUserManager as User
from django.contrib.auth import get_user_model

class EmailAuthBackend(backends.ModelBackend):
    """
    Email Authentication Backend

    Allows a user to sign in using an email/password pair, then check
    a username/password pair if email failed
    """

    def authenticate(self, username=None, password=None):
        """ Authenticate a user based on email address as the user name. """
        User = get_user_model()
        
        try:
            user = User.objects.get(email=username.lower())
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None
            

    def get_user(self, user_id):
        """ Get a User object from the user_id. """
        User = get_user_model()
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None