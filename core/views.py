from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import CustomUser
from .forms import CustomUserForm
from django.apps import apps
from django.http import HttpResponse
import json
from django.core.mail import send_mail
from decouple import config
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import atexit

from datetime import date
from clients.models import Client
from loan.models import Loan, PendingPayment
from datetime import datetime, timedelta
#SMS
import nexmo
import phonenumbers
from django.conf import settings


def ActivateCustomUser(request):
    if request.POST:
        id = request.POST.get('id')
        isActive = True if request.POST.get('action') == 'activar' else False
        user = CustomUser.objects.get(pk=id)
        user.is_active = isActive
        user.save()
    return HttpResponse(json.dumps({'success': True}), content_type="application/json")

class HomePageView(TemplateView):
    template_name = "core/home.html"

    def get(self, request, *args, **kwargs):
        username = 'invitado' if str(request.user) == 'AnonymousUser' else request.user
        return render(request, self.template_name, {'title': 'Bienvenido {}'.format(username)})

class LoginView(TemplateView):
    template_name = "core/login.html"

class CustomUserListView(ListView):
    model = CustomUser
    paginate_by = 10

class CustomUserDetailView(DetailView):
    model = CustomUser

class CustomUserCreate(CreateView):
    model = CustomUser
    form_class = CustomUserForm

class CustomUserDelete(DeleteView):
    model = CustomUser
    success_url = reverse_lazy('families:families')

class CustomUserUpdate(UpdateView):
    model = CustomUser
    form_class = CustomUserForm
    template_name_suffix = '_update_form'

def ActivateModel(request):
    if request.POST:
        id = request.POST.get('id')
        isActive = True if request.POST.get('action') == 'activar' else False
        modelName = request.POST.get('model')
        label = request.POST.get('label')
        model = apps.get_model(label, modelName)
        harvest = model.objects.get(pk=id)
        harvest.is_active = isActive
        harvest.save()

        
        client = Client.objects.get(user=id)
        client.is_active = True if request.POST.get('action') == 'activar' else False
        client.status = Client.USER_ACCOUNT if request.POST.get('action') == 'activar' else Client.CANCEL_ACCOUNT
        client.save()

    return HttpResponse(json.dumps({'success': True}), content_type="application/json")


def send_email_reminder(client,date,amount,email):
    print("=========SEND EMAIL " +str(client) + " =============")
    send_mail(
    'Recordatorio IXHUA.MX',
    'Hola '+(client)+', te recordamos que el dia '+str(date)+' se hara el cobro automatico a tu cuenta por '+str(amount)+' pesos. No olvides tener disponible el dinero en tu cuenta. IXHUA.MX',
    config('EMAIL_HOST_USER'),
    #[email],
    ['dmartinez@ixhua.mx'],
    )

def send_sms_reminder(client,date,amount,cellphone):
    
    sms = nexmo.Client(key=settings.NEXMO_KEY, secret=settings.NEXMO_SECRET)
    try:
        sms.send_message({
        'from': 'Ixhua',
        'to': '52'+ str(cellphone),
        'text': 'Hola '+(client)+', te recordamos que el dia '+date.strftime("%d/%m/%Y")+' se hara el cobro automatico a tu cuenta por '+str(amount)+' pesos. No olvides tener disponible el dinero en tu cuenta. IXHUA.MX',
        })
    except:
        pass


def cron_task():

    today = date.today()
    days_before_3 = today + timedelta(days=3)
    days_before_1 = today + timedelta(days=1)
    
    print("today",today)
    print("days_before_3",days_before_3)
    print("days_before_1",days_before_1)

    x = date(2020, 7, 30)
    print("x=",x)

    print("XXXXXXXXXXXXX COMIENZA CRON XXXXXXXXXXXXX")
    pendingPayment = PendingPayment.objects.filter(paid_out=False)
    query = pendingPayment.prefetch_related('loan').all()

    for p in query:
        print("entra for")
        print(p)
        
        #if (p.loan.payment_date == days_before_3) or (p.loan.payment_date == days_before_1) or (p.loan.payment_date == today) or (p.loan.payment_date == x):
        try:
            #send_email_reminder(p.loan.client.names,p.loan.payment_date,p.amount_total,p.loan.client.email)
            send_mail(
            'Recordatorio IXHUA.MX PRUEBA' + str(p.loan.request_no) ,
            'Hola '+(p.loan.client.names)+', te recordamos que el dia '+str(p.loan.payment_date)+' se hara el cobro automatico a tu cuenta por '+str(p.amount_total)+' pesos. No olvides tener disponible el dinero en tu cuenta. IXHUA.MX',
            config('EMAIL_HOST_USER'),
            ['dmartinez@ixhua.mx'],
            )
        except:
            print("except email")
            pass

    print("XXXXXXXXXXXXX TERMINA CRON XXXXXXXXXXXXX")


#scheduler = BackgroundScheduler()
#scheduler.add_job(func=cron_task, trigger="interval",minutes=1)
#scheduler.add_job(cron_task,'cron',hour='14',minute='30')
#scheduler.add_job(func=cron_task,trigger='cron',hour=17,minute=50,second=0,max_instances=1)
#scheduler.start()
# Shut down the scheduler when exiting the app
#atexit.register(lambda: scheduler.shutdown())
