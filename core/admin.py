from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser,Role
from django import forms
from django.utils.safestring import mark_safe

from django.contrib.auth.models import Group
import sys

# Register your models here.
class UserCreationForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ('email',)
        #fields = ('first_name', 'middle_name', 'father_last_name', 'mother_last_name', 'password', 'role', 'email', 'phone', 'cellphone', 'is_active')

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user



class CustomUserAdmin(UserAdmin):
#class CustomUserAdmin(admin.ModelAdmin):
    # The forms to add and change user instances
    add_form = UserCreationForm
    list_max_show_all = sys.maxsize
    list_per_page = 10
    list_display = ("first_name", "father_last_name", "mother_last_name", "email", "role",'last_login','log_count' ,"is_active",)
    ordering = ("email",)

    fieldsets = (
        (None, {'fields': ('first_name', 'father_last_name', 'mother_last_name', 'password', 'role', 'email', 'phone', 'cellphone', 'detail_actions',)}),
        # (None, {'fields': ('first_name', 'middle_name', 'father_last_name', 'mother_last_name', 'password', 'role', 'email', 'phone', 'cellphone', 'detail_actions',)}),
        )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'father_last_name', 'mother_last_name', 'password', 'role', 'email', 'phone', 'cellphone')}
            # 'fields': ('first_name', 'middle_name', 'father_last_name', 'mother_last_name', 'password', 'role', 'email', 'phone', 'cellphone')}
            ),
        )

    search_fields = ("first_name", "father_last_name", "mother_last_name", "email", "is_active")
    #filter_vertical = ('is_active', )
    #filter_horizontal = ['role']
    readonly_fields = ('detail_actions', )
    # exclude = ['is_active', 'middle_name']
    exclude = ['is_active']


    def save_model(self, request, obj, form, change):
        
        if obj.id:
            obj.groups.clear()

        if obj.role == Role.sysadmin.id:
            obj.is_superuser = True
            obj.is_staff = True
            my_group = Group.objects.get(name='Sysadmin')
            obj.save()
            my_group.user_set.add(obj)
        elif obj.role == Role.analista.id:
            obj.is_superuser = False
            obj.is_staff = True
            my_group = Group.objects.get(name='Analista')
            obj.save()
            my_group.user_set.add(obj)
        elif obj.role == Role.cobranza.id:
            obj.is_superuser = False
            obj.is_staff = True
            my_group = Group.objects.get(name='Cobranza')
            obj.save()
            my_group.user_set.add(obj)
        elif obj.role == Role.tesorería.id:
            obj.is_superuser = False
            obj.is_staff = True
            my_group = Group.objects.get(name='Tesorería')
            obj.save()
            my_group.user_set.add(obj)
        elif obj.role == Role.admin.id:
            obj.is_superuser = False
            obj.is_staff = True
            my_group = Group.objects.get(name='Admin')
            obj.save()
            my_group.user_set.add(obj)
        elif obj.role == Role.consulta.id:
            obj.is_superuser = False
            obj.is_staff = True
            my_group = Group.objects.get(name='Consulta')
            obj.save()
            my_group.user_set.add(obj)
        elif obj.role == Role.cliente.id:
            obj.is_superuser = False
            obj.is_staff = False
            my_group = Group.objects.get(name='Cliente')
            obj.save()
            my_group.user_set.add(obj)
        else:
            obj.is_superuser = False
            obj.is_staff = False
            obj.save()
        
        #obj.save()

    def detail_actions(self, obj):
        if obj.id:
            action = 'Desactivar' if obj.is_active else 'Activar'
            model = 'CustomUser'
            message = 'este usuario'
            return mark_safe(
                '<a id="activate-model" style="float: none;" class="button default" data-id={0} data-action={1}  data-model={2} data-label={3} data-message="{4}" href="{0}">{1}</a>&nbsp;'
                '<a id="cancel-save" class="deletelink" style="background: none;" data-url="{5}" href="#">Cancelar</a>'.format(
                    obj.pk,
                    action,
                    model,
                    obj._meta.app_label,
                    message,
                    '/admin/{}/{}'.format(obj._meta.app_label, obj._meta.model_name))
            )
        return mark_safe('<a id="cancel-save" class="btn" data-url="{}" href="#">Cancel</a>'.format(
            '/admin/{0}/{1}'.format(obj._meta.app_label, obj._meta.model_name)))
    detail_actions.short_description = 'Acciones'

    def formfield_for_choice_field(self, db_field, request, **kwargs):

        if db_field.name == "role" and request.user.role != 1:
            db_field.choices = db_field.choices[1:]

        return super(CustomUserAdmin, self).formfield_for_choice_field(db_field, request, **kwargs)

    class Media:
        css = {
            'all': ('admin/css/sweetalert.css',)
        }
        js = ('admin/js/vendor/jquery.min.js', 'admin/js/vendor/sweetalert.min.js', 'admin/js/admin.js', )


admin.site.register(CustomUser, CustomUserAdmin)
