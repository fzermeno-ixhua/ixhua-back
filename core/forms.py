from django import forms
from .models import CustomUser

class CustomUserForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['email', 'role', 'first_name', 'middle_name', 'father_last_name', 'mother_last_name', 'phone', 'cellphone']

    def __init__(self, *args, **kwargs):
        super(CustomUserForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = False
        self.fields['role'].required = False
        self.fields['first_name'].required = False
        self.fields['middle_name'].required = False
        self.fields['father_last_name'].required = False
        self.fields['mother_last_name'].required = False
        self.fields['phone'].required = False
        self.fields['cellphone'].required = False