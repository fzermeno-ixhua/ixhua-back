from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.db import models
from dj.choices import Choices, Choice

from .managers import CustomUserManager
from django.core.validators import MinLengthValidator, MaxLengthValidator, RegexValidator

class Role(Choices):
    sysadmin = Choice("Sysadmin")
    analista = Choice("Analista")
    cobranza = Choice("Cobranza")
    tesorería = Choice("Tesorería")
    cliente = Choice("Cliente")
    admin = Choice("Admin")
    consulta = Choice("Consulta")

class CustomUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(verbose_name="Nombres *", max_length=50, null=True)
    middle_name = models.CharField(verbose_name="Segundo Nombre", max_length=50, null=True, blank=True)
    father_last_name = models.CharField(verbose_name="Apellido paterno *", max_length=50, null=True)
    mother_last_name = models.CharField(verbose_name="Apellido materno *", max_length=50, null=True)
    role = models.IntegerField(verbose_name="Rol", choices=Role(), default=Role.sysadmin.id)
    email = models.EmailField(unique=True, null=False)
    phone = models.CharField(verbose_name="Telefono fijo", null=True, blank=True, max_length=10, validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    cellphone = models.CharField(verbose_name="Celular *", null=True, max_length=10,validators=[MinLengthValidator(10), RegexValidator('^[0-9]*$')])
    log_count = models.IntegerField(verbose_name="# Login", default=0)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. ''Unselect this instead of deleting accounts.'
        ),
    )

    USERNAME_FIELD = 'email'
    objects = CustomUserManager()

    class Meta:
        #verbose_name = _('user')
        #verbose_name_plural = _('users')
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.get_full_name()

    def __str__(self):
        return self.email
